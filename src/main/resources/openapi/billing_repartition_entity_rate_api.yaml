billingRepartitionEntityRates:
  get:
    tags:
      - billingRepartitionEntityRate
      - admin
    summary: Get all the billing repartition entity rates
    description: This method can be used to get the billing repartition entity rates
      <br/>All users with read admin rights are able to do it
    operationId: "getBillingRepartitionEntityRates"
    x-spring-paginated: true
    parameters:
      - $ref: "common_api.yaml#/components/parameters/activeParam"
    responses:
      "200":
        description: "OK"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/ArrayOfBillingRepartitionEntityRates"
      "401":
        $ref: "common_api.yaml#/components/responses/NotAuthenticatedResponse"
      "403":
        $ref: "common_api.yaml#/components/responses/NotAuthorizedResponse"
    security:
      - OAuth2:
        - read-admin
  post:
    tags:
      - billingRepartitionEntityRate
      - admin
    summary: Add a new billing repartition entity rate
    description: This method can be used to add a new billing repartition entity rate.
      <br/>Only users with write admin rights are able to do it.
      <br/>
      <br/>A billing repartition entity rate is valid if
      <ul>
        <li>it is linked to a billing entity repartition</li>
        <li>it has a rate comprised between 0 and 100</li>
      </ul>
    operationId: "addBillingRepartitionEntityRate"
    requestBody:
      description: A perfectly formed *BillingRepartitionEntityRate*
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/BillingRepartitionEntityRate"
    responses:
      "201":
        description: "The billing repartition entity rate has been correctly created"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/BillingRepartitionEntityRate"
      "400":
        description: "Bad request. Something went wrong on the billing repartition entity rate creation"
        content:
          application/json:
            schema:
              $ref: "common_api.yaml#/components/schemas/ApiErrorResponse"
      "401":
        $ref: "common_api.yaml#/components/responses/NotAuthenticatedResponse"
      "403":
        $ref: "common_api.yaml#/components/responses/NotAuthorizedResponse"
    security:
      - OAuth2:
         - write-admin

billingRepartitionEntityRate-id:
  parameters:
    - $ref: "#/components/parameters/billingRepartitionEntityRateIdParam"
  get:
    tags:
      - billingRepartitionEntityRate
      - admin
    summary: Get a billing repartition entity rate
    description: Get a billing repartition entity rate by its id
      <br/>Only user with read admin rights are able to do it
    operationId: "getBillingRepartitionEntityRateById"
    responses:
      "200":
        description: "A billing repartition entity rate"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/BillingRepartitionEntityRate"
      "404":
        $ref: "#/components/responses/BillingRepartitionEntityRateDoesNotExistsResponse"
      "401":
        $ref: "common_api.yaml#/components/responses/NotAuthenticatedResponse"
      "403":
        $ref: "common_api.yaml#/components/responses/NotAuthorizedResponse"
    security:
      - OAuth2:
        - read-admin
  delete:
    tags:
      - billingRepartitionEntityRate
      - admin
    summary: Delete a billing repartition entity rate
    description: Delete a billing repartition entity rate.
      <br/>Only users with write admin rights are able to do it.
    operationId: "deleteBillingRepartitionEntityRateById"
    responses:
      "204":
        description: "Billing repartition entity rate deleted"
      "404": 
        $ref: "#/components/responses/BillingRepartitionEntityRateDoesNotExistsResponse"
      "401":
        $ref: "common_api.yaml#/components/responses/NotAuthenticatedResponse"
      "403":
        $ref: "common_api.yaml#/components/responses/NotAuthorizedResponse"
    security:
      - OAuth2:
        - write-admin
  put:
    tags: 
      - billingRepartitionEntityRate
      - admin
    summary: Update a billing repartition entity rate
    description: Update a billing repartition entity rate
      <br/>Only users with write admin rights are able to do it
      <br/>
      <br/>A billing repartition entity rate is valid if
      <ul>
        <li>it is linked to a billing entity repartition</li>
        <li>it has a rate comprised between 0 and 100</li>
      </ul>
    operationId: "updateBillingRepartitionEntityRateById"
    requestBody:
      description: A perfectly formed *BillingRepartitionEntityRate*
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/BillingRepartitionEntityRate"
    responses:
      "200":
        description: "Billing repartition entity rate updated"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/BillingRepartitionEntityRate"
      "404": 
        $ref: "#/components/responses/BillingRepartitionEntityRateDoesNotExistsResponse"
      "401":
        $ref: "common_api.yaml#/components/responses/NotAuthenticatedResponse"
      "403":
        $ref: "common_api.yaml#/components/responses/NotAuthorizedResponse"
    security:
      - OAuth2:
        - write-admin

components:
  parameters:
    billingRepartitionEntityRateIdParam:
      name: id
      in: path
      required: true
      schema:
        $ref: "common_api.yaml#/components/schemas/IdLong"
  responses:
    BillingRepartitionEntityRateDoesNotExistsResponse:
      description: "Billing repartition entity rate does not exist"
  schemas:
    BillingRepartitionEntityRate:
      type: "object"
      properties:
        id:
          $ref: "common_api.yaml#/components/schemas/IdLong"
          description: Technical identifier of the billing repartition entity
        entity:
          $ref: "billing_repartition_entity_api.yaml#/components/schemas/BillingRepartitionEntity"
          description: Billing repartition entity to which this billing repartition entity rate is linked
        rate:
          type: number
          description: Rate for this billing repartition entity
    ArrayOfBillingRepartitionEntityRates:
      type: "array"
      items:
        $ref: "#/components/schemas/BillingRepartitionEntityRate"
      
