create table if not exists billing_budget_group (
    id serial PRIMARY KEY,
    name varchar(128),
    type_id integer,
    validity_start_date date,
    validity_end_date date,
    created_date date,
    created_by varchar(128),
    modified_date date,
    modified_by varchar(128),
    constraint fk_billing_budget_group_type
        foreign key (type_id)
        references billing_budget_type (id)
);