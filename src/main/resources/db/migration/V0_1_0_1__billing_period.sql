create table if not exists billing_period (
    id serial PRIMARY KEY,
    label varchar(128),
    start_date date,
    end_date date
);