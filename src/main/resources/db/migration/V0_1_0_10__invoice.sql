create table if not exists invoice (
    id serial PRIMARY KEY,
    provider_id integer,
    period_id integer,
    description varchar,
    invoice_number varchar(128),
    provider_invoice_number varchar(128),
    state_id integer,
    reception_date date,
    provider_sent_date date,
    created_date date,
    created_by varchar(128),
    modified_date date,
    modified_by varchar(128),
    constraint fk_invoice_provider
        foreign key (provider_id)
        references billing_provider (id),
    constraint fk_invoice_period
        foreign key (period_id)
        references billing_period (id),
    constraint fk_invoice_state
        foreign key (state_id)
        references invoice_state (id)
);