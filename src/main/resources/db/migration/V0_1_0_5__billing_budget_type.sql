create table if not exists billing_budget_type (
    id serial PRIMARY KEY,
    name varchar(128),
    validity_start_date date,
    validity_end_date date,
    created_date date,
    created_by varchar(128),
    modified_date date,
    modified_by varchar(128)
);