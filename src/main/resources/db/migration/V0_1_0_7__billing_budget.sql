create table if not exists billing_budget (
    id serial PRIMARY KEY,
    code varchar(128),
    name varchar(128),
    manager_id integer,
    group_id integer,
    validity_start_date date,
    validity_end_date date,
    created_date date,
    created_by varchar(128),
    modified_date date,
    modified_by varchar(128),
    constraint fk_billing_budget_group
        foreign key (group_id)
        references billing_budget_group (id)
);