create table if not exists billing_repartition_entity (
    id serial PRIMARY KEY,
    name varchar(128),
    validity_start_date date,
    validity_end_date date,
    validator_id integer,
    created_date date,
    created_by varchar(128),
    modified_date date,
    modified_by varchar(128)
);