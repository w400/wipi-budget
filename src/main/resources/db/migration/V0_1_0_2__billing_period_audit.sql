alter table billing_period add created_date date;
alter table billing_period add created_by varchar(128);
alter table billing_period add modified_date date;
alter table billing_period add modified_by varchar(128);