alter table billing_repartition_entity_rate add created_date date;
alter table billing_repartition_entity_rate add created_by varchar(128);
alter table billing_repartition_entity_rate add modified_date date;
alter table billing_repartition_entity_rate add modified_by varchar(128);