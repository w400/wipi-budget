create table if not exists billing_provider (
    id serial PRIMARY KEY,
    name varchar(128),
    contact_email varchar(512),
    created_date date,
    created_by varchar(128),
    modified_date date,
    modified_by varchar(128)
);