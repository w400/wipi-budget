create table if not exists invoice_state (
    id serial PRIMARY KEY,
    name varchar(128),
    created_date date,
    created_by varchar(128),
    modified_date date,
    modified_by varchar(128)
);