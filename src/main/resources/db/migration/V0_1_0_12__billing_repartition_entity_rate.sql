create table if not exists billing_repartition_entity_rate (
    id serial PRIMARY KEY,
    entity_id integer,
    rate numeric(3,1),
    constraint fk_billing_repartition_entity_rate_entity
        foreign key (entity_id)
        references billing_repartition_entity (id)
);