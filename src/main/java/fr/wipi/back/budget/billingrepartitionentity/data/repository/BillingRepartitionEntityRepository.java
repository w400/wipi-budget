package fr.wipi.back.budget.billingrepartitionentity.data.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;

public interface BillingRepartitionEntityRepository extends PagingAndSortingRepository<BillingRepartitionEntity, Long>, CrudRepository<BillingRepartitionEntity, Long> {
    @Query("select bre from BillingRepartitionEntity bre where bre.validityEndDate >= ?1 and bre.validityStartDate <= ?1")
    Iterable<BillingRepartitionEntity> findActivesRepartitionEntities(LocalDate currentDate);

    @Query("select bre from BillingRepartitionEntity bre where bre.validityEndDate < ?1 or bre.validityStartDate > ?1")
    Iterable<BillingRepartitionEntity> findNotActivesRepartitionEntities(LocalDate currentDate);
}
