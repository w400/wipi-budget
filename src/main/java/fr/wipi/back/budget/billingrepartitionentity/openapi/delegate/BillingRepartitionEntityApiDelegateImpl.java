package fr.wipi.back.budget.billingrepartitionentity.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;
import fr.wipi.back.budget.billingrepartitionentity.service.BillingRepartitionEntityService;
import fr.wipi.back.budget.openapi.api.BillingRepartitionEntityApiDelegate;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityDTO;

@Component
public class BillingRepartitionEntityApiDelegateImpl implements BillingRepartitionEntityApiDelegate {
    @Autowired
    BillingRepartitionEntityService service;

    @Override
    public ResponseEntity<BillingRepartitionEntityDTO> addBillingRepartitionEntity(
            BillingRepartitionEntityDTO billingRepartitionEntityDTO) {
        billingRepartitionEntityDTO.setId(service.addObject(billingRepartitionEntityDTO));
        return new ResponseEntity<>(billingRepartitionEntityDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<BillingRepartitionEntityDTO>> getBillingRepartitionEntities(Boolean active,
            Pageable pageable) {
        Iterable<BillingRepartitionEntity> repartitionEntities = service.findObjects(active, pageable);

        return new ResponseEntity<>(BillingRepartitionEntity.toDtos(repartitionEntities).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BillingRepartitionEntityDTO> getBillingRepartitionEntityById(Long id) {
        Optional<BillingRepartitionEntity> optRepartitionEntity = service.findById(id);

        if (optRepartitionEntity.isPresent()) {
            return new ResponseEntity<>(BillingRepartitionEntity.toDto(optRepartitionEntity).orElse(null),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteBillingRepartitionEntityById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<BillingRepartitionEntityDTO> updateBillingRepartitionEntityById(Long id,
            BillingRepartitionEntityDTO billingRepartitionEntityDTO) {
        if (service.updateObject(id, billingRepartitionEntityDTO)) {
            return new ResponseEntity<>(billingRepartitionEntityDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
