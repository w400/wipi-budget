package fr.wipi.back.budget.billingrepartitionentity.data.model.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BillingRepartitionEntityStartDateBeforeEndDateValidator.class)
public @interface BillingRepartitionEntityStartDateBeforeEndDateConstraint {
    String message() default "{fr.wipi.back.budget.BillingRepartitionEntityStartDateBeforeEndDate.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
