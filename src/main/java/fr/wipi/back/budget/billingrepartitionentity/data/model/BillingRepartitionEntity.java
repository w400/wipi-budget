package fr.wipi.back.budget.billingrepartitionentity.data.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.billingrepartitionentity.data.model.validator.BillingRepartitionEntityStartDateBeforeEndDateConstraint;
import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.data.model.EntityWithValidityDates;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityDTO;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@BillingRepartitionEntityStartDateBeforeEndDateConstraint
public class BillingRepartitionEntity implements EntityWithAuditFields, EntityWithValidityDates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @NotNull
    // TODO To be replaced by a manyToOne association
    private Long validatorId;

    @NotNull
    private LocalDate validityStartDate;

    private LocalDate validityEndDate;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    BillingRepartitionEntityDTO toDto() {
        BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
        dto.id(id);
        dto.name(name);
        dto.validatorId(validatorId);
        dto.validityStartDate(validityStartDate);
        dto.validityEndDate(validityEndDate);

        return dto;
    }

    public static Optional<List<BillingRepartitionEntityDTO>> toDtos(Iterable<BillingRepartitionEntity> billingRepartitionEntitys) {
        if (billingRepartitionEntitys != null) {
            List<BillingRepartitionEntityDTO> dtos = new ArrayList<>();

            billingRepartitionEntitys.forEach(provider -> dtos.add(provider.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<BillingRepartitionEntityDTO> toDto(Optional<BillingRepartitionEntity> optBillingRepartitionEntity) {
        if (optBillingRepartitionEntity.isPresent() && optBillingRepartitionEntity.get() != null) {
            return Optional.of(optBillingRepartitionEntity.get().toDto());
        }

        return Optional.empty();
    }
}
