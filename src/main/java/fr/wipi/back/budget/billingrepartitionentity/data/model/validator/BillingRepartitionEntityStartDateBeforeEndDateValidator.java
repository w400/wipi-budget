package fr.wipi.back.budget.billingrepartitionentity.data.model.validator;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;
import fr.wipi.back.budget.data.model.validator.ValidityDatesValidator;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class BillingRepartitionEntityStartDateBeforeEndDateValidator
        implements ValidityDatesValidator<BillingRepartitionEntity>,
        ConstraintValidator<BillingRepartitionEntityStartDateBeforeEndDateConstraint, BillingRepartitionEntity> {

    @Override
    public boolean isValid(BillingRepartitionEntity repartitionEntity, ConstraintValidatorContext context) {
        return isValidWithEquality(repartitionEntity);
    }

}
