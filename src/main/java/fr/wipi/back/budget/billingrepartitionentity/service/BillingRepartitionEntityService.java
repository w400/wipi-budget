package fr.wipi.back.budget.billingrepartitionentity.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;
import fr.wipi.back.budget.billingrepartitionentity.data.repository.BillingRepartitionEntityRepository;
import fr.wipi.back.budget.billingrepartitionentityrate.data.repository.BillingRepartitionEntityRateRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class BillingRepartitionEntityService
        implements WipiObjectService<BillingRepartitionEntityDTO, BillingRepartitionEntity> {

    @Autowired
    BillingRepartitionEntityRepository repository;

    @Autowired
    BillingRepartitionEntityRateRepository rateRepository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(BillingRepartitionEntity object) {
        repository.save(object);
    }

    @Override
    public Iterable<BillingRepartitionEntity> findObjects(Boolean active, Pageable pageable) {
        Iterable<BillingRepartitionEntity> providers;
        if (active != null) {
            if (active.booleanValue()) {
                providers = repository.findActivesRepartitionEntities(LocalDate.now());
            } else {
                providers = repository.findNotActivesRepartitionEntities(LocalDate.now());
            }
        } else {
            providers = repository.findAll(pageable);
        }

        return providers;
    }

    @Override
    public Optional<BillingRepartitionEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        boolean deleteSuccess = false;
        if (repository.existsById(id)) {
            if (rateRepository.findByEntityId(id).iterator().hasNext()) {
                // Cannot destroy the repartition entity because it is linked to at least one
                // repartition entity rate
                throw buildError(ApiError.BILLING_REPARTITION_ENTITY_NOT_DELETABLE_LINKED_TO_RATE, List.of("id"), null,
                        ApiError.BILLING_REPARTITION_ENTITY_NOT_DELETABLE_LINKED_TO_RATE.getTitle(), mapper);
            }

            repository.deleteById(id);

            deleteSuccess = true;
        }

        return deleteSuccess;
    }

    @Override
    public Long addObject(BillingRepartitionEntityDTO dto) {
        BillingRepartitionEntity repartitionEntity = BillingRepartitionEntity.builder().name(dto.getName())
                .validityStartDate(dto.getValidityStartDate()).validityEndDate(dto.getValidityEndDate())
                .validatorId(dto.getValidatorId())
                .build();

        validateAndSave(repartitionEntity, validator, "repartitionEntity",
                ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID,
                mapper);
        return repartitionEntity.getId();
    }

    @Override
    public boolean updateObject(Long id, BillingRepartitionEntityDTO dto) {
        boolean updateSuccess = false;
        Optional<BillingRepartitionEntity> optProvider = repository.findById(id);
        if (optProvider.isPresent()) {
            // TODO Assert there is no billing item repartition linked to this billing
            // repartition entity with not corresponding dates

            BillingRepartitionEntity provider = optProvider.get();
            provider.setName(dto.getName());
            provider.setValidatorId(dto.getValidatorId());
            provider.setValidityStartDate(dto.getValidityStartDate());
            provider.setValidityEndDate(dto.getValidityEndDate());

            validateAndSave(provider, validator, "repartitionEntity", ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID,
                    mapper);
            updateSuccess = true;
        }

        return updateSuccess;
    }

}
