package fr.wipi.back.budget.invoice.data.model.validator;

import java.util.Optional;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import fr.wipi.back.budget.billingprovider.data.repository.BillingProviderRepository;
import fr.wipi.back.budget.data.model.validator.LinkedObjectsValidityDatesValidator;
import fr.wipi.back.budget.invoice.data.model.Invoice;

@Service
public class InvoiceValidBillingProviderValidator
        implements LinkedObjectsValidityDatesValidator<Invoice, BillingProvider>,
        ConstraintValidator<InvoiceValidBillingProviderConstraint, Invoice> {

    @Autowired
    BillingProviderRepository repository;

    @Override
    public void initialize(InvoiceValidBillingProviderConstraint constraintAnnotation) {
        // Initialize mandatory for autowiring
    }

    @Override
    public boolean isValid(Invoice invoice, ConstraintValidatorContext context) {
        return checkValidity(invoice);
    }

    public Optional<BillingProvider> getLinkedObject(Invoice invoice) {
        if (invoice != null && invoice.getBaseBillingItem().getProvider() != null
                && invoice.getBaseBillingItem().getProvider().getId() != null) {
            return repository.findById(invoice.getBaseBillingItem().getProvider().getId());
        }

        return Optional.empty();
    }

}
