package fr.wipi.back.budget.invoice.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoice.service.InvoiceService;
import fr.wipi.back.budget.invoice.utils.InvoiceSearchCriteria;
import fr.wipi.back.budget.openapi.api.InvoiceApiDelegate;
import fr.wipi.back.budget.openapi.model.InvoiceDTO;

@Component
public class InvoiceApiDelegateImpl implements InvoiceApiDelegate {
    @Autowired
    InvoiceService service;

    @Override
    public ResponseEntity<InvoiceDTO> addInvoice(InvoiceDTO invoiceDTO) {
        invoiceDTO.setId(service.addObject(invoiceDTO));
        return new ResponseEntity<>(invoiceDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<InvoiceDTO>> getInvoices(String invoiceNumber, String providerInvoiceNumber,
            List<Long> state, List<String> periodCoveredDate, List<String> receptionDate, List<String> providerSentDate,
            List<String> creationDate, List<Long> provider, List<Long> period, List<Long> budget, List<Long> budgetType,
            List<Long> budgetGroup, List<Long> budgetManager, List<String> purchaseOrderNumber,
            final Pageable pageable) {
        Iterable<Invoice> invoices = service.searchObjects(InvoiceSearchCriteria.builder().invoiceNumber(invoiceNumber)
                .providerInvoiceNumber(providerInvoiceNumber).states(state).periodCoveredDates(periodCoveredDate)
                .receptionDates(receptionDate).providerSentDates(providerSentDate)
                .creationDates(creationDate).providers(provider).periods(period).budgets(budget).budgetTypes(budgetType)
                .budgetGroups(budgetGroup).budgetManagers(budgetManager).purchaseOrderNumbers(purchaseOrderNumber)
                .build(),
                pageable);

        return new ResponseEntity<>(Invoice.toDtos(invoices).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<InvoiceDTO> getInvoiceById(Long id) {
        Optional<Invoice> optInvoice = service.findById(id);

        if (optInvoice.isPresent()) {
            return new ResponseEntity<>(Invoice.toDto(optInvoice).orElse(null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteInvoiceById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<InvoiceDTO> updateInvoiceById(Long id,
            InvoiceDTO invoiceDTO) {
        if (service.updateObject(id, invoiceDTO)) {
            return new ResponseEntity<>(invoiceDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
