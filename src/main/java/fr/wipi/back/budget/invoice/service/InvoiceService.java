package fr.wipi.back.budget.invoice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingperiod.data.repository.BillingPeriodRepository;
import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import fr.wipi.back.budget.config.WipiConfig;
import fr.wipi.back.budget.data.model.BaseBillingItem;
import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoice.data.repository.InvoiceRepository;
import fr.wipi.back.budget.invoice.utils.InvoiceSearchCriteria;
import fr.wipi.back.budget.invoicestate.data.model.InvoiceState;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.InvoiceDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class InvoiceService implements WipiObjectService<InvoiceDTO, Invoice> {
    @Autowired
    InvoiceRepository repository;

    @Autowired
    BillingPeriodRepository periodRepository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    WipiConfig wipiConfig;

    @Override
    public void saveObject(Invoice object) {
        repository.save(object);
    }

    public Iterable<Invoice> searchObjects(InvoiceSearchCriteria searchCriteria, final Pageable pageable) {
        return repository.searchObjects(searchCriteria, pageable);
    }

    @Override
    public Optional<Invoice> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        boolean deleteSuccess = false;
        Optional<Invoice> optInvoice = repository.findById(id);
        if (optInvoice.isPresent()) {
            Invoice invoice = optInvoice.get();
            // Not possible to delete invoice that have been validated
            if (wipiConfig.invoiceStateValidatedId.equals(invoice.getState().getId())) {
                throw buildError(ApiError.INVOICE_VALIDATED_NOT_DELETABLE, List.of("state"), null,
                        ApiError.INVOICE_VALIDATED_NOT_DELETABLE.getTitle(), mapper);
            } else {
                // TODO Assert that there is no invoice detail line linked to this invoice (or
                // cascade ??)

                repository.deleteById(id);

                deleteSuccess = true;
            }
        }

        return deleteSuccess;
    }

    @Override
    public Long addObject(InvoiceDTO dto) {
        Invoice invoice = Invoice.builder()
                .baseBillingItem(BaseBillingItem.builder()
                        .provider(BillingProvider.builder()
                                .id(dto.getProvider() != null ? dto.getProvider().getId() : null)
                                .build())
                        .description(dto.getDescription()).build())
                .invoiceNumber(dto.getInvoiceNumber())
                .providerInvoiceNumber(dto.getProviderInvoiceNumber())
                .state(InvoiceState.builder().id(dto.getState() != null ? dto.getState().getId() : null).build())
                .receptionDate(dto.getReceptionDate())
                .providerSentDate(dto.getProviderSentDate())
                .build();

        // Mandatory to get the real period from database, otherwise the
        // InvoiceValidBillingProviderConstraint will not work (due to the
        // implementation of getValidityStartDate and getValidityEndDate in Invoice
        // class)
        if (dto.getPeriod() != null) {
            Optional<BillingPeriod> optPeriod = periodRepository.findById(dto.getPeriod().getId());

            if (optPeriod.isPresent()) {
                invoice.getBaseBillingItem().setPeriod(optPeriod.get());
            }
        }

        validateAndSave(invoice, validator, "invoice", ApiError.INVOICE_NOT_VALID, mapper);
        return invoice.getId();
    }

    @Override
    public boolean updateObject(Long id, InvoiceDTO dto) {
        Optional<Invoice> optInvoice = findById(id);
        if (optInvoice.isPresent()) {
            Invoice invoice = optInvoice.get();
            invoice.getBaseBillingItem().setProvider(
                    BillingProvider.builder().id(dto.getProvider() != null ? dto.getProvider().getId() : null)
                            .build());
            invoice.getBaseBillingItem().setDescription(dto.getDescription());
            invoice.setInvoiceNumber(dto.getInvoiceNumber());
            invoice.setProviderInvoiceNumber(dto.getProviderInvoiceNumber());
            invoice.setState(InvoiceState.builder().id(dto.getState() != null ? dto.getState().getId() : null).build());
            invoice.setReceptionDate(dto.getReceptionDate());
            invoice.setProviderSentDate(dto.getProviderSentDate());

            // Mandatory to get the real period from database, otherwise the
            // InvoiceValidBillingProviderConstraint will not work (due to the
            // implementation of getValidityStartDate and getValidityEndDate in Invoice
            // class)
            if (dto.getPeriod() != null) {
                Optional<BillingPeriod> optPeriod = periodRepository.findById(dto.getPeriod().getId());

                if (optPeriod.isPresent()) {
                    invoice.getBaseBillingItem().setPeriod(optPeriod.get());
                } else {
                    invoice.getBaseBillingItem().setPeriod(null);
                }
            } else {
                invoice.getBaseBillingItem().setPeriod(null);
            }

            validateAndSave(invoice, validator, "invoice", ApiError.INVOICE_NOT_VALID,
                    mapper);
            return true;
        } else {
            return false;
        }
    }

}
