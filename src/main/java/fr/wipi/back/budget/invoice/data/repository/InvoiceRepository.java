package fr.wipi.back.budget.invoice.data.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoice.utils.InvoiceSearchCriteria;

public interface InvoiceRepository extends PagingAndSortingRepository<Invoice, Long>, CrudRepository<Invoice, Long>,
        JpaSpecificationExecutor<Invoice> {
    default Page<Invoice> searchObjects(InvoiceSearchCriteria criteria, Pageable pageable) {
        List<Specification<Invoice>> specificationsToTest = new ArrayList<>();

        if (StringUtils.isNotEmpty(criteria.getInvoiceNumber())) {
            specificationsToTest.add(InvoiceSpecifications.invoiceNumberContains(criteria.getInvoiceNumber()));
        }

        if (StringUtils.isNotEmpty(criteria.getProviderInvoiceNumber())) {
                specificationsToTest.add(InvoiceSpecifications
                        .providerInvoiceNumberContains(criteria.getProviderInvoiceNumber()));
        }

        if (criteria.getStates() != null && !criteria.getStates().isEmpty()) {
            specificationsToTest.add(InvoiceSpecifications.statesIn(criteria.getStates()));
        }

        // TODO Implement periodCoveredDates (when invoice linked to invoice detail
        // lines)

        if (criteria.getReceptionDates() != null && !criteria.getReceptionDates().isEmpty()) {
            specificationsToTest.add(InvoiceSpecifications.receptionDates(criteria.getReceptionDates()));
        }

        if (criteria.getProviderSentDates() != null && !criteria.getProviderSentDates().isEmpty()) {
            specificationsToTest.add(InvoiceSpecifications.providerSentDates(criteria.getProviderSentDates()));
        }

        if (criteria.getCreationDates() != null && !criteria.getCreationDates().isEmpty()) {
            specificationsToTest.add(InvoiceSpecifications.creationDates(criteria.getCreationDates()));
        }

        if (criteria.getProviders() != null && !criteria.getProviders().isEmpty()) {
            specificationsToTest.add(InvoiceSpecifications.providersIn(criteria.getProviders()));
        }

        if (criteria.getPeriods() != null && !criteria.getPeriods().isEmpty()) {
            specificationsToTest.add(InvoiceSpecifications.periodsIn(criteria.getPeriods()));
        }

        // TODO Implement budgets (when invoice linked to invoice detail lines)
        // TODO Implement budget groups (when invoice linked to invoice detail lines)
        // TODO Implement budget types (when invoice linked to invoice detail lines)
        // TODO Implement budget managers (when invoice linked to invoice detail lines)
        // TODO Implement purchase order numbers (when invoice linked to purchase orders)

        Specification<Invoice> spec = Specification.allOf(specificationsToTest);
        return findAll(spec, pageable);
    }
}