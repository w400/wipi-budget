package fr.wipi.back.budget.invoice.data.model.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import fr.wipi.back.budget.invoice.data.model.Invoice;

public class InvoiceProviderSentDateBeforeReceptionDateValidator
        implements ConstraintValidator<InvoiceProviderSentDateBeforeReceptionDateConstraint, Invoice> {

    @Override
    public boolean isValid(Invoice invoice, ConstraintValidatorContext context) {
        if (invoice != null && invoice.getProviderSentDate() != null) {
            if (invoice.getReceptionDate() == null) {
                return true;
            } else {
                return (invoice.getProviderSentDate().isBefore(invoice.getReceptionDate())
                        || invoice.getProviderSentDate().equals(invoice.getReceptionDate()));
            }
        }

        return false;
    }

}
