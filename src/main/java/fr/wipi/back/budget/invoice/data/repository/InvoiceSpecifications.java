package fr.wipi.back.budget.invoice.data.repository;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import fr.wipi.back.budget.data.utils.ConjunctionBuilderLocalDate;
import fr.wipi.back.budget.invoice.data.model.Invoice;

public final class InvoiceSpecifications {
    private static final ConjunctionBuilderLocalDate<Invoice> CONJUCTION_BUILDER_LOCAL_DATE = new ConjunctionBuilderLocalDate<>();
    private static final String BASE_BILLING_ITEM_NAME = "baseBillingItem";

    private InvoiceSpecifications() {
    }

    private static String contains(String expression) {
        return MessageFormat.format("%{0}%", expression);
    }

    public static Specification<Invoice> invoiceNumberContains(String invoiceNumber) {
        return (root, query, builder) -> builder.like(root.get("invoiceNumber"), contains(invoiceNumber));
    }

    public static Specification<Invoice> providerInvoiceNumberContains(String providerInvoiceNumber) {
        return (root, query, builder) -> builder.like(root.get("providerInvoiceNumber"),
                contains(providerInvoiceNumber));
    }

    public static Specification<Invoice> statesIn(List<Long> statesIds) {
        return (root, query, builder) -> builder.in(root.join("state").get("id")).value(statesIds);
    }

    public static Specification<Invoice> receptionDates(List<String> receptionDates) {
        return CONJUCTION_BUILDER_LOCAL_DATE.buildConjunction(receptionDates, List.of("receptionDate"));
    }

    public static Specification<Invoice> providerSentDates(List<String> providerSentDates) {
        return CONJUCTION_BUILDER_LOCAL_DATE.buildConjunction(providerSentDates, List.of("providerSentDate"));
    }

    public static Specification<Invoice> creationDates(List<String> creationDates) {
        return CONJUCTION_BUILDER_LOCAL_DATE.buildConjunction(creationDates, List.of(BASE_BILLING_ITEM_NAME, "baseEntityAudit", "createdDate"));
    }

    public static Specification<Invoice> providersIn(List<Long> providersIds) {
        return (root, query, builder) -> builder.in(root.get(BASE_BILLING_ITEM_NAME).get("provider").get("id")).value(providersIds);
    }

    public static Specification<Invoice> periodsIn(List<Long> periodsIds) {
        return (root, query, builder) -> builder.in(root.get(BASE_BILLING_ITEM_NAME).get("period").get("id")).value(periodsIds);
    }
}
