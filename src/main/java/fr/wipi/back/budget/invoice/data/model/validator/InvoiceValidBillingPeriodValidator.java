package fr.wipi.back.budget.invoice.data.model.validator;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingperiod.data.repository.BillingPeriodRepository;
import fr.wipi.back.budget.data.model.validator.ExistingLinkedObjectValidator;
import fr.wipi.back.budget.invoice.data.model.Invoice;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

@Service
public class InvoiceValidBillingPeriodValidator
        implements ExistingLinkedObjectValidator<Invoice, BillingPeriod>,
        ConstraintValidator<InvoiceValidBillingPeriodConstraint, Invoice> {

    @Autowired
    BillingPeriodRepository repository;

    @Override
    public void initialize(InvoiceValidBillingPeriodConstraint constraintAnnotation) {
        // Initialize mandatory for autowiring
    }

    @Override
    public boolean isValid(Invoice invoice, ConstraintValidatorContext context) {
        return checkValidity(invoice);
    }

    public Optional<BillingPeriod> getLinkedObject(Invoice invoice) {
        if (invoice != null && invoice.getBaseBillingItem().getPeriod() != null
                && invoice.getBaseBillingItem().getPeriod().getId() != null) {
            return repository.findById(invoice.getBaseBillingItem().getPeriod().getId());
        }

        return Optional.empty();
    }

}
