package fr.wipi.back.budget.invoice.data.model.validator;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wipi.back.budget.data.model.validator.ExistingLinkedObjectValidator;
import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoicestate.data.model.InvoiceState;
import fr.wipi.back.budget.invoicestate.data.repository.InvoiceStateRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

@Service
public class InvoiceValidInvoiceStateValidator
        implements ExistingLinkedObjectValidator<Invoice, InvoiceState>, ConstraintValidator<InvoiceValidInvoiceStateConstraint, Invoice> {

    @Autowired
    InvoiceStateRepository repository;

    @Override
    public void initialize(InvoiceValidInvoiceStateConstraint constraintAnnotation) {
        // Initialize mandatory for autowiring
    }

    @Override
    public boolean isValid(Invoice invoice, ConstraintValidatorContext context) {
        return checkValidity(invoice);
    }

    public Optional<InvoiceState> getLinkedObject(Invoice invoice) {
        if (invoice != null && invoice.getState() != null && invoice.getState().getId() != null) {
            return repository.findById(invoice.getState().getId());
        }

        return Optional.empty();
    }

}
