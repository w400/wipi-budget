package fr.wipi.back.budget.invoice.data.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import fr.wipi.back.budget.data.model.BaseBillingItem;
import fr.wipi.back.budget.data.model.EntityWithBaseBillingItem;
import fr.wipi.back.budget.data.model.EntityWithValidityDates;
import fr.wipi.back.budget.invoice.data.model.validator.InvoiceProviderSentDateBeforeReceptionDateConstraint;
import fr.wipi.back.budget.invoice.data.model.validator.InvoiceValidBillingPeriodConstraint;
import fr.wipi.back.budget.invoice.data.model.validator.InvoiceValidBillingProviderConstraint;
import fr.wipi.back.budget.invoice.data.model.validator.InvoiceValidInvoiceStateConstraint;
import fr.wipi.back.budget.invoicestate.data.model.InvoiceState;
import fr.wipi.back.budget.openapi.model.BillingPeriodDTO;
import fr.wipi.back.budget.openapi.model.BillingProviderDTO;
import fr.wipi.back.budget.openapi.model.InvoiceDTO;
import fr.wipi.back.budget.openapi.model.InvoiceStateDTO;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@InvoiceValidBillingProviderConstraint
@InvoiceProviderSentDateBeforeReceptionDateConstraint
@InvoiceValidBillingPeriodConstraint
@InvoiceValidInvoiceStateConstraint
public class Invoice implements EntityWithBaseBillingItem, EntityWithValidityDates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String invoiceNumber;

    @NotBlank
    private String providerInvoiceNumber;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "state_id", referencedColumnName = "id")
    private InvoiceState state;

    @NotNull
    private LocalDate receptionDate;

    private LocalDate providerSentDate;

    @Embedded
    @Builder.Default
    @Valid
    public BaseBillingItem baseBillingItem = new BaseBillingItem();

    public BaseBillingItem getBaseBillingItem() {
        return baseBillingItem;
    }

    @Override
    public LocalDate getValidityStartDate() {
        if (getBaseBillingItem() != null && getBaseBillingItem().getPeriod() != null) {
            return getBaseBillingItem().getPeriod().getValidityStartDate();
        }

        return null;
    }

    @Override
    public LocalDate getValidityEndDate() {
        if (getBaseBillingItem() != null && getBaseBillingItem().getPeriod() != null) {
            return getBaseBillingItem().getPeriod().getValidityEndDate();
        }

        return null;
    }

    InvoiceDTO toDto() {
        InvoiceDTO dto = new InvoiceDTO();
        dto.id(id);
        if (getBaseBillingItem().getProvider() != null) {
            Optional<BillingProviderDTO> optProviderDto = BillingProvider.toDto(Optional.of(getBaseBillingItem().getProvider()));
            dto.provider(optProviderDto.isPresent() ? optProviderDto.get() : null);
        }
        if (getBaseBillingItem().getPeriod() != null) {
            Optional<BillingPeriodDTO> optPeriodDto = BillingPeriod.toDto(Optional.of(getBaseBillingItem().getPeriod()));
            dto.period(optPeriodDto.isPresent() ? optPeriodDto.get() : null);
        }
        dto.description(getBaseBillingItem().getDescription());
        dto.invoiceNumber(invoiceNumber);
        dto.providerInvoiceNumber(providerInvoiceNumber);
        if (state != null) {
            Optional<InvoiceStateDTO> optStateDto = InvoiceState.toDto(Optional.of(state));
            dto.state(optStateDto.isPresent() ? optStateDto.get() : null);
        }
        dto.receptionDate(receptionDate);
        dto.providerSentDate(providerSentDate);
        dto.creationDate(getBaseBillingItem().getBaseEntityAudit().createdDate);

        return dto;
    }

    public static Optional<List<InvoiceDTO>> toDtos(Iterable<Invoice> invoices) {
        if (invoices != null) {
            List<InvoiceDTO> dtos = new ArrayList<>();

            invoices.forEach(invoice -> dtos.add(invoice.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<InvoiceDTO> toDto(Optional<Invoice> optInvoice) {
        if (optInvoice.isPresent() && optInvoice.get() != null) {
            return Optional.of(optInvoice.get().toDto());
        }

        return Optional.empty();
    }
}
