package fr.wipi.back.budget.invoice.data.model.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = InvoiceValidInvoiceStateValidator.class)
public @interface InvoiceValidInvoiceStateConstraint {
    String message() default "{fr.wipi.back.budget.InvoiceValidInvoiceState.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
