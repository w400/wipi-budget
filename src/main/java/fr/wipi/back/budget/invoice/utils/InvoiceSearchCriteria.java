package fr.wipi.back.budget.invoice.utils;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoiceSearchCriteria {
    String invoiceNumber;
    String providerInvoiceNumber;
    List<Long> states;
    List<String> periodCoveredDates;
    List<String> receptionDates;
    List<String> providerSentDates;
    List<String> creationDates;
    List<Long> providers;
    List<Long> periods;
    List<Long> budgets;
    List<Long> budgetTypes;
    List<Long> budgetGroups;
    List<Long> budgetManagers;
    List<String> purchaseOrderNumbers;
}
