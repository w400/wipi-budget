package fr.wipi.back.budget.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration()
public class WipiConfig {
    @Value("${wipi.invoice.state.validated.id}")
    public Long invoiceStateValidatedId;
}
