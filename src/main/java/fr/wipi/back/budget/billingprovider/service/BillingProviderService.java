package fr.wipi.back.budget.billingprovider.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import fr.wipi.back.budget.billingprovider.data.repository.BillingProviderRepository;
import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoice.data.repository.InvoiceRepository;
import fr.wipi.back.budget.invoice.data.repository.InvoiceSpecifications;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingProviderDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class BillingProviderService implements WipiObjectService<BillingProviderDTO, BillingProvider> {
    @Autowired
    BillingProviderRepository repository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(BillingProvider object) {
        repository.save(object);
    }

    @Override
    public Iterable<BillingProvider> findObjects(Boolean active, Pageable pageable) {
        Iterable<BillingProvider> providers;
        if (active != null) {
            if (active.booleanValue()) {
                providers = repository.findActivesProviders(LocalDate.now());
            } else {
                providers = repository.findNotActivesProviders(LocalDate.now());
            }
        } else {
            providers = repository.findAll(pageable);
        }

        return providers;
    }

    @Override
    public Optional<BillingProvider> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        boolean deleteSuccess = false;
        if (repository.existsById(id)) {
            // Assert there is no invoice linked to this billing provider
            List<Invoice> invoicesList = invoiceRepository.findAll(InvoiceSpecifications.providersIn(List.of(id)));
            if (!invoicesList.isEmpty()) {
                throw buildError(ApiError.BILLING_PROVIDER_NOT_DELETABLE_LINKED_TO_INVOICE, List.of("id"), null,
                        ApiError.BILLING_PROVIDER_NOT_DELETABLE_LINKED_TO_INVOICE.getTitle(), mapper);
            }
            // TODO Assert that there is no purchase order linked to this billing provider

            repository.deleteById(id);

            deleteSuccess = true;
        }

        return deleteSuccess;
    }

    @Override
    public Long addObject(BillingProviderDTO dto) {
        BillingProvider provider = BillingProvider.builder().name(dto.getName())
                .contactEmail(dto.getContactEmail())
                .validityStartDate(dto.getValidityStartDate()).validityEndDate(dto.getValidityEndDate())
                .build();

        validateAndSave(provider, validator, "provider", ApiError.BILLING_PROVIDER_NOT_VALID, mapper);
        return provider.getId();
    }

    @Override
    public boolean updateObject(Long id, BillingProviderDTO dto) {
        boolean updateSuccess = false;
        Optional<BillingProvider> optProvider = repository.findById(id);
        if (optProvider.isPresent()) {
            // Assert there is no invoice linked to this billing provider
            // TODO When invoice linked to invoice details, verify if at least one invoice details is not covered by the new validity period of the billing provider
            List<Invoice> invoicesList = invoiceRepository.findAll(InvoiceSpecifications.providersIn(List.of(id)));
            if (!invoicesList.isEmpty()) {
                throw buildError(ApiError.BILLING_PROVIDER_NOT_UPDATABLE_LINKED_TO_INVOICE, List.of("id"), null,
                        ApiError.BILLING_PROVIDER_NOT_UPDATABLE_LINKED_TO_INVOICE.getTitle(), mapper);
            }
            // TODO Assert that there is no purchase order linked to this billing provider

            BillingProvider provider = optProvider.get();
            provider.setName(dto.getName());
            provider.setContactEmail(dto.getContactEmail());
            provider.setValidityStartDate(dto.getValidityStartDate());
            provider.setValidityEndDate(dto.getValidityEndDate());

            validateAndSave(provider, validator, "provider", ApiError.BILLING_PROVIDER_NOT_VALID, mapper);
            updateSuccess = true;
        }

        return updateSuccess;
    }

}
