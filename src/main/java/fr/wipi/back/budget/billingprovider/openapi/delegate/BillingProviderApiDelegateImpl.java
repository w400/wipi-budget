package fr.wipi.back.budget.billingprovider.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import fr.wipi.back.budget.billingprovider.service.BillingProviderService;
import fr.wipi.back.budget.openapi.api.BillingProviderApiDelegate;
import fr.wipi.back.budget.openapi.model.BillingProviderDTO;

@Component
public class BillingProviderApiDelegateImpl implements BillingProviderApiDelegate {
    @Autowired
    BillingProviderService service;

    @Override
    public ResponseEntity<BillingProviderDTO> addBillingProvider(BillingProviderDTO billingProviderDTO) {
        billingProviderDTO.setId(service.addObject(billingProviderDTO));
        return new ResponseEntity<>(billingProviderDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<BillingProviderDTO>> getBillingProviders(Boolean active, Pageable pageable) {
        Iterable<BillingProvider> providers = service.findObjects(active, pageable);

        return new ResponseEntity<>(BillingProvider.toDtos(providers).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BillingProviderDTO> getBillingProviderById(Long id) {
        Optional<BillingProvider> optProvider = service.findById(id);

        if (optProvider.isPresent()) {
            return new ResponseEntity<>(BillingProvider.toDto(optProvider).orElse(null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteBillingProviderById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<BillingProviderDTO> updateBillingProviderById(Long id,
            BillingProviderDTO billingProviderDTO) {
        if (service.updateObject(id, billingProviderDTO)) {
            return new ResponseEntity<>(billingProviderDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
