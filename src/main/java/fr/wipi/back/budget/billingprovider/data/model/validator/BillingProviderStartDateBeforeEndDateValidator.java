package fr.wipi.back.budget.billingprovider.data.model.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import fr.wipi.back.budget.data.model.validator.ValidityDatesValidator;

public class BillingProviderStartDateBeforeEndDateValidator
        implements ValidityDatesValidator<BillingProvider>,
        ConstraintValidator<BillingProviderStartDateBeforeEndDateConstraint, BillingProvider> {

    @Override
    public boolean isValid(BillingProvider provider, ConstraintValidatorContext context) {
        return isValidWithEquality(provider);
    }

}
