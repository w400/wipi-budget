package fr.wipi.back.budget.billingprovider.data.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;

public interface BillingProviderRepository extends PagingAndSortingRepository<BillingProvider, Long>, CrudRepository<BillingProvider, Long> {

    @Query("select bp from BillingProvider bp where bp.validityEndDate >= ?1 and bp.validityStartDate <= ?1")
    Iterable<BillingProvider> findActivesProviders(LocalDate currentDate);

    @Query("select bp from BillingProvider bp where bp.validityEndDate < ?1 or bp.validityStartDate > ?1")
    Iterable<BillingProvider> findNotActivesProviders(LocalDate currentDate);
}
