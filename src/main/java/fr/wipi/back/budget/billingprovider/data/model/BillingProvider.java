package fr.wipi.back.budget.billingprovider.data.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.billingprovider.data.model.validator.BillingProviderStartDateBeforeEndDateConstraint;
import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.data.model.EntityWithValidityDates;
import fr.wipi.back.budget.openapi.model.BillingProviderDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@BillingProviderStartDateBeforeEndDateConstraint
public class BillingProvider implements EntityWithAuditFields, EntityWithValidityDates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    @Email
    private String contactEmail;

    @NotNull
    private LocalDate validityStartDate;

    private LocalDate validityEndDate;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    BillingProviderDTO toDto() {
        BillingProviderDTO dto = new BillingProviderDTO();
        dto.id(id);
        dto.name(name);
        dto.contactEmail(contactEmail);
        dto.validityStartDate(validityStartDate);
        dto.validityEndDate(validityEndDate);

        return dto;
    }

    public static Optional<List<BillingProviderDTO>> toDtos(Iterable<BillingProvider> billingProviders) {
        if (billingProviders != null) {
            List<BillingProviderDTO> dtos = new ArrayList<>();

            billingProviders.forEach(provider -> dtos.add(provider.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<BillingProviderDTO> toDto(Optional<BillingProvider> optBillingProvider) {
        if (optBillingProvider.isPresent() && optBillingProvider.get() != null) {
            return Optional.of(optBillingProvider.get().toDto());
        }

        return Optional.empty();
    }
}
