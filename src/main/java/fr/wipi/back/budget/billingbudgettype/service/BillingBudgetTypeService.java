package fr.wipi.back.budget.billingbudgettype.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;
import fr.wipi.back.budget.billingbudgettype.data.repository.BillingBudgetTypeRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingBudgetTypeDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class BillingBudgetTypeService implements WipiObjectService<BillingBudgetTypeDTO, BillingBudgetType> {
    @Autowired
    BillingBudgetTypeRepository repository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(BillingBudgetType object) {
        repository.save(object);
    }

    @Override
    public Iterable<BillingBudgetType> findObjects(Boolean active, Pageable pageable) {
        Iterable<BillingBudgetType> budgetTypes;
        if (active != null) {
            if (active.booleanValue()) {
                budgetTypes = repository.findActivesBudgetTypes(LocalDate.now());
            } else {
                budgetTypes = repository.findNotActivesBudgetTypes(LocalDate.now());
            }
        } else {
            budgetTypes = repository.findAll(pageable);
        }

        return budgetTypes;
    }

    @Override
    public Optional<BillingBudgetType> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        if (repository.existsById(id)) {
            // TODO Assert that there is no budget group linked to this budget type

            repository.deleteById(id);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public Long addObject(BillingBudgetTypeDTO dto) {
        BillingBudgetType budgetType = BillingBudgetType.builder().name(dto.getName())
                .validityStartDate(dto.getValidityStartDate())
                .validityEndDate(dto.getValidityEndDate())
                .build();

        validateAndSave(budgetType, validator, "budgetType", ApiError.BILLING_BUDGET_TYPE_NOT_VALID, mapper);
        return budgetType.getId();
    }

    @Override
    public boolean updateObject(Long id, BillingBudgetTypeDTO dto) {
        Optional<BillingBudgetType> optBudgetType = repository.findById(id);
        if (optBudgetType.isPresent()) {
            // TODO Assert that there is no budget group linked to this budget type for
            // which the period covered by the budget type validity dates includes the
            // period covered by the budget group validity dates

            BillingBudgetType budgetType = optBudgetType.get();
            budgetType.setName(dto.getName());
            budgetType.setValidityStartDate(dto.getValidityStartDate());
            budgetType.setValidityEndDate(dto.getValidityEndDate());

            validateAndSave(budgetType, validator, "budgetType", ApiError.BILLING_BUDGET_TYPE_NOT_VALID, mapper);
            return true;
        } else {
            return false;
        }
    }

}
