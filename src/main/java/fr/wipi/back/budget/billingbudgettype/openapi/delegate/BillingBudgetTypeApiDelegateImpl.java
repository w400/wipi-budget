package fr.wipi.back.budget.billingbudgettype.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;
import fr.wipi.back.budget.billingbudgettype.service.BillingBudgetTypeService;
import fr.wipi.back.budget.openapi.api.BillingBudgetTypeApiDelegate;
import fr.wipi.back.budget.openapi.model.BillingBudgetTypeDTO;

@Component
public class BillingBudgetTypeApiDelegateImpl implements BillingBudgetTypeApiDelegate {
    @Autowired
    BillingBudgetTypeService service;

    @Override
    public ResponseEntity<BillingBudgetTypeDTO> addBillingBudgetType(BillingBudgetTypeDTO billingBudgetTypeDTO) {
        billingBudgetTypeDTO.setId(service.addObject(billingBudgetTypeDTO));
        return new ResponseEntity<>(billingBudgetTypeDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<BillingBudgetTypeDTO>> getBillingBudgetTypes(Boolean active, Pageable pageable) {
        Iterable<BillingBudgetType> budgetTypes = service.findObjects(active, pageable);

        return new ResponseEntity<>(BillingBudgetType.toDtos(budgetTypes).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BillingBudgetTypeDTO> getBillingBudgetTypeById(Long id) {
        Optional<BillingBudgetType> optBudgetType = service.findById(id);

        if (optBudgetType.isPresent()) {
            return new ResponseEntity<>(BillingBudgetType.toDto(optBudgetType).orElse(null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteBillingBudgetTypeById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<BillingBudgetTypeDTO> updateBillingBudgetTypeById(Long id,
            BillingBudgetTypeDTO billingBudgetTypeDTO) {
        if (service.updateObject(id, billingBudgetTypeDTO)) {
            return new ResponseEntity<>(billingBudgetTypeDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
