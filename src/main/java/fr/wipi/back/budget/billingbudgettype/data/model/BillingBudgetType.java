package fr.wipi.back.budget.billingbudgettype.data.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.billingbudgettype.data.model.validator.BillingBudgetTypeStartDateBeforeEndDateConstraint;
import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.data.model.EntityWithValidityDates;
import fr.wipi.back.budget.openapi.model.BillingBudgetTypeDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@BillingBudgetTypeStartDateBeforeEndDateConstraint
public class BillingBudgetType implements EntityWithAuditFields, EntityWithValidityDates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @NotNull
    private LocalDate validityStartDate;

    private LocalDate validityEndDate;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    BillingBudgetTypeDTO toDto() {
        BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
        dto.id(id);
        dto.name(name);
        dto.validityStartDate(validityStartDate);
        dto.validityEndDate(validityEndDate);

        return dto;
    }

    public static Optional<List<BillingBudgetTypeDTO>> toDtos(Iterable<BillingBudgetType> billingBudgetTypes) {
        if (billingBudgetTypes != null) {
            List<BillingBudgetTypeDTO> dtos = new ArrayList<>();

            billingBudgetTypes.forEach(budgetType -> dtos.add(budgetType.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<BillingBudgetTypeDTO> toDto(Optional<BillingBudgetType> optBillingBudgetType) {
        if (optBillingBudgetType.isPresent() && optBillingBudgetType.get() != null) {
            return Optional.of(optBillingBudgetType.get().toDto());
        }

        return Optional.empty();
    }
}
