package fr.wipi.back.budget.billingbudgettype.data.model.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;
import fr.wipi.back.budget.data.model.validator.ValidityDatesValidator;

public class BillingBudgetTypeStartDateBeforeEndDateValidator
        implements ValidityDatesValidator<BillingBudgetType>,
        ConstraintValidator<BillingBudgetTypeStartDateBeforeEndDateConstraint, BillingBudgetType> {

    @Override
    public boolean isValid(BillingBudgetType budgetType, ConstraintValidatorContext context) {
        return isValidWithEquality(budgetType);
    }

}
