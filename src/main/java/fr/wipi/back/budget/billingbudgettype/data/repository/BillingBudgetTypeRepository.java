package fr.wipi.back.budget.billingbudgettype.data.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;

public interface BillingBudgetTypeRepository extends PagingAndSortingRepository<BillingBudgetType, Long>, CrudRepository<BillingBudgetType, Long> {

    @Query("select bp from BillingBudgetType bp where bp.validityEndDate >= ?1 and bp.validityStartDate <= ?1")
    Iterable<BillingBudgetType> findActivesBudgetTypes(LocalDate currentDate);

    @Query("select bp from BillingBudgetType bp where bp.validityEndDate < ?1 or bp.validityStartDate > ?1")
    Iterable<BillingBudgetType> findNotActivesBudgetTypes(LocalDate currentDate);
}
