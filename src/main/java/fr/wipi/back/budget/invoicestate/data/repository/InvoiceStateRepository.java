package fr.wipi.back.budget.invoicestate.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.invoicestate.data.model.InvoiceState;

public interface InvoiceStateRepository extends PagingAndSortingRepository<InvoiceState, Long>, CrudRepository<InvoiceState, Long> {

}
