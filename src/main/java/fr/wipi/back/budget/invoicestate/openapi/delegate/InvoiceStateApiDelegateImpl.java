package fr.wipi.back.budget.invoicestate.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.invoicestate.data.model.InvoiceState;
import fr.wipi.back.budget.invoicestate.service.InvoiceStateService;
import fr.wipi.back.budget.openapi.api.InvoiceStateApiDelegate;
import fr.wipi.back.budget.openapi.model.InvoiceStateDTO;

@Component
public class InvoiceStateApiDelegateImpl implements InvoiceStateApiDelegate {
    @Autowired
    InvoiceStateService service;

    @Override
    public ResponseEntity<InvoiceStateDTO> addInvoiceState(InvoiceStateDTO invoiceStateDTO) {
        invoiceStateDTO.setId(service.addObject(invoiceStateDTO));
        return new ResponseEntity<>(invoiceStateDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<InvoiceStateDTO>> getInvoiceStates(Boolean active, Pageable pageable) {
        Iterable<InvoiceState> invoiceStates = service.findObjects(active, pageable);

        return new ResponseEntity<>(InvoiceState.toDtos(invoiceStates).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<InvoiceStateDTO> getInvoiceStateById(Long id) {
        Optional<InvoiceState> optInvoiceState = service.findById(id);

        if (optInvoiceState.isPresent()) {
            return new ResponseEntity<>(InvoiceState.toDto(optInvoiceState).orElse(null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteInvoiceStateById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<InvoiceStateDTO> updateInvoiceStateById(Long id,
            InvoiceStateDTO invoiceStateDTO) {
        if (service.updateObject(id, invoiceStateDTO)) {
            return new ResponseEntity<>(invoiceStateDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
