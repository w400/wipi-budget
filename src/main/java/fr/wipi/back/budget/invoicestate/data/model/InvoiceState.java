package fr.wipi.back.budget.invoicestate.data.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.openapi.model.InvoiceStateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceState implements EntityWithAuditFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    InvoiceStateDTO toDto() {
        InvoiceStateDTO dto = new InvoiceStateDTO();
        dto.id(id);
        dto.name(name);

        return dto;
    }

    public static Optional<List<InvoiceStateDTO>> toDtos(Iterable<InvoiceState> invoiceStates) {
        if (invoiceStates != null) {
            List<InvoiceStateDTO> dtos = new ArrayList<>();

            invoiceStates.forEach(budgetGroup -> dtos.add(budgetGroup.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<InvoiceStateDTO> toDto(Optional<InvoiceState> optInvoiceState) {
        if (optInvoiceState.isPresent() && optInvoiceState.get() != null) {
            return Optional.of(optInvoiceState.get().toDto());
        }

        return Optional.empty();
    }
}
