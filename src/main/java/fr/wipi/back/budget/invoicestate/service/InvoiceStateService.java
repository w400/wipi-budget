package fr.wipi.back.budget.invoicestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoice.data.repository.InvoiceRepository;
import fr.wipi.back.budget.invoice.data.repository.InvoiceSpecifications;
import fr.wipi.back.budget.invoicestate.data.model.InvoiceState;
import fr.wipi.back.budget.invoicestate.data.repository.InvoiceStateRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.InvoiceStateDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class InvoiceStateService implements WipiObjectService<InvoiceStateDTO, InvoiceState> {
    @Autowired
    InvoiceStateRepository repository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(InvoiceState object) {
        repository.save(object);
    }

    @Override
    public Iterable<InvoiceState> findObjects(Boolean active, Pageable pageable) {
        Iterable<InvoiceState> invoiceStates;
        if (active != null) {
            throw buildError(ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING, List.of("active"), null,
                    "active parameter not managed", mapper);
        } else {
            invoiceStates = repository.findAll(pageable);
        }

        return invoiceStates;
    }

    @Override
    public Optional<InvoiceState> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        boolean deleteSuccess = false;
        if (repository.existsById(id)) {
            // Assert that there is no invoice linked to this invoice state
            List<Invoice> invoicesList = invoiceRepository.findAll(InvoiceSpecifications.statesIn(List.of(id)));
            if (!invoicesList.isEmpty()) {
                throw buildError(ApiError.INVOICE_STATE_NOT_DELETABLE_LINKED_TO_INVOICE, List.of("id"), null,
                        ApiError.INVOICE_STATE_NOT_DELETABLE_LINKED_TO_INVOICE.getTitle(), mapper);
            }

            repository.deleteById(id);

            deleteSuccess = true;
        }
        return deleteSuccess;
    }

    @Override
    public Long addObject(InvoiceStateDTO dto) {
        InvoiceState invoice = InvoiceState.builder().name(dto.getName())
                .build();

        validateAndSave(invoice, validator, "invoiceState", ApiError.INVOICE_STATE_NOT_VALID, mapper);
        return invoice.getId();
    }

    @Override
    public boolean updateObject(Long id, InvoiceStateDTO dto) {
        Optional<InvoiceState> optInvoiceState = findById(id);
        if (optInvoiceState.isPresent()) {
            InvoiceState budget = optInvoiceState.get();
            budget.setName(dto.getName());

            validateAndSave(budget, validator, "invoiceState", ApiError.INVOICE_STATE_NOT_VALID,
                    mapper);
            return true;
        } else {
            return false;
        }
    }

}
