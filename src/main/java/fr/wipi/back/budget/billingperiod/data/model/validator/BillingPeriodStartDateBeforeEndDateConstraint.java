package fr.wipi.back.budget.billingperiod.data.model.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BillingPeriodStartDateBeforeEndDateValidator.class)
public @interface BillingPeriodStartDateBeforeEndDateConstraint {
    String message() default "{fr.wipi.back.budget.BillingPeriodStartDateBeforeEndDate.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
