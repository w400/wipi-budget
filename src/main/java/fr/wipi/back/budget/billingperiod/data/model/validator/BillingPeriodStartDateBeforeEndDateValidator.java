package fr.wipi.back.budget.billingperiod.data.model.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.data.model.validator.ValidityDatesValidator;

public class BillingPeriodStartDateBeforeEndDateValidator
        implements ValidityDatesValidator<BillingPeriod>,
        ConstraintValidator<BillingPeriodStartDateBeforeEndDateConstraint, BillingPeriod> {

    @Override
    public boolean isValid(BillingPeriod period, ConstraintValidatorContext context) {
        return isValidWithoutEquality(period);
    }

}
