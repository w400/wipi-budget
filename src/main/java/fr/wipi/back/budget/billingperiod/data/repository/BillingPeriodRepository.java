package fr.wipi.back.budget.billingperiod.data.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;

public interface BillingPeriodRepository extends PagingAndSortingRepository<BillingPeriod, Long>, CrudRepository<BillingPeriod, Long> {
    
    @Query("select bp from BillingPeriod bp where bp.endDate >= ?1 and bp.startDate <= ?2")
    Iterable<BillingPeriod> findOverlappingPeriods(LocalDate startDate, LocalDate endDate);

    @Query("select bp from BillingPeriod bp where bp.endDate >= ?1 and bp.startDate <= ?1")
    Iterable<BillingPeriod> findActivesPeriods(LocalDate currentDate);
}
