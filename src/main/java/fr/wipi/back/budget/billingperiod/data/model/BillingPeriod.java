package fr.wipi.back.budget.billingperiod.data.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.billingperiod.data.model.validator.BillingPeriodNotOverlappingConstraint;
import fr.wipi.back.budget.billingperiod.data.model.validator.BillingPeriodStartDateBeforeEndDateConstraint;
import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.data.model.EntityWithValidityDates;
import fr.wipi.back.budget.openapi.model.BillingPeriodDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@BillingPeriodStartDateBeforeEndDateConstraint
@BillingPeriodNotOverlappingConstraint
public class BillingPeriod implements EntityWithAuditFields, EntityWithValidityDates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String label;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    public LocalDate getValidityStartDate() {
        return this.startDate;
    }
    
    public LocalDate getValidityEndDate() {
        return this.endDate;
    }

    BillingPeriodDTO toDto() {
        BillingPeriodDTO dto = new BillingPeriodDTO();
        dto.id(id);
        dto.name(label);
        dto.startDate(startDate);
        dto.endDate(endDate);

        return dto;
    }

    public static Optional<List<BillingPeriodDTO>> toDtos(Iterable<BillingPeriod> billingPeriods) {
        if (billingPeriods != null) {
            List<BillingPeriodDTO> dtos = new ArrayList<>();

            billingPeriods.forEach(period -> dtos.add(period.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<BillingPeriodDTO> toDto(Optional<BillingPeriod> optBillingPeriod) {
        if (optBillingPeriod.isPresent() && optBillingPeriod.get() != null) {
            return Optional.of(optBillingPeriod.get().toDto());
        }

        return Optional.empty();
    }
}
