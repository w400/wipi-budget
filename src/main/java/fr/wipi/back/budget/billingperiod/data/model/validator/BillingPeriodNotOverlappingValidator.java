package fr.wipi.back.budget.billingperiod.data.model.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingperiod.data.repository.BillingPeriodRepository;

@Service
public class BillingPeriodNotOverlappingValidator
        implements ConstraintValidator<BillingPeriodNotOverlappingConstraint, BillingPeriod> {

    @Autowired
    private BillingPeriodRepository repository;

    @Override
    public void initialize(BillingPeriodNotOverlappingConstraint constraintAnnotation) {
        //Initialize mandatory for autowiring
    }

    @Override
    public boolean isValid(BillingPeriod period, ConstraintValidatorContext context) {
        if(period != null && period.getStartDate() != null && period.getEndDate() != null) {
            return !repository.findOverlappingPeriods(period.getStartDate(), period.getEndDate()).iterator().hasNext();
        }

        return false;
    }

}
