package fr.wipi.back.budget.billingperiod.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingperiod.data.repository.BillingPeriodRepository;
import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoice.data.repository.InvoiceRepository;
import fr.wipi.back.budget.invoice.data.repository.InvoiceSpecifications;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingPeriodDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class BillingPeriodService implements WipiObjectService<BillingPeriodDTO, BillingPeriod> {
    @Autowired
    BillingPeriodRepository repository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(BillingPeriod object) {
        repository.save(object);
    }

    @Override
    public Iterable<BillingPeriod> findObjects(Boolean active, Pageable pageable) {
        Iterable<BillingPeriod> periods;
        if (active != null && active.booleanValue()) {
            periods = repository.findActivesPeriods(LocalDate.now());
        } else {
            periods = repository.findAll(pageable);
        }

        return periods;
    }

    @Override
    public Optional<BillingPeriod> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        boolean deleteSuccess = false;
        if (repository.existsById(id)) {
            // Assert there is no invoice linked to this period
            List<Invoice> invoicesList = invoiceRepository.findAll(InvoiceSpecifications.periodsIn(List.of(id)));
            if (!invoicesList.isEmpty()) {
                throw buildError(ApiError.BILLING_PERIOD_NOT_DELETABLE_LINKED_TO_INVOICE, List.of("id"), null,
                        ApiError.BILLING_PERIOD_NOT_DELETABLE_LINKED_TO_INVOICE.getTitle(), mapper);
            }
            // TODO Assert that there is no purchase order linked to this period

            repository.deleteById(id);

            deleteSuccess = true;
        }

        return deleteSuccess;
    }

    @Override
    public Long addObject(BillingPeriodDTO dto) {
        BillingPeriod period = BillingPeriod.builder().label(dto.getName())
                .startDate(dto.getStartDate()).endDate(dto.getEndDate())
                .build();

        validateAndSave(period, validator, "period", ApiError.BILLING_PERIOD_NOT_VALID, mapper);
        return period.getId();
    }

    @Override
    public boolean updateObject(Long id, BillingPeriodDTO dto) {
        boolean updateSuccess = false;
        Optional<BillingPeriod> optPeriod = repository.findById(id);
        if (optPeriod.isPresent()) {
            // Assert there is no invoice linked to this period
            // TODO When invoice linked to invoice details, verify if at least one invoice
            // details is not covered by the new validity period of the billing period
            List<Invoice> invoicesList = invoiceRepository.findAll(InvoiceSpecifications.periodsIn(List.of(id)));
            if (!invoicesList.isEmpty()) {
                throw buildError(ApiError.BILLING_PERIOD_NOT_UPDATABLE_LINKED_TO_INVOICE, List.of("id"), null,
                        ApiError.BILLING_PERIOD_NOT_UPDATABLE_LINKED_TO_INVOICE.getTitle(), mapper);
            }
            // TODO Assert that there is no purchase order linked to this period

            BillingPeriod period = optPeriod.get();
            period.setLabel(dto.getName());
            period.setStartDate(dto.getStartDate());
            period.setEndDate(dto.getEndDate());

            validateAndSave(period, validator, "period", ApiError.BILLING_PERIOD_NOT_VALID, mapper);
            updateSuccess = true;
        }

        return updateSuccess;
    }

}
