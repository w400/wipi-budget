package fr.wipi.back.budget.billingperiod.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingperiod.service.BillingPeriodService;
import fr.wipi.back.budget.openapi.api.BillingPeriodApiDelegate;
import fr.wipi.back.budget.openapi.model.BillingPeriodDTO;

@Component
public class BillingPeriodApiDelegateImpl implements BillingPeriodApiDelegate {

    @Autowired
    BillingPeriodService service;

    @Override
    public ResponseEntity<BillingPeriodDTO> addBillingPeriod(BillingPeriodDTO billingPeriodDTO) {
        billingPeriodDTO.setId(service.addObject(billingPeriodDTO));
        return new ResponseEntity<>(billingPeriodDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<BillingPeriodDTO>> getBillingPeriods(Boolean active, Pageable pageable) {
        Iterable<BillingPeriod> periods = service.findObjects(active, pageable);

        return new ResponseEntity<>(BillingPeriod.toDtos(periods).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BillingPeriodDTO> getBillingPeriodById(Long id) {
        Optional<BillingPeriod> optPeriod = service.findById(id);

        if (optPeriod.isPresent()) {
            return new ResponseEntity<>(BillingPeriod.toDto(optPeriod).orElse(null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteBillingPeriodById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<BillingPeriodDTO> updateBillingPeriodById(Long id, BillingPeriodDTO billingPeriodDTO) {
        if (service.updateObject(id, billingPeriodDTO)) {
            return new ResponseEntity<>(billingPeriodDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
