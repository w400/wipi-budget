package fr.wipi.back.budget.billingbudget.data.model.validator;

import java.util.Optional;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wipi.back.budget.billingbudget.data.model.BillingBudget;
import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.billingbudgetgroup.data.repository.BillingBudgetGroupRepository;
import fr.wipi.back.budget.data.model.validator.LinkedObjectsValidityDatesValidator;

@Service
public class BillingBudgetValidBudgetGroupValidator
        implements LinkedObjectsValidityDatesValidator<BillingBudget, BillingBudgetGroup>, ConstraintValidator<BillingBudgetValidBudgetGroupConstraint, BillingBudget> {

    @Autowired
    BillingBudgetGroupRepository repository;

    @Override
    public void initialize(BillingBudgetValidBudgetGroupConstraint constraintAnnotation) {
        // Initialize mandatory for autowiring
    }

    @Override
    public boolean isValid(BillingBudget budget, ConstraintValidatorContext context) {
        return checkValidity(budget);
    }

    public Optional<BillingBudgetGroup> getLinkedObject(BillingBudget budget) {
        if (budget != null && budget.getGroup() != null && budget.getGroup().getId() != null) {
            return repository.findById(budget.getGroup().getId());
        }

        return Optional.empty();
    }

}
