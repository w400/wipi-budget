package fr.wipi.back.budget.billingbudget.data.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.billingbudget.data.model.BillingBudget;

public interface BillingBudgetRepository extends PagingAndSortingRepository<BillingBudget, Long>, CrudRepository<BillingBudget, Long> {

    @Query("select bp from BillingBudget bp where bp.validityEndDate >= ?1 and bp.validityStartDate <= ?1")
    Iterable<BillingBudget> findActivesBudgets(LocalDate currentDate);

    @Query("select bp from BillingBudget bp where bp.validityEndDate < ?1 or bp.validityStartDate > ?1")
    Iterable<BillingBudget> findNotActivesBudgets(LocalDate currentDate);
}
