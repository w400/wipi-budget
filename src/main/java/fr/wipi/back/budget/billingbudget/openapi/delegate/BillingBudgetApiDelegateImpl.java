package fr.wipi.back.budget.billingbudget.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.billingbudget.data.model.BillingBudget;
import fr.wipi.back.budget.billingbudget.service.BillingBudgetService;
import fr.wipi.back.budget.openapi.api.BillingBudgetApiDelegate;
import fr.wipi.back.budget.openapi.model.BillingBudgetDTO;

@Component
public class BillingBudgetApiDelegateImpl
        implements BillingBudgetApiDelegate {
    @Autowired
    BillingBudgetService service;

    @Override
    public ResponseEntity<BillingBudgetDTO> addBillingBudget(BillingBudgetDTO billingBudgetDTO) {
        billingBudgetDTO.setId(service.addObject(billingBudgetDTO));
        return new ResponseEntity<>(billingBudgetDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<BillingBudgetDTO>> getBillingBudgets(Boolean active, Pageable pageable) {
        Iterable<BillingBudget> budgets = service.findObjects(active, pageable);

        return new ResponseEntity<>(BillingBudget.toDtos(budgets).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BillingBudgetDTO> getBillingBudgetById(Long id) {
        Optional<BillingBudget> optBudget = service.findById(id);

        if (optBudget.isPresent()) {
            return new ResponseEntity<>(BillingBudget.toDto(optBudget).orElse(null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteBillingBudgetById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<BillingBudgetDTO> updateBillingBudgetById(Long id,
            BillingBudgetDTO billingBudgetDTO) {
        if (service.updateObject(id, billingBudgetDTO)) {
            return new ResponseEntity<>(billingBudgetDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}