package fr.wipi.back.budget.billingbudget.data.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.billingbudget.data.model.validator.BillingBudgetStartDateBeforeEndDateConstraint;
import fr.wipi.back.budget.billingbudget.data.model.validator.BillingBudgetValidBudgetGroupConstraint;
import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.data.model.EntityWithValidityDates;
import fr.wipi.back.budget.openapi.model.BillingBudgetDTO;
import fr.wipi.back.budget.openapi.model.BillingBudgetGroupDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@BillingBudgetStartDateBeforeEndDateConstraint
@BillingBudgetValidBudgetGroupConstraint
public class BillingBudget implements EntityWithAuditFields, EntityWithValidityDates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String code;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    private BillingBudgetGroup group;

    @NotNull
    // TODO To be replaced by a manyToOne association
    private Long managerId;

    @NotNull
    private LocalDate validityStartDate;

    private LocalDate validityEndDate;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    BillingBudgetDTO toDto() {
        BillingBudgetDTO dto = new BillingBudgetDTO();
        dto.id(id);
        dto.name(name);
        dto.validityStartDate(validityStartDate);
        dto.validityEndDate(validityEndDate);
        if (group != null) {
            Optional<BillingBudgetGroupDTO> optGroupDto = BillingBudgetGroup.toDto(Optional.of(group));
            dto.setGroup(optGroupDto.isPresent() ? optGroupDto.get() : null);
        }

        return dto;
    }

    public static Optional<List<BillingBudgetDTO>> toDtos(Iterable<BillingBudget> billingBudgets) {
        if (billingBudgets != null) {
            List<BillingBudgetDTO> dtos = new ArrayList<>();

            billingBudgets.forEach(budgetGroup -> dtos.add(budgetGroup.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<BillingBudgetDTO> toDto(Optional<BillingBudget> optBillingBudget) {
        if (optBillingBudget.isPresent() && optBillingBudget.get() != null) {
            return Optional.of(optBillingBudget.get().toDto());
        }

        return Optional.empty();
    }
}
