package fr.wipi.back.budget.billingbudget.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.billingbudget.data.model.BillingBudget;
import fr.wipi.back.budget.billingbudget.data.repository.BillingBudgetRepository;
import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingBudgetDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class BillingBudgetService implements WipiObjectService<BillingBudgetDTO, BillingBudget> {
    @Autowired
    BillingBudgetRepository repository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(BillingBudget object) {
        repository.save(object);
    }

    @Override
    public Optional<BillingBudget> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        if (repository.existsById(id)) {
            // TODO Assert that there is no purchased order or invoice linked to this budget

            repository.deleteById(id);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public Iterable<BillingBudget> findObjects(Boolean active, Pageable pageable) {
        Iterable<BillingBudget> budgets;
        if (active != null) {
            if (active.booleanValue()) {
                budgets = repository.findActivesBudgets(LocalDate.now());
            } else {
                budgets = repository.findNotActivesBudgets(LocalDate.now());
            }
        } else {
            budgets = repository.findAll(pageable);
        }

        return budgets;
    }

    @Override
    public Long addObject(BillingBudgetDTO dto) {
        BillingBudget budget = BillingBudget.builder().name(dto.getName()).code(dto.getCode())
                .validityStartDate(dto.getValidityStartDate())
                .validityEndDate(dto.getValidityEndDate())
                .managerId(dto.getManagerId())
                .group(BillingBudgetGroup.builder()
                        .id(dto.getGroup() != null ? dto.getGroup().getId() : null)
                        .build())
                .build();

        validateAndSave(budget, validator, "budget", ApiError.BILLING_BUDGET_NOT_VALID, mapper);
        return budget.getId();
    }

    @Override
    public boolean updateObject(Long id, BillingBudgetDTO dto) {
        Optional<BillingBudget> optBudget = findById(id);
        if (optBudget.isPresent()) {
            // TODO Assert that there is no purchase order or invoice linked to this budget

            BillingBudget budget = optBudget.get();
            budget.setName(dto.getName());
            budget.setCode(dto.getCode());
            budget.setValidityStartDate(dto.getValidityStartDate());
            budget.setValidityEndDate(dto.getValidityEndDate());
            budget.setManagerId(dto.getManagerId());
            budget.setGroup(BillingBudgetGroup.builder()
                    .id(dto.getGroup() != null ? dto.getGroup().getId() : null)
                    .build());

            validateAndSave(budget, validator, "budget", ApiError.BILLING_BUDGET_NOT_VALID,
                    mapper);
            return true;
        } else {
            return false;
        }

    }
}
