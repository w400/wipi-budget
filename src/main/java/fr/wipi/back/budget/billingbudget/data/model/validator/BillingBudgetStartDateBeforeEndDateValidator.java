package fr.wipi.back.budget.billingbudget.data.model.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import fr.wipi.back.budget.billingbudget.data.model.BillingBudget;
import fr.wipi.back.budget.data.model.validator.ValidityDatesValidator;

public class BillingBudgetStartDateBeforeEndDateValidator
        implements ValidityDatesValidator<BillingBudget>,
        ConstraintValidator<BillingBudgetStartDateBeforeEndDateConstraint, BillingBudget> {

    @Override
    public boolean isValid(BillingBudget budget, ConstraintValidatorContext context) {
        return isValidWithEquality(budget);
    }

}
