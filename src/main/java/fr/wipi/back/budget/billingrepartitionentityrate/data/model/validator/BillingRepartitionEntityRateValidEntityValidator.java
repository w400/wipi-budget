package fr.wipi.back.budget.billingrepartitionentityrate.data.model.validator;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;
import fr.wipi.back.budget.billingrepartitionentity.data.repository.BillingRepartitionEntityRepository;
import fr.wipi.back.budget.billingrepartitionentityrate.data.model.BillingRepartitionEntityRate;
import fr.wipi.back.budget.data.model.validator.ExistingLinkedObjectValidator;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

@Service
public class BillingRepartitionEntityRateValidEntityValidator
        implements ExistingLinkedObjectValidator<BillingRepartitionEntityRate, BillingRepartitionEntity>, ConstraintValidator<BillingRepartitionEntityRateValidEntityConstraint, BillingRepartitionEntityRate> {

    @Autowired
    BillingRepartitionEntityRepository repository;

    @Override
    public void initialize(BillingRepartitionEntityRateValidEntityConstraint constraintAnnotation) {
        // Initialize mandatory for autowiring
    }

    @Override
    public boolean isValid(BillingRepartitionEntityRate repartitionEntityRate, ConstraintValidatorContext context) {
        return checkValidity(repartitionEntityRate);
    }

    public Optional<BillingRepartitionEntity> getLinkedObject(BillingRepartitionEntityRate repartitionEntityRate) {
        if (repartitionEntityRate != null && repartitionEntityRate.getEntity() != null && repartitionEntityRate.getEntity().getId() != null) {
            return repository.findById(repartitionEntityRate.getEntity().getId());
        }

        return Optional.empty();
    }

}
