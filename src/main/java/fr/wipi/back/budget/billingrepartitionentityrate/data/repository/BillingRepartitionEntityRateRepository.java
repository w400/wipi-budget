package fr.wipi.back.budget.billingrepartitionentityrate.data.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.billingrepartitionentityrate.data.model.BillingRepartitionEntityRate;

public interface BillingRepartitionEntityRateRepository extends PagingAndSortingRepository<BillingRepartitionEntityRate, Long>, CrudRepository<BillingRepartitionEntityRate, Long> {
    @Query("select brer from BillingRepartitionEntityRate brer where brer.entity.validityEndDate >= ?1 and brer.entity.validityStartDate <= ?1")
    Iterable<BillingRepartitionEntityRate> findActivesRepartitionEntityRates(LocalDate currentDate);

    @Query("select brer from BillingRepartitionEntityRate brer where brer.entity.validityEndDate < ?1 or brer.entity.validityStartDate > ?1")
    Iterable<BillingRepartitionEntityRate> findNotActivesRepartitionEntityRates(LocalDate currentDate);

    Iterable<BillingRepartitionEntityRate> findByEntityId(Long id);
}
