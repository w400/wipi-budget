package fr.wipi.back.budget.billingrepartitionentityrate.data.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;
import fr.wipi.back.budget.billingrepartitionentityrate.data.model.validator.BillingRepartitionEntityRateValidEntityConstraint;
import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityDTO;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityRateDTO;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@BillingRepartitionEntityRateValidEntityConstraint
public class BillingRepartitionEntityRate implements EntityWithAuditFields {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "entity_id", referencedColumnName = "id")
    private BillingRepartitionEntity entity;

    @Min(0)
    @Max(100)
    @NotNull
    private BigDecimal rate;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    BillingRepartitionEntityRateDTO toDto() {
        BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
        dto.id(id);
        dto.rate(rate);
        if(entity != null) {
            Optional<BillingRepartitionEntityDTO> optEntityDto = BillingRepartitionEntity.toDto(Optional.of(entity));
            dto.setEntity(optEntityDto.isPresent() ? optEntityDto.get() : null);
        }

        return dto;
    }

    public static Optional<List<BillingRepartitionEntityRateDTO>> toDtos(Iterable<BillingRepartitionEntityRate> billingRepartitionEntityRates) {
        if (billingRepartitionEntityRates != null) {
            List<BillingRepartitionEntityRateDTO> dtos = new ArrayList<>();

            billingRepartitionEntityRates.forEach(provider -> dtos.add(provider.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<BillingRepartitionEntityRateDTO> toDto(Optional<BillingRepartitionEntityRate> optBillingRepartitionEntity) {
        if (optBillingRepartitionEntity.isPresent() && optBillingRepartitionEntity.get() != null) {
            return Optional.of(optBillingRepartitionEntity.get().toDto());
        }

        return Optional.empty();
    }
}
