package fr.wipi.back.budget.billingrepartitionentityrate.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.billingrepartitionentityrate.data.model.BillingRepartitionEntityRate;
import fr.wipi.back.budget.billingrepartitionentityrate.service.BillingRepartitionEntityRateService;
import fr.wipi.back.budget.openapi.api.BillingRepartitionEntityRateApiDelegate;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityRateDTO;

@Component
public class BillingRepartitionEntityRateApiDelegateImpl implements BillingRepartitionEntityRateApiDelegate {

    @Autowired
    BillingRepartitionEntityRateService service;

    @Override
    public ResponseEntity<BillingRepartitionEntityRateDTO> addBillingRepartitionEntityRate(
            BillingRepartitionEntityRateDTO billingRepartitionEntityRateDTO) {
        billingRepartitionEntityRateDTO.setId(service.addObject(billingRepartitionEntityRateDTO));
        return new ResponseEntity<>(billingRepartitionEntityRateDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<BillingRepartitionEntityRateDTO>> getBillingRepartitionEntityRates(Boolean active,
            Pageable pageable) {
        Iterable<BillingRepartitionEntityRate> repartitionEntityRates = service.findObjects(active, pageable);

        return new ResponseEntity<>(BillingRepartitionEntityRate.toDtos(repartitionEntityRates).orElse(null),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BillingRepartitionEntityRateDTO> getBillingRepartitionEntityRateById(Long id) {
        Optional<BillingRepartitionEntityRate> optRepartitionEntityRate = service.findById(id);

        if (optRepartitionEntityRate.isPresent()) {
            return new ResponseEntity<>(BillingRepartitionEntityRate.toDto(optRepartitionEntityRate).orElse(null),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteBillingRepartitionEntityRateById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<BillingRepartitionEntityRateDTO> updateBillingRepartitionEntityRateById(Long id,
            BillingRepartitionEntityRateDTO billingRepartitionEntityRateDTO) {
        if (service.updateObject(id, billingRepartitionEntityRateDTO)) {
            return new ResponseEntity<>(billingRepartitionEntityRateDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
