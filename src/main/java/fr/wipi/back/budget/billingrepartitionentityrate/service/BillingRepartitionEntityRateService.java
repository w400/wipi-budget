package fr.wipi.back.budget.billingrepartitionentityrate.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;
import fr.wipi.back.budget.billingrepartitionentityrate.data.model.BillingRepartitionEntityRate;
import fr.wipi.back.budget.billingrepartitionentityrate.data.repository.BillingRepartitionEntityRateRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityRateDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class BillingRepartitionEntityRateService
        implements WipiObjectService<BillingRepartitionEntityRateDTO, BillingRepartitionEntityRate> {

    @Autowired
    BillingRepartitionEntityRateRepository repository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(BillingRepartitionEntityRate object) {
        repository.save(object);
    }

    @Override
    public Iterable<BillingRepartitionEntityRate> findObjects(Boolean active, Pageable pageable) {
        Iterable<BillingRepartitionEntityRate> rates;
        if (active != null) {
            if (active.booleanValue()) {
                rates = repository.findActivesRepartitionEntityRates(LocalDate.now());
            } else {
                rates = repository.findNotActivesRepartitionEntityRates(LocalDate.now());
            }
        } else {
            rates = repository.findAll(pageable);
        }

        return rates;
    }

    @Override
    public Optional<BillingRepartitionEntityRate> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public Long addObject(BillingRepartitionEntityRateDTO dto) {
        BillingRepartitionEntityRate repartitionEntityRate = BillingRepartitionEntityRate.builder()
                .entity(BillingRepartitionEntity.builder().id(dto.getEntity() != null ? dto.getEntity().getId() : null)
                        .build())
                .rate(dto.getRate())
                .build();

        validateAndSave(repartitionEntityRate, validator, "repartitionEntityRate",
                ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID,
                mapper);
        return repartitionEntityRate.getId();
    }

    @Override
    public boolean updateObject(Long id, BillingRepartitionEntityRateDTO dto) {
        Optional<BillingRepartitionEntityRate> optBillingRepartitionEntityRate = findById(id);
        if (optBillingRepartitionEntityRate.isPresent()) {
            BillingRepartitionEntityRate billingRepartitionEntityRate = optBillingRepartitionEntityRate.get();
            billingRepartitionEntityRate.setRate(dto.getRate());
            billingRepartitionEntityRate.setEntity(BillingRepartitionEntity.builder()
                    .id(dto.getEntity() != null ? dto.getEntity().getId() : null)
                    .build());

            validateAndSave(billingRepartitionEntityRate, validator, "repartitionEntityRate",
                    ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID,
                    mapper);
            return true;
        } else {
            return false;
        }
    }

}
