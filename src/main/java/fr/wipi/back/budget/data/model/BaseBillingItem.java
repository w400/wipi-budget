package fr.wipi.back.budget.data.model;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Embedded;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseBillingItem implements EntityWithAuditFields {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    private BillingProvider provider;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "period_id", referencedColumnName = "id")
    private BillingPeriod period;

    @NotBlank
    private String description;
    
    @Embedded
    @Builder.Default
    public BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }
}