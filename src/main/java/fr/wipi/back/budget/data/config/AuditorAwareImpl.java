package fr.wipi.back.budget.data.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        // TODO To be replaced by the "real" user when implementing Spring security
        return Optional.of("Admin");
    }
}