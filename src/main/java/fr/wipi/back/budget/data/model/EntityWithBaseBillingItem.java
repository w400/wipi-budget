package fr.wipi.back.budget.data.model;

public interface EntityWithBaseBillingItem { 
    BaseBillingItem getBaseBillingItem(); 
  }