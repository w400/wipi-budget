package fr.wipi.back.budget.data.model;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Embeddable
public class BaseEntityAudit { 
  @Column(name = "created_date", nullable = false, updatable = false) 
  @CreatedDate 
  public LocalDate createdDate; 

  @Column(name = "created_by") 
  @CreatedBy 
  public String createdBy; 

  @Column(name = "modified_date") 
  @LastModifiedDate 
  public LocalDate modifiedDate; 

  @Column(name = "modified_by") 
  @LastModifiedBy 
  public String modifiedBy;
  
}