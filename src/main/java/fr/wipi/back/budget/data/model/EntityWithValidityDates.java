package fr.wipi.back.budget.data.model;

import java.time.LocalDate;

public interface EntityWithValidityDates { 
    LocalDate getValidityStartDate();
    
    LocalDate getValidityEndDate(); 
  }