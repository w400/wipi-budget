package fr.wipi.back.budget.data.model.validator;

import java.util.Optional;

public interface ExistingLinkedObjectValidator<C, P>  {
    
    public Optional<P> getLinkedObject(C child);

    default boolean checkValidity(C child) {
        Optional<P> optParent = getLinkedObject(child);

        return optParent.isPresent();
    }
}
