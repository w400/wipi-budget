package fr.wipi.back.budget.data.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.domain.Specification;

import fr.wipi.back.budget.openapi.RHSOperands;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

public class ConjunctionBuilderLocalDate<B> {
    private static Map<RHSOperands, String> extractFilters(List<String> filters) {
        Map<RHSOperands, String> extractedFilters = new EnumMap<>(RHSOperands.class);
        for (String filter : filters) {
            if (filter.contains(":")) {
                String operandStr = filter.substring(0, filter.indexOf(":"));
                RHSOperands operand = RHSOperands.valueOfByOperatorIgnoreError(operandStr);
                extractedFilters.put(operand, filter.substring(filter.indexOf(":") + 1));
            } else {
                extractedFilters.put(RHSOperands.STRICTLY_EQUAL_TO, filter);
            }
        }

        return extractedFilters;
    }

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static LocalDate transformStringToLocalDate(String dateStr) {
        return LocalDate.parse(dateStr, formatter);
    }

    private <X> Path<X> getAttribute(Root<B> root, List<String> attributesPath) {
        Path<X> p = null;
        for (String attribute : attributesPath) {
            if (p == null) {
                p = root.get(attribute);
            } else {
                p = p.get(attribute);
            }
        }

        return p;
    }

    public Specification<B> buildConjunction(List<String> filters, List<String> attributesPath) {
        return (root, query, builder) -> {
            Map<RHSOperands, String> extractedFilters = extractFilters(filters);
            Predicate predicate = builder.conjunction();

            for (Map.Entry<RHSOperands, String> filter : extractedFilters.entrySet()) {
                switch (filter.getKey()) {
                    case GREATER_THAN:
                        predicate = builder.and(predicate, builder.greaterThan(getAttribute(root, attributesPath),
                                transformStringToLocalDate(filter.getValue())));
                        break;
                    case GREATER_THAN_OR_EQUAL_TO:
                        predicate = builder.and(predicate,
                                builder.greaterThanOrEqualTo(getAttribute(root, attributesPath),
                                        transformStringToLocalDate(filter.getValue())));
                        break;
                    case LESS_THAN:
                        predicate = builder.and(predicate, builder.lessThan(getAttribute(root, attributesPath),
                                transformStringToLocalDate(filter.getValue())));
                        break;
                    case LESS_THAN_OR_EQUAL_TO:
                        predicate = builder.and(predicate,
                                builder.lessThanOrEqualTo(getAttribute(root, attributesPath),
                                        transformStringToLocalDate(filter.getValue())));
                        break;
                    case STRICTLY_EQUAL_TO:
                    default:
                        predicate = builder.and(predicate,
                                builder.equal(getAttribute(root, attributesPath),
                                        transformStringToLocalDate(filter.getValue())));
                        break;
                }
            }

            return predicate;
        };
    }
}