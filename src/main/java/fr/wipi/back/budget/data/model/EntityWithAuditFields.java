package fr.wipi.back.budget.data.model;

public interface EntityWithAuditFields { 
    BaseEntityAudit getBaseEntityAudit(); 
  }