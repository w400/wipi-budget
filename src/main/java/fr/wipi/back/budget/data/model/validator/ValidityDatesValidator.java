package fr.wipi.back.budget.data.model.validator;

import fr.wipi.back.budget.data.model.EntityWithValidityDates;

public interface ValidityDatesValidator<T extends EntityWithValidityDates> {

    public default boolean isValidWithEquality(T object) {
        if (object != null && object.getValidityStartDate() != null) {
            if (object.getValidityEndDate() == null) {
                return true;
            } else {
                return (object.getValidityStartDate().isBefore(object.getValidityEndDate())
                        || object.getValidityStartDate().equals(object.getValidityEndDate()));
            }
        }

        return false;
    }

    public default boolean isValidWithoutEquality(T object) {
        return object != null && object.getValidityStartDate() != null && object.getValidityEndDate() != null
                && object.getValidityStartDate().isBefore(object.getValidityEndDate());
    }
}
