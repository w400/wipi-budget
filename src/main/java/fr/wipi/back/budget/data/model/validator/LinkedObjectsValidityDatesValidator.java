package fr.wipi.back.budget.data.model.validator;

import java.util.Optional;

import fr.wipi.back.budget.data.model.EntityWithValidityDates;

public interface LinkedObjectsValidityDatesValidator<C extends EntityWithValidityDates, P extends EntityWithValidityDates>  {
    
    public Optional<P> getLinkedObject(C child);

    default boolean checkValidity(C child) {
        Optional<P> optParent = getLinkedObject(child);

        if (optParent.isPresent()) {
            P parent = optParent.get();
            return checkValidityStartDate(parent, child) && (checkNoValidityEndDates(parent, child)
                    || checkValidityEndDates(parent, child));
        }
        return false;
    }

    private boolean checkValidityStartDate(P parent, C child) {
        return (child.getValidityStartDate() != null
                && (parent.getValidityStartDate().isBefore(child.getValidityStartDate())
                        || parent.getValidityStartDate().equals(child.getValidityStartDate())));
    }

    private boolean checkNoValidityEndDates(P parent, C child) {
        return (child.getValidityEndDate() == null && parent.getValidityEndDate() == null);
    }

    private boolean checkValidityEndDates(P parent, C child) {
        return (child.getValidityEndDate() != null
                && (parent.getValidityEndDate() == null
                        || parent.getValidityEndDate().isAfter(child.getValidityEndDate())
                        || parent.getValidityEndDate().equals(child.getValidityEndDate())));
    }
}
