package fr.wipi.back.budget.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.error.ApiExceptionHandler;
import fr.wipi.back.budget.openapi.model.ApiErrorResponseDTO;

public interface WipiObjectService<S, T> {
    void saveObject(T object);

    default Iterable<T> findObjects(Boolean active, Pageable pageable) {
        return null;
    }

    Optional<T> findById(Long id);

    boolean tryDelete(Long id);

    Long addObject(S dto);

    boolean updateObject(Long id, S dto);

    public default void validateAndSave(T object, SmartValidator validator, String objectName, ApiError error,
            ObjectMapper mapper) {
        Errors errors = getErrors(object, validator, objectName);

        if (errors.getErrorCount() > 0) {
            ApiErrorResponseDTO apiError = ApiExceptionHandler.buildErrorDto(error,
                    HttpStatus.BAD_REQUEST, errors);

            try {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, mapper.writeValueAsString(apiError));
            } catch (JsonProcessingException e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            saveObject(object);
        }
    }

    public default Errors getErrors(T object, SmartValidator validator, String objectName) {
        Errors errors = new DirectFieldBindingResult(object, objectName);
        validator.validate(object, errors);

        return errors;
    }

    public default ResponseStatusException buildError(ApiError error, List<String> incorrectFields, List<String> notValidatedConstraints,
            String detail, ObjectMapper mapper) {
        ApiErrorResponseDTO apiError = ApiExceptionHandler.buildErrorDto(error, incorrectFields,
                notValidatedConstraints, detail);
        try {
            return new ResponseStatusException(HttpStatus.BAD_REQUEST, mapper.writeValueAsString(apiError));
        } catch (JsonProcessingException e) {
            return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
