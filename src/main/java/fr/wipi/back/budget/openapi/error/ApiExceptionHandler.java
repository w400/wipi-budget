package fr.wipi.back.budget.openapi.error;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.ApiErrorResponseDTO;

@RestControllerAdvice
public class ApiExceptionHandler {

    @Autowired
    ObjectMapper mapper;

    public static ApiErrorResponseDTO buildErrorDto(ApiError errorEnum, HttpStatus status, Errors errors) {
        ApiErrorResponseDTO apiError = new ApiErrorResponseDTO();
        apiError.setType(errorEnum.getType());
        apiError.setTitle(errorEnum.getTitle());
        apiError.setStatus(status.value());
        apiError.incorrectFields(new ArrayList<>(2));
        apiError.notValidatedConstraints(new ArrayList<>(2));

        // TODO Implémenter la gestion des instances d'erreur

        StringBuilder errorBuilder = new StringBuilder();
        for (ObjectError error : errors.getAllErrors()) {
            if (error instanceof FieldError fieldError) {
                errorBuilder.append(fieldError.getField()).append(" ").append(error.getDefaultMessage()).append("\n");
                apiError.addIncorrectFieldsItem(fieldError.getField());
            } else {
                errorBuilder.append(error.getDefaultMessage()).append("\n");

                List<String> errorCodes = Arrays.asList(error.getCodes());
                if (errorCodes != null && errorCodes.size() >= 2) {
                    apiError.addNotValidatedConstraintsItem(errorCodes.get(1));
                }
            }
        }
        apiError.setDetail(errorBuilder.toString());

        return apiError;
    }

    public static ApiErrorResponseDTO buildErrorDto(ApiError errorEnum, List<String> incorrectFields,
            List<String> notValidatedConstraints, String detail) {
        ApiErrorResponseDTO apiError = new ApiErrorResponseDTO();
        apiError.type(errorEnum.getType());
        apiError.title(errorEnum.getTitle());
        apiError.status(HttpStatus.BAD_REQUEST.value());
        apiError.incorrectFields(incorrectFields);
        apiError.notValidatedConstraints(notValidatedConstraints);
        apiError.detail(detail);

        return apiError;
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ApiErrorResponseDTO> handleResponseStatusException(ResponseStatusException ex) {
        ApiErrorResponseDTO error = null;
        try {
            error = mapper.readValue(ex.getReason(), ApiErrorResponseDTO.class);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(error, ex.getStatusCode());
    }
}
