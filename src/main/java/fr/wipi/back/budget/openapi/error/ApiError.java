package fr.wipi.back.budget.openapi.error;

import lombok.Getter;

@Getter
public enum ApiError {
    BILLING_PERIOD_NOT_VALID("BILLING_PERIOD_NOT_VALID", "Not a valid billing period"),
    BILLING_PROVIDER_NOT_VALID("BILLING_PROVIDER_NOT_VALID", "Not a valid billing provider"),
    BILLING_BUDGET_TYPE_NOT_VALID("BILLING_BUDGET_TYPE_NOT_VALID", "Not a valid billing budget type"),
    BILLING_BUDGET_GROUP_NOT_VALID("BILLING_BUDGET_GROUP_NOT_VALID", "Not a valid billing budget group"),
    BILLING_BUDGET_NOT_VALID("BILLING_BUDGET_NOT_VALID", "Not a valid billing budget"),
    INVOICE_STATE_NOT_VALID("INVOICE_STATE_NOT_VALID","Not a valid invoice state"),
    ACTIVE_PARAM_NOT_MANAGED_IN_LISTING("ACTIVE_PARAM_NOT_MANAGED_IN_LISTING", "The active parameter is not managed on this entity listing"),
    PURCHASE_ORDER_STATE_NOT_VALID("PURCHASE_ORDER_STATE_NOT_VALID","Not a valid purchase order state"),
    INVOICE_NOT_VALID("INVOICE_NOT_VALID", "Not a valid invoice"),
    INVOICE_VALIDATED_NOT_DELETABLE("INVOICE_VALIDATED_NOT_DELETABLE", "A validated invoice cannot be deleted"),
    BILLING_PERIOD_NOT_DELETABLE_LINKED_TO_INVOICE("BILLING_PERIOD_NOT_DELETABLE_LINKED_TO_INVOICE", "The billing period cannot be deleted because it is linked to at least one invoice"),
    BILLING_PERIOD_NOT_UPDATABLE_LINKED_TO_INVOICE("BILLING_PERIOD_NOT_UPDATABLE_LINKED_TO_INVOICE", "The billing period cannot be updated because it is linked to at least one invoice"),
    BILLING_PROVIDER_NOT_DELETABLE_LINKED_TO_INVOICE("BILLING_PROVIDER_NOT_DELETABLE_LINKED_TO_INVOICE", "The billing provider cannot be deleted because it is linked to at least one invoice"),
    BILLING_PROVIDER_NOT_UPDATABLE_LINKED_TO_INVOICE("BILLING_PROVIDER_NOT_UPDATABLE_LINKED_TO_INVOICE", "The billing provider cannot be updated because it is linked to at least one invoice"),
    INVOICE_STATE_NOT_DELETABLE_LINKED_TO_INVOICE("BILLING_PROVIDER_NOT_DELETABLE_LINKED_TO_INVOICE", "The invoice state cannot be deleted because it is linked to at least one invoice"),
    BILLING_REPARTITION_ENTITY_NOT_VALID("BILLING_REPARTITION_ENTITY_NOT_VALID","Not a valid billing repartition entity"),
    BILLING_REPARTITION_ENTITY_NOT_DELETABLE_LINKED_TO_RATE("BILLING_REPARTITION_ENTITY_NOT_DELETABLE_LINKED_TO_RATE","The billing repartition entity cannot be deleted because it is linked to at least one billing repartition entity rate"),
    BILLING_REPARTITION_ENTITY_RATE_NOT_VALID("BILLING_REPARTITION_ENTITY_RATE_NOT_VALID", "Not a valid billing repartition entity rate");

    String type;
    String title;

    ApiError(String type, String title) {
        this.type = type;
        this.title = title;
    }
}
