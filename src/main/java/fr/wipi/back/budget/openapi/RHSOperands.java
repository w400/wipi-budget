package fr.wipi.back.budget.openapi;

import org.apache.commons.lang3.StringUtils;

public enum RHSOperands {
    GREATER_THAN("gt"),
    GREATER_THAN_OR_EQUAL_TO("gte"),
    LESS_THAN("lt"),
    LESS_THAN_OR_EQUAL_TO("lte"),
    STRICTLY_EQUAL_TO("eq");

    private final String operator;

    RHSOperands(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return this.operator;
    }

    public static RHSOperands valueOfByOperatorIgnoreError(String operandStr) {
        for (RHSOperands enumValue : values()) {
            if (StringUtils.equals(enumValue.getOperator(), operandStr)) {
                return enumValue;
            }
        }

        return RHSOperands.STRICTLY_EQUAL_TO;
    }
}
