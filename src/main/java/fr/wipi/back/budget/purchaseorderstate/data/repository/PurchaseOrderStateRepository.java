package fr.wipi.back.budget.purchaseorderstate.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.purchaseorderstate.data.model.PurchaseOrderState;

public interface PurchaseOrderStateRepository extends PagingAndSortingRepository<PurchaseOrderState, Long>, CrudRepository<PurchaseOrderState, Long> {

}
