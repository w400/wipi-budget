package fr.wipi.back.budget.purchaseorderstate.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.openapi.api.PurchaseOrderStateApiDelegate;
import fr.wipi.back.budget.openapi.model.PurchaseOrderStateDTO;
import fr.wipi.back.budget.purchaseorderstate.data.model.PurchaseOrderState;
import fr.wipi.back.budget.purchaseorderstate.service.PurchaseOrderStateService;

@Component
public class PurchaseOrderStateApiDelegateImpl implements PurchaseOrderStateApiDelegate {
    @Autowired
    PurchaseOrderStateService service;

    @Override
    public ResponseEntity<PurchaseOrderStateDTO> addPurchaseOrderState(PurchaseOrderStateDTO purchaseOrderStateDTO) {
        purchaseOrderStateDTO.setId(service.addObject(purchaseOrderStateDTO));
        return new ResponseEntity<>(purchaseOrderStateDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<PurchaseOrderStateDTO>> getPurchaseOrderStates(Boolean active, Pageable pageable) {
        Iterable<PurchaseOrderState> purchaseOrderStates = service.findObjects(active, pageable);

        return new ResponseEntity<>(PurchaseOrderState.toDtos(purchaseOrderStates).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PurchaseOrderStateDTO> getPurchaseOrderStateById(Long id) {
        Optional<PurchaseOrderState> optPurchaseOrderState = service.findById(id);

        if (optPurchaseOrderState.isPresent()) {
            return new ResponseEntity<>(PurchaseOrderState.toDto(optPurchaseOrderState).orElse(null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deletePurchaseOrderStateById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<PurchaseOrderStateDTO> updatePurchaseOrderStateById(Long id,
            PurchaseOrderStateDTO purchaseOrderStateDTO) {
        if (service.updateObject(id, purchaseOrderStateDTO)) {
            return new ResponseEntity<>(purchaseOrderStateDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
