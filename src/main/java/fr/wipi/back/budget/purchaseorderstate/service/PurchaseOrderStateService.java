package fr.wipi.back.budget.purchaseorderstate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.PurchaseOrderStateDTO;
import fr.wipi.back.budget.purchaseorderstate.data.model.PurchaseOrderState;
import fr.wipi.back.budget.purchaseorderstate.data.repository.PurchaseOrderStateRepository;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class PurchaseOrderStateService implements WipiObjectService<PurchaseOrderStateDTO, PurchaseOrderState> {
    @Autowired
    PurchaseOrderStateRepository repository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(PurchaseOrderState object) {
        repository.save(object);
    }

    @Override
    public Iterable<PurchaseOrderState> findObjects(Boolean active, Pageable pageable) {
        Iterable<PurchaseOrderState> purchaseOrderStates;
        if (active != null) {
            throw buildError(ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING, List.of("active"), null,
                    "active parameter not managed", mapper);
        } else {
            purchaseOrderStates = repository.findAll(pageable);
        }

        return purchaseOrderStates;
    }

    @Override
    public Optional<PurchaseOrderState> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        if (repository.existsById(id)) {
            // TODO Assert that there is no purchase order linked to this purchase order
            // state

            repository.deleteById(id);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public Long addObject(PurchaseOrderStateDTO dto) {
        PurchaseOrderState purchaseOrderState = PurchaseOrderState.builder().name(dto.getName())
                .build();

        validateAndSave(purchaseOrderState, validator, "purchaseOrderState", ApiError.PURCHASE_ORDER_STATE_NOT_VALID,
                mapper);
        return purchaseOrderState.getId();
    }

    @Override
    public boolean updateObject(Long id, PurchaseOrderStateDTO dto) {
        Optional<PurchaseOrderState> optPurchaseOrderState = findById(id);
        if (optPurchaseOrderState.isPresent()) {
            PurchaseOrderState purchaseOrderState = optPurchaseOrderState.get();
            purchaseOrderState.setName(dto.getName());

            validateAndSave(purchaseOrderState, validator, "purchaseOrderState",
                    ApiError.PURCHASE_ORDER_STATE_NOT_VALID,
                    mapper);
            return true;
        } else {
            return false;
        }
    }

}
