package fr.wipi.back.budget.purchaseorderstate.data.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.openapi.model.PurchaseOrderStateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PurchaseOrderState implements EntityWithAuditFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    PurchaseOrderStateDTO toDto() {
        PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();
        dto.id(id);
        dto.name(name);

        return dto;
    }

    public static Optional<List<PurchaseOrderStateDTO>> toDtos(Iterable<PurchaseOrderState> invoiceStates) {
        if (invoiceStates != null) {
            List<PurchaseOrderStateDTO> dtos = new ArrayList<>();

            invoiceStates.forEach(budgetGroup -> dtos.add(budgetGroup.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<PurchaseOrderStateDTO> toDto(Optional<PurchaseOrderState> optInvoiceState) {
        if (optInvoiceState.isPresent() && optInvoiceState.get() != null) {
            return Optional.of(optInvoiceState.get().toDto());
        }

        return Optional.empty();
    }
}
