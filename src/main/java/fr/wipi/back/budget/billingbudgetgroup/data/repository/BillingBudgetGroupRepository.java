package fr.wipi.back.budget.billingbudgetgroup.data.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;

public interface BillingBudgetGroupRepository extends PagingAndSortingRepository<BillingBudgetGroup, Long>, CrudRepository<BillingBudgetGroup, Long> {

    @Query("select bp from BillingBudgetGroup bp where bp.validityEndDate >= ?1 and bp.validityStartDate <= ?1")
    Iterable<BillingBudgetGroup> findActivesBudgetGroups(LocalDate currentDate);

    @Query("select bp from BillingBudgetGroup bp where bp.validityEndDate < ?1 or bp.validityStartDate > ?1")
    Iterable<BillingBudgetGroup> findNotActivesBudgetGroups(LocalDate currentDate);
}
