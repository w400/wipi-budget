package fr.wipi.back.budget.billingbudgetgroup.data.model.validator;

import java.util.Optional;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;
import fr.wipi.back.budget.billingbudgettype.data.repository.BillingBudgetTypeRepository;
import fr.wipi.back.budget.data.model.validator.LinkedObjectsValidityDatesValidator;

@Service
public class BillingBudgetGroupValidBudgetTypeValidator
        implements LinkedObjectsValidityDatesValidator<BillingBudgetGroup, BillingBudgetType>, ConstraintValidator<BillingBudgetGroupValidBudgetTypeConstraint, BillingBudgetGroup> {

    @Autowired
    BillingBudgetTypeRepository repository;

    @Override
    public void initialize(BillingBudgetGroupValidBudgetTypeConstraint constraintAnnotation) {
        // Initialize mandatory for autowiring
    }

    @Override
    public boolean isValid(BillingBudgetGroup budgetGroup, ConstraintValidatorContext context) {
        return checkValidity(budgetGroup);
    }

    public Optional<BillingBudgetType> getLinkedObject(BillingBudgetGroup budgetGroup) {
        if (budgetGroup != null && budgetGroup.getType() != null && budgetGroup.getType().getId() != null) {
            return repository.findById(budgetGroup.getType().getId());
        }

        return Optional.empty();
    }

}
