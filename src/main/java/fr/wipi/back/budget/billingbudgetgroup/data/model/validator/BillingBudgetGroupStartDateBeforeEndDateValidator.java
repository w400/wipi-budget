package fr.wipi.back.budget.billingbudgetgroup.data.model.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.data.model.validator.ValidityDatesValidator;

public class BillingBudgetGroupStartDateBeforeEndDateValidator
        implements ValidityDatesValidator<BillingBudgetGroup>,
        ConstraintValidator<BillingBudgetGroupStartDateBeforeEndDateConstraint, BillingBudgetGroup> {

    @Override
    public boolean isValid(BillingBudgetGroup budgetGroup, ConstraintValidatorContext context) {
        return isValidWithEquality(budgetGroup);
    }

}
