package fr.wipi.back.budget.billingbudgetgroup.openapi.delegate;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.billingbudgetgroup.service.BillingBudgetGroupeService;
import fr.wipi.back.budget.openapi.api.BillingBudgetGroupApiDelegate;
import fr.wipi.back.budget.openapi.model.BillingBudgetGroupDTO;

@Component
public class BillingBudgetGroupApiDelegateImpl
        implements BillingBudgetGroupApiDelegate {
    @Autowired
    BillingBudgetGroupeService service;

    @Override
    public ResponseEntity<BillingBudgetGroupDTO> addBillingBudgetGroup(BillingBudgetGroupDTO billingBudgetGroupDTO) {
        billingBudgetGroupDTO.setId(service.addObject(billingBudgetGroupDTO));
        return new ResponseEntity<>(billingBudgetGroupDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<BillingBudgetGroupDTO>> getBillingBudgetGroups(Boolean active, Pageable pageable) {
        Iterable<BillingBudgetGroup> budgetGroups = service.findObjects(active, pageable);

        return new ResponseEntity<>(BillingBudgetGroup.toDtos(budgetGroups).orElse(null), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BillingBudgetGroupDTO> getBillingBudgetGroupById(Long id) {
        Optional<BillingBudgetGroup> optBudgetGroup = service.findById(id);

        if (optBudgetGroup.isPresent()) {
            return new ResponseEntity<>(BillingBudgetGroup.toDto(optBudgetGroup).orElse(null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> deleteBillingBudgetGroupById(Long id) {
        if (service.tryDelete(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<BillingBudgetGroupDTO> updateBillingBudgetGroupById(Long id,
            BillingBudgetGroupDTO billingBudgetGroupDTO) {
        if (service.updateObject(id, billingBudgetGroupDTO)) {
            return new ResponseEntity<>(billingBudgetGroupDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
