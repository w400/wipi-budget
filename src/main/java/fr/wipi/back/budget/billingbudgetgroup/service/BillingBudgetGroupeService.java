package fr.wipi.back.budget.billingbudgetgroup.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.billingbudgetgroup.data.repository.BillingBudgetGroupRepository;
import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingBudgetGroupDTO;
import fr.wipi.back.budget.service.WipiObjectService;

@Service
public class BillingBudgetGroupeService implements WipiObjectService<BillingBudgetGroupDTO, BillingBudgetGroup> {
    @Autowired
    BillingBudgetGroupRepository repository;

    @Autowired
    SmartValidator validator;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void saveObject(BillingBudgetGroup object) {
        repository.save(object);
    }

    @Override
    public Optional<BillingBudgetGroup> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean tryDelete(Long id) {
        if (repository.existsById(id)) {
            // TODO Assert that there is no budget linked to this budget group

            repository.deleteById(id);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public Iterable<BillingBudgetGroup> findObjects(Boolean active, Pageable pageable) {
        Iterable<BillingBudgetGroup> budgetGroups;
        if (active != null) {
            if (active.booleanValue()) {
                budgetGroups = repository.findActivesBudgetGroups(LocalDate.now());
            } else {
                budgetGroups = repository.findNotActivesBudgetGroups(LocalDate.now());
            }
        } else {
            budgetGroups = repository.findAll(pageable);
        }

        return budgetGroups;
    }

    @Override
    public Long addObject(BillingBudgetGroupDTO dto) {
        BillingBudgetGroup budgetGroup = BillingBudgetGroup.builder().name(dto.getName())
                .validityStartDate(dto.getValidityStartDate())
                .validityEndDate(dto.getValidityEndDate())
                .type(BillingBudgetType.builder()
                        .id(dto.getType() != null ? dto.getType().getId() : null)
                        .build())
                .build();

        validateAndSave(budgetGroup, validator, "budgetGroup", ApiError.BILLING_BUDGET_GROUP_NOT_VALID, mapper);
        return budgetGroup.getId();
    }

    @Override
    public boolean updateObject(Long id, BillingBudgetGroupDTO dto) {
        Optional<BillingBudgetGroup> optBudgetGroup = findById(id);
        if (optBudgetGroup.isPresent()) {
            // TODO Assert that there is no budget linked to this budget group for
            // which the period covered by the budget group validity dates includes the
            // period covered by the budget validity dates

            BillingBudgetGroup budgetGroup = optBudgetGroup.get();
            budgetGroup.setName(dto.getName());
            budgetGroup.setValidityStartDate(dto.getValidityStartDate());
            budgetGroup.setValidityEndDate(dto.getValidityEndDate());
            budgetGroup.setType(BillingBudgetType.builder()
                    .id(dto.getType() != null ? dto.getType().getId() : null)
                    .build());

            validateAndSave(budgetGroup, validator, "budgetGroup", ApiError.BILLING_BUDGET_GROUP_NOT_VALID,
                    mapper);
            return true;
        } else {
            return false;
        }

    }
}
