package fr.wipi.back.budget.billingbudgetgroup.data.model.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BillingBudgetGroupValidBudgetTypeValidator.class)
public @interface BillingBudgetGroupValidBudgetTypeConstraint {
    String message() default "{fr.wipi.back.budget.BillingBudgetGroupValidBudgetType.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
