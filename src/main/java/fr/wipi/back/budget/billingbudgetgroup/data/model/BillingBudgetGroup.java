package fr.wipi.back.budget.billingbudgetgroup.data.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.wipi.back.budget.billingbudgetgroup.data.model.validator.BillingBudgetGroupStartDateBeforeEndDateConstraint;
import fr.wipi.back.budget.billingbudgetgroup.data.model.validator.BillingBudgetGroupValidBudgetTypeConstraint;
import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;
import fr.wipi.back.budget.data.model.BaseEntityAudit;
import fr.wipi.back.budget.data.model.EntityWithAuditFields;
import fr.wipi.back.budget.data.model.EntityWithValidityDates;
import fr.wipi.back.budget.openapi.model.BillingBudgetGroupDTO;
import fr.wipi.back.budget.openapi.model.BillingBudgetTypeDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@BillingBudgetGroupStartDateBeforeEndDateConstraint
@BillingBudgetGroupValidBudgetTypeConstraint
public class BillingBudgetGroup implements EntityWithAuditFields, EntityWithValidityDates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    private BillingBudgetType type;

    @NotNull
    private LocalDate validityStartDate;

    private LocalDate validityEndDate;

    @Embedded
    @Builder.Default
    private BaseEntityAudit baseEntityAudit = new BaseEntityAudit();

    public BaseEntityAudit getBaseEntityAudit() {
        return baseEntityAudit;
    }

    BillingBudgetGroupDTO toDto() {
        BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
        dto.id(id);
        dto.name(name);
        dto.validityStartDate(validityStartDate);
        dto.validityEndDate(validityEndDate);
        if(type != null) {
            Optional<BillingBudgetTypeDTO> optTypeDto = BillingBudgetType.toDto(Optional.of(type));
            dto.setType(optTypeDto.isPresent() ? optTypeDto.get() : null);
        }

        return dto;
    }

    public static Optional<List<BillingBudgetGroupDTO>> toDtos(Iterable<BillingBudgetGroup> billingBudgetGroups) {
        if (billingBudgetGroups != null) {
            List<BillingBudgetGroupDTO> dtos = new ArrayList<>();

            billingBudgetGroups.forEach(budgetGroup -> dtos.add(budgetGroup.toDto()));

            return Optional.of(dtos);
        }

        return Optional.empty();
    }

    public static Optional<BillingBudgetGroupDTO> toDto(Optional<BillingBudgetGroup> optBillingBudgetGroup) {
        if (optBillingBudgetGroup.isPresent() && optBillingBudgetGroup.get() != null) {
            return Optional.of(optBillingBudgetGroup.get().toDto());
        }

        return Optional.empty();
    }
}
