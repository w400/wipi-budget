INSERT INTO invoice_state(id, name, created_date) values (1, 'Invoice state 1', '2023-02-09');
INSERT INTO invoice_state(id, name, created_date) values (2, 'Invoice state 2', '2023-02-09');
INSERT INTO invoice_state(id, name, created_date) values (3, 'Invoice state 3', '2023-02-09');

INSERT INTO billing_provider(id, name, contact_email, validity_start_date, validity_end_date, created_date) values (1, 'Provider 1', 'contact@provider1.com', '2021-01-01', '2023-12-31', '2023-02-09');
INSERT INTO billing_provider(id, name, contact_email, validity_start_date, validity_end_date, created_date) values (2, 'Provider 2', 'contact@provider1.com', '2021-01-01', '2023-12-31', '2023-02-09');
INSERT INTO billing_provider(id, name, contact_email, validity_start_date, validity_end_date, created_date) values (3, 'Provider 3', 'contact@provider1.com', '2021-01-01', '2023-12-31', '2023-02-09');

INSERT INTO billing_period(id, label, start_date, end_date, created_date) values (1, 'Year 2021', '2021-01-01', '2021-12-31', '2023-02-09');
INSERT INTO billing_period(id, label, start_date, end_date, created_date) values (2, 'Year 2022', '2022-01-01', '2022-12-31', '2023-02-09');
INSERT INTO billing_period(id, label, start_date, end_date, created_date) values (3, 'Year 2023', '2023-01-01', '2023-12-31', '2023-02-09');

INSERT INTO billing_budget_type(name, validity_start_date, validity_end_date, created_date) values ('BudgetType 1', '2021-01-01', '2023-12-31', '2023-02-09');
INSERT INTO billing_budget_type(name, validity_start_date, validity_end_date, created_date) values ('BudgetType 2', '2021-01-01', '2023-12-31', '2023-02-09');

INSERT INTO billing_budget_group(name, validity_start_date, validity_end_date, type_id, created_date) values ('BudgetGroup 1', '2021-01-01', '2023-12-31', 1, '2023-02-09');
INSERT INTO billing_budget_group(name, validity_start_date, validity_end_date, type_id, created_date) values ('BudgetGroup 2', '2021-01-01', '2023-12-31', 1, '2023-02-09');
INSERT INTO billing_budget_group(name, validity_start_date, validity_end_date, type_id, created_date) values ('BudgetGroup 3', '2021-01-01', '2023-12-31', 1, '2023-02-09');
INSERT INTO billing_budget_group(name, validity_start_date, validity_end_date, type_id, created_date) values ('BudgetGroup 4', '2021-01-01', '2023-12-31', 2, '2023-02-09');
INSERT INTO billing_budget_group(name, validity_start_date, validity_end_date, type_id, created_date) values ('BudgetGroup 5', '2021-01-01', '2023-12-31', 2, '2023-02-09');
INSERT INTO billing_budget_group(name, validity_start_date, validity_end_date, type_id, created_date) values ('BudgetGroup 6', '2021-01-01', '2023-12-31', 2, '2023-02-09');

INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 1', '2021-01-01', '2023-12-31', 1, 1, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 2', '2021-01-01', '2023-12-31', 1, 2, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 3', '2021-01-01', '2023-12-31', 2, 3, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 4', '2021-01-01', '2023-12-31', 2, 1, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 5', '2021-01-01', '2023-12-31', 3, 2, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 6', '2021-01-01', '2023-12-31', 3, 3, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 7', '2021-01-01', '2023-12-31', 4, 1, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 8', '2021-01-01', '2023-12-31', 4, 2, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 9', '2021-01-01', '2023-12-31', 5, 3, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 10', '2021-01-01', '2023-12-31', 5, 1, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 11', '2021-01-01', '2023-12-31', 6, 2, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 12', '2021-01-01', '2023-12-31', 6, 3, '2023-02-09');

INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (1, 1, 'Invoice 1', 'Invoice number 1', 'Provider invoice number 1', 1, '2023-01-01', '2022-12-31', '2023-01-01');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 2, 'Invoice 2', 'Invoice number 2', 'Provider invoice number 2', 2, '2023-01-02', '2022-12-30', '2023-01-02');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (3, 3, 'Invoice 3', 'Invoice number 3', 'Provider invoice number 3', 3, '2023-01-03', '2022-12-29', '2023-01-03');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (1, 1, 'Invoice 4', 'Invoice number 4', 'Provider invoice number 4', 1, '2023-01-04', '2022-12-28', '2023-01-04');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 2, 'Invoice 5', 'Invoice number 5', 'Provider invoice number 5', 2, '2023-01-05', '2022-12-27', '2023-01-05');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (3, 3, 'Invoice 6', 'Invoice number 6', 'Provider invoice number 6', 3, '2023-01-06', '2022-12-26', '2023-01-06');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (1, 1, 'Invoice 7', 'Invoice number 7', 'Provider invoice number 7', 1, '2023-01-07', '2022-12-25', '2023-01-07');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 2, 'Invoice 8', 'Invoice number 8', 'Provider invoice number 8', 2, '2023-01-08', '2022-12-24', '2023-01-08');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (3, 3, 'Invoice 9', 'Invoice number 9', 'Provider invoice number 9', 3, '2023-01-09', '2022-12-23', '2023-01-09');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (1, 1, 'Invoice 10', 'Invoice number 10', 'Provider invoice number 10', 1, '2023-01-10', '2022-12-22', '2023-01-10');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 2, 'Invoice 11', 'Invoice number 11', 'Provider invoice number 11', 2, '2023-01-11', '2022-12-21', '2023-01-11');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (3, 3, 'Invoice 12', 'Invoice number 12', 'Provider invoice number 12', 3, '2023-01-12', '2022-12-20', '2023-01-12');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (1, 1, 'Invoice 13', 'Invoice number 13', 'Provider invoice number 13', 1, '2023-01-13', '2022-12-19', '2023-01-13');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 2, 'Invoice 14', 'Invoice number 14', 'Provider invoice number 14', 2, '2023-01-14', '2022-12-18', '2023-01-14');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (3, 3, 'Invoice 15', 'Invoice number 15', 'Provider invoice number 15', 3, '2023-01-15', '2022-12-17', '2023-01-15');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (1, 1, 'Invoice 16', 'Invoice number 16', 'Provider invoice number 16', 1, '2023-01-16', '2022-12-16', '2023-01-16');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 2, 'Invoice 17', 'Invoice number 17', 'Provider invoice number 17', 2, '2023-01-17', '2022-12-15', '2023-01-17');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (3, 3, 'Invoice 18', 'Invoice number 18', 'Provider invoice number 18', 3, '2023-01-18', '2022-12-14', '2023-01-18');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (1, 1, 'Invoice 19', 'Invoice number 19', 'Provider invoice number 19', 1, '2023-01-19', '2022-12-13', '2023-01-19');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 2, 'Invoice 20', 'Invoice number 20', 'Provider invoice number 20', 2, '2023-01-20', '2022-12-12', '2023-01-20');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (3, 3, 'Invoice 21', 'Invoice number 21', 'Provider invoice number 21', 3, '2023-01-21', '2022-12-11', '2023-01-21');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (1, 1, 'Invoice 22', 'Invoice number 22', 'Provider invoice number 22', 1, '2023-01-22', '2022-12-10', '2023-01-22');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 2, 'Invoice 23', 'Invoice number 23', 'Provider invoice number 23', 2, '2023-01-23', '2022-12-09', '2023-01-23');
INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (3, 3, 'Invoice 24', 'Invoice number 24', 'Provider invoice number 24', 3, '2023-01-24', '2022-12-08', '2023-01-24');