INSERT INTO invoice_state(id, name, created_date) values (1, 'Invoice state 1', '2023-02-08');

INSERT INTO billing_provider(id, name, contact_email, validity_start_date, validity_end_date, created_date) values (1, 'Provider 1', 'contact@provider1.com', '1921-01-01', '1921-12-31', '2023-02-08');

INSERT INTO billing_period(id, label, start_date, end_date, created_date) values (1, 'Year 1921', '1921-01-01', '1921-12-31', '2023-02-08');