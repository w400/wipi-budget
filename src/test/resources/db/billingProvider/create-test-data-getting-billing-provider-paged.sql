INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 1', 'contact@provider1.com', '1921-01-01', '1921-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 2', 'contact@provider2.com', '1922-01-01', '1922-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 3', 'contact@provider3.com', '1923-01-01', '1923-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 4', 'contact@provider4.com', '1924-01-01', '1924-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 5', 'contact@provider5.com', '1925-01-01', '1925-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 6', 'contact@provider6.com', '1926-01-01', '1926-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 7', 'contact@provider7.com', '1927-01-01', '1927-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 8', 'contact@provider8.com', '1928-01-01', '1928-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 9', 'contact@provider9.com', '1929-01-01', '1929-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 10', 'contact@provider10.com', '1930-01-01', '1930-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 11', 'contact@provider11.com', '1931-01-01', '1931-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 12', 'contact@provider12.com', '1932-01-01', '1932-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 13', 'contact@provider13.com', '1933-01-01', '1933-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 14', 'contact@provider14.com', '1934-01-01', '1934-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 15', 'contact@provider15.com', '1935-01-01', '1935-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 16', 'contact@provider16.com', '1936-01-01', '1936-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 17', 'contact@provider17.com', '1937-01-01', '1937-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 18', 'contact@provider18.com', '1938-01-01', '1938-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 19', 'contact@provider19.com', '1939-01-01', '1939-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 20', 'contact@provider20.com', '1940-01-01', '1940-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 21', 'contact@provider21.com', '1941-01-01', '1941-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 22', 'contact@provider22.com', '1942-01-01', '1942-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 23', 'contact@provider23.com', '1943-01-01', '1943-12-31', '2022-05-31');
INSERT INTO billing_provider(name, contact_email, validity_start_date, validity_end_date, created_date) values ('Provider 24', 'contact@provider24.com', '1944-01-01', '1944-12-31', '2022-05-31');


INSERT INTO invoice_state(id, name, created_date) values (1, 'Invoice state 1', '2023-02-09');
INSERT INTO billing_budget_type(name, validity_start_date, validity_end_date, created_date) values ('BudgetType 1', '2021-01-01', '2023-12-31', '2023-02-09');
INSERT INTO billing_budget_group(name, validity_start_date, validity_end_date, type_id, created_date) values ('BudgetGroup 1', '2021-01-01', '2023-12-31', 1, '2023-02-09');
INSERT INTO billing_budget(name, validity_start_date, validity_end_date, group_id, manager_id, created_date) values ('Budget 1', '2021-01-01', '2023-12-31', 1, 1, '2023-02-09');
INSERT INTO billing_period(label, start_date, end_date, created_date) values ('Year 1944', '1944-01-01', '1944-12-31', '2022-05-31');

INSERT INTO invoice(provider_id, period_id, description, invoice_number, provider_invoice_number, state_id, reception_date, provider_sent_date, created_date) values (2, 1, 'Invoice 1', 'Invoice number 1', 'Provider invoice number 1', 1, '2023-01-01', '2022-12-31', '2023-01-01');