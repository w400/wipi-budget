DELETE FROM billing_budget;
ALTER TABLE billing_budget ALTER COLUMN ID RESTART WITH 1;

DELETE FROM billing_budget_group;
ALTER TABLE billing_budget_group ALTER COLUMN ID RESTART WITH 1;
