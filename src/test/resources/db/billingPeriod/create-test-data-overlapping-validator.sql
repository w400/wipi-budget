INSERT INTO billing_period(label, start_date, end_date, created_date) values ('Year 2021', '2021-01-01', '2021-12-31', '2022-05-31');
INSERT INTO billing_period(label, start_date, end_date, created_date) values ('Year 2022', '2022-01-01', '2022-12-31', '2022-05-31');
INSERT INTO billing_period(label, start_date, end_date, created_date) values ('Q1 2020', '2020-01-01', '2020-03-31', '2022-05-31');