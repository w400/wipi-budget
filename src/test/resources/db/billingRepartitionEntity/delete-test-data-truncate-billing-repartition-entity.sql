DELETE FROM billing_repartition_entity_rate;
ALTER TABLE billing_repartition_entity_rate ALTER COLUMN ID RESTART WITH 1;

DELETE FROM billing_repartition_entity;
ALTER TABLE billing_repartition_entity ALTER COLUMN ID RESTART WITH 1;