INSERT INTO billing_repartition_entity(id, name, validator_id, validity_start_date, validity_end_date, created_date) values (1, 'Repartition entity 1', 1, '1921-01-01', '1921-12-31', '2023-02-26');
INSERT INTO billing_repartition_entity(id, name, validator_id, validity_start_date, validity_end_date, created_date) values (2, 'Repartition entity 2', 2, '1922-01-01', '1922-12-31', '2023-02-26');
INSERT INTO billing_repartition_entity(id, name, validator_id, validity_start_date, validity_end_date, created_date) values (3, 'Active repartition entity', 3, CURRENT_DATE()-1, CURRENT_DATE()+1, '2023-02-26');

INSERT INTO billing_repartition_entity_rate(id, entity_id, rate, created_date) values (4, 1, 1, '2023-02-26');
INSERT INTO billing_repartition_entity_rate(id, entity_id, rate, created_date) values (5, 2, 2, '2023-02-26');
INSERT INTO billing_repartition_entity_rate(id, entity_id, rate, created_date) values (6, 3, 3, '2023-02-26');