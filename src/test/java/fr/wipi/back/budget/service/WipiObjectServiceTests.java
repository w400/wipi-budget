package fr.wipi.back.budget.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.error.ApiError;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SpringBootTest
class WipiObjectServiceTests {
    DefaultWipiObjectService service = new DefaultWipiObjectService();

    @Autowired
    SmartValidator validator;

    @Service
    public class DefaultWipiObjectService implements WipiObjectService<DefaultObjectDTO, DefaultObject> {

        @Override
        public void saveObject(DefaultObject object) {
        }

        @Override
        public Optional<DefaultObject> findById(Long id) {
            return Optional.empty();
        }

        @Override
        public boolean tryDelete(Long id) {
            return false;
        }

        @Override
        public Long addObject(DefaultObjectDTO dto) {
            return null;
        }

        @Override
        public boolean updateObject(Long id, DefaultObjectDTO dto) {
            return false;
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class DefaultObject {
        @NotNull
        String name;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class DefaultObjectDTO {
        String name;
    }

    @Test
    void givenIncorrectObject_whenJsonExceptionOnValidateAndSave_thenInternalServerError()
            throws JsonProcessingException {
        // Given
        DefaultObject defObj = new DefaultObject();
        ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
        doThrow(JsonProcessingException.class).when(mapper).writeValueAsString(any());

        // When
        Exception e = null;
        try {
            service.validateAndSave(defObj, validator, "defaultObject", ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING,
                    mapper);
        } catch (ResponseStatusException rse) {
            e = rse;
        }

        // Then
        assertNotNull(e);
        assertEquals(ResponseStatusException.class, e.getClass());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, ((ResponseStatusException) e).getStatusCode());
    }

    @Test
    void givenIncorrectObject_whenJsonExceptionOnThrowError_thenInternalServerError() throws JsonProcessingException {
        // Given
        ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
        doThrow(JsonProcessingException.class).when(mapper).writeValueAsString(any());

        // When
        Exception e = null;
        try {
            throw service.buildError(ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING, List.of("active"), null,
                    "active parameter not managed", mapper);
        } catch (ResponseStatusException rse) {
            e = rse;
        }

        // Then
        assertNotNull(e);
        assertEquals(ResponseStatusException.class, e.getClass());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, ((ResponseStatusException) e).getStatusCode());
    }

    @Test
    void givenNonOverridedMethd_whenCallingFindObjects_thenReturnNull() {
        assertNull(service.findObjects(false, null));
    }
}
