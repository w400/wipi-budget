package fr.wipi.back.budget.openapi.error;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import fr.wipi.back.budget.openapi.model.ApiErrorResponseDTO;

@SpringBootTest
class ApiExceptionHandlerTests {
    @Autowired
    ApiExceptionHandler handler;

    @Test
    void givenIncorrectException_whenHandlingJsonException_thenInternalServerErrorThrown() {
        ResponseEntity<ApiErrorResponseDTO> response = handler
                .handleResponseStatusException(new ResponseStatusException(HttpStatus.BAD_REQUEST, ""));

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }
}