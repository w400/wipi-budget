package fr.wipi.back.budget.billingbudgettype.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.BillingBudgetTypeDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class BillingBudgetTypeApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetTypeDeleteExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudgetType/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("billing-budget-type-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the billing budget type to delete"))));
        }

        @Test
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetTypeAddExample() throws Exception {
                // Given
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2024, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2024, 12, 31));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetType").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("billing-budget-type-add-example",
                                requestFields(billingBudgetTypeFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetTypeGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-type-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the billing budget type to retrieve")),
                                responseFields(billingBudgetTypeFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetTypeGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-type-get-example", queryParameters(parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]").description("An array of billing budget types"))
                                                .andWithPrefix("[].", billingBudgetTypeFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetTypeGetActiveExample() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType?active={active}", active));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-type-get-active-example", queryParameters(parameterWithName(
                                "active")
                                .description("A boolean flag to control if only active budget types should be returned")),
                                responseFields(fieldWithPath("[]").description("An array of billing budget types"))
                                                .andWithPrefix("[].", billingBudgetTypeFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetTypeUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                String newBudgetTypeName = "Test name";
                LocalDate newStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newEndDate = LocalDate.of(2024, 12, 31);
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName(newBudgetTypeName);
                dto.setValidityStartDate(newStartDate);
                dto.setValidityEndDate(newEndDate);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetType/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-type-update-example", pathParameters(
                                parameterWithName("id").description("The id of the billing budget type to update")),
                                requestFields(billingBudgetTypeFieldDescriptors(true)),
                                responseFields(billingBudgetTypeFieldDescriptors(false))));
        }

        private FieldDescriptor[] billingBudgetTypeFieldDescriptors(boolean creation) {
                return new FieldDescriptor[] { fieldDescriptorForId(creation), fieldDescriptorForName(),
                                fieldDescriptorForValidityStartDate(), fieldDescriptorForEndingDate() };
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the billing budget type." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("name").description(
                                "Name of the billing budget type");
        }

        private FieldDescriptor fieldDescriptorForValidityStartDate() {
                return fieldWithPath("validityStartDate").description(
                                "Validity start date of the budget type. Before this date the budget type could not be used");
        }

        private FieldDescriptor fieldDescriptorForEndingDate() {
                return fieldWithPath("validityEndDate").description(
                                "Validity end date of the budget type. After this date the budget type could not be used. If null the budget type is always valid starting after the validity start date");
        }

}
