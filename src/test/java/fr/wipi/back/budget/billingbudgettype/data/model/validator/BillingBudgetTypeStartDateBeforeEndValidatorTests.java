package fr.wipi.back.budget.billingbudgettype.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;

@SpringBootTest
class BillingBudgetTypeStartDateBeforeEndValidatorTests {

    private BillingBudgetTypeStartDateBeforeEndDateValidator validator = new BillingBudgetTypeStartDateBeforeEndDateValidator();
    
    @Test
    void whenBudgetTypeIsNull_thenTestFailed() {
        // Given
        BillingBudgetType budgetType = null;

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertFalse(isValid, "When the BillingBudgetType object is null, the validator should failed");
    }

    @Test
    void whenEndDateIsNull_thenTestSucceed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setValidityStartDate(LocalDate.now());
        budgetType.setValidityEndDate(null);

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertTrue(isValid, "When the end date is null, the validator should succeed");
    }

    @Test
    void whenStartDateIsNull_thenTestFailed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setValidityStartDate(null);
        budgetType.setValidityEndDate(LocalDate.now());

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertFalse(isValid, "When the start date is null, the validator should failed");
    }

    @Test
    void whenStartDateBeforeEndDate_thenTestSucceeded() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setValidityStartDate(LocalDate.of(2022, 01, 01));
        budgetType.setValidityEndDate(LocalDate.of(2022, 12, 31));

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertTrue(isValid, "When the start date is before the end date, the validator should succeed");
    }

    @Test
    void whenStartDateEqualEndDate_thenTestSucceed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setValidityStartDate(LocalDate.of(2022, 01, 01));
        budgetType.setValidityEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertTrue(isValid, "When the start date is strictly equal to the end date, the validator should succeed");
    }

    @Test
    void whenStartDateAfterEndDate_thenTestFailed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setValidityStartDate(LocalDate.of(2023, 01, 01));
        budgetType.setValidityEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertFalse(isValid, "When the start date is after the end date, the validator should failed");
    }
}
