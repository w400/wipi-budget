package fr.wipi.back.budget.billingbudgettype.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import jakarta.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;
import fr.wipi.back.budget.billingbudgettype.data.repository.BillingBudgetTypeRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingBudgetTypeDTO;

@SpringBootTest
class BillingBudgetTypeApiDelegateImplTests implements GenericApiDelegateImplTests {
        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        BillingBudgetTypeRepository repository;

        private MockMvc mockMvc;

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesBillingBudgetTypeApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("billingBudgetTypeApiController"));
        }

        @Test
        void givenEmptyBudgetType_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetType").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_BUDGET_TYPE_NOT_VALID,
                                List.of("name", "validityStartDate"),
                                List.of("BillingBudgetTypeStartDateBeforeEndDateConstraint"));
        }

        @Test
        void givenBudgetTypeWithoutName_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetType").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_TYPE_NOT_VALID, "name");
        }

        @Test
        void givenBudgetTypeWithoutStartDate_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName("Test name");
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetType").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_TYPE_NOT_VALID, "validityStartDate");
        }

        @Test
        void givenBudgetTypeWithStartDateAfterEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetType").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_TYPE_NOT_VALID,
                                "BillingBudgetTypeStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeCorrect_whenCreating_thenCreated() throws Exception {
                // Given
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2024, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2024, 12, 31));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetType").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budgetType should have been created but there is no budgetType in the database");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeWithoutEndDate_whenCreating_thenCreated() throws Exception {
                // Given
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetType").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budgetType should have been created but there is no budgetType in the database");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeWithStartDateEqualEndDate_whenCreating_thenCreated() throws Exception {
                // Given
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2018, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2018, 1, 1));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetType").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budgetType should have been created but there is no budgetType in the database");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"BudgetType 1\"},
                                                    {\"id\": 2, \"name\": \"BudgetType 2\"},
                                                    {\"id\": 3, \"name\": \"Active budgetType\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenActiveParams_whenListing_thenReturnActiveBudgetTypesOnly() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                "[{\"id\": 3, \"name\": \"Active budgetType\"}]"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNotActiveParams_whenListing_thenReturnNotActiveBudgetTypesOnly() throws Exception {
                // Given
                String active = "false";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"BudgetType 1\"},
                                                    {\"id\": 2, \"name\": \"BudgetType 2\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"BudgetType 1\"},
                                                    {\"name\": \"BudgetType 2\"},
                                                    {\"name\": \"BudgetType 3\"},
                                                    {\"name\": \"BudgetType 4\"},
                                                    {\"name\": \"BudgetType 5\"},
                                                    {\"name\": \"BudgetType 6\"},
                                                    {\"name\": \"BudgetType 7\"},
                                                    {\"name\": \"BudgetType 8\"},
                                                    {\"name\": \"BudgetType 9\"},
                                                    {\"name\": \"BudgetType 10\"},
                                                    {\"name\": \"BudgetType 11\"},
                                                    {\"name\": \"BudgetType 12\"},
                                                    {\"name\": \"BudgetType 13\"},
                                                    {\"name\": \"BudgetType 14\"},
                                                    {\"name\": \"BudgetType 15\"},
                                                    {\"name\": \"BudgetType 16\"},
                                                    {\"name\": \"BudgetType 17\"},
                                                    {\"name\": \"BudgetType 18\"},
                                                    {\"name\": \"BudgetType 19\"},
                                                    {\"name\": \"BudgetType 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"BudgetType 1\"},
                                                    {\"name\": \"BudgetType 2\"},
                                                    {\"name\": \"BudgetType 3\"},
                                                    {\"name\": \"BudgetType 4\"},
                                                    {\"name\": \"BudgetType 5\"},
                                                    {\"name\": \"BudgetType 6\"},
                                                    {\"name\": \"BudgetType 7\"},
                                                    {\"name\": \"BudgetType 8\"},
                                                    {\"name\": \"BudgetType 9\"},
                                                    {\"name\": \"BudgetType 10\"},
                                                    {\"name\": \"BudgetType 11\"},
                                                    {\"name\": \"BudgetType 12\"},
                                                    {\"name\": \"BudgetType 13\"},
                                                    {\"name\": \"BudgetType 14\"},
                                                    {\"name\": \"BudgetType 15\"},
                                                    {\"name\": \"BudgetType 16\"},
                                                    {\"name\": \"BudgetType 17\"},
                                                    {\"name\": \"BudgetType 18\"},
                                                    {\"name\": \"BudgetType 19\"},
                                                    {\"name\": \"BudgetType 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"BudgetType 21\"},
                                                    {\"name\": \"BudgetType 22\"},
                                                    {\"name\": \"BudgetType 23\"},
                                                    {\"name\": \"BudgetType 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/billingBudgetType?page={page}&size={size}", page, nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"BudgetType 11\"},
                                                    {\"name\": \"BudgetType 12\"},
                                                    {\"name\": \"BudgetType 13\"},
                                                    {\"name\": \"BudgetType 14\"},
                                                    {\"name\": \"BudgetType 15\"},
                                                    {\"name\": \"BudgetType 16\"},
                                                    {\"name\": \"BudgetType 17\"},
                                                    {\"name\": \"BudgetType 18\"},
                                                    {\"name\": \"BudgetType 19\"},
                                                    {\"name\": \"BudgetType 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"name\": \"BudgetType 1\"}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetType/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudgetType/{id}", id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)),
                                "The billing budget type still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudgetType/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudgetType/{id}", id)
                                .content(asJsonString(dto)).contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyBudgetType_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetType/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_BUDGET_TYPE_NOT_VALID,
                                List.of("name", "validityStartDate"),
                                List.of("BillingBudgetTypeStartDateBeforeEndDateConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeWithoutName_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetType/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_TYPE_NOT_VALID, "name");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeWithoutStartDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName("Test name");
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetType/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_TYPE_NOT_VALID, "validityStartDate");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeWithStartDateAfterEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetType/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_TYPE_NOT_VALID,
                                "BillingBudgetTypeStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeCorrect_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = LocalDate.of(2024, 12, 31);
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName(newName);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetType/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingBudgetType> optNewBudgetType = repository.findById(id);
                if (optNewBudgetType.isPresent()) {
                        BillingBudgetType budgetType = optNewBudgetType.get();
                        assertEquals(newName, budgetType.getName());
                        assertEquals(newValidityStartDate, budgetType.getValidityStartDate());
                        assertEquals(newValidityEndDate, budgetType.getValidityEndDate());
                } else {
                        assertFalse(true, "The budget type with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeWithoutEndDate_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = null;
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName(newName);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetType/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingBudgetType> optNewBudgetType = repository.findById(id);
                if (optNewBudgetType.isPresent()) {
                        BillingBudgetType budgetType = optNewBudgetType.get();
                        assertEquals(newName, budgetType.getName());
                        assertEquals(newValidityStartDate, budgetType.getValidityStartDate());
                        assertEquals(newValidityEndDate, budgetType.getValidityEndDate());
                } else {
                        assertFalse(true, "The budget type with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql",
                        "/db/billingBudgetType/create-test-data-getting-billing-budget-type.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetType/delete-test-data-truncate-billing-budget-type.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetTypeWithStartDateEqualEndDate_whenUpdating_thenCreated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = newValidityStartDate;
                BillingBudgetTypeDTO dto = new BillingBudgetTypeDTO();
                dto.setName(newName);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetType/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingBudgetType> optNewBudgetType = repository.findById(id);
                if (optNewBudgetType.isPresent()) {
                        BillingBudgetType budgetType = optNewBudgetType.get();
                        assertEquals(newName, budgetType.getName());
                        assertEquals(newValidityStartDate, budgetType.getValidityStartDate());
                        assertEquals(newValidityEndDate, budgetType.getValidityEndDate());
                } else {
                        assertFalse(true, "The budget type with the given id does not exists anymore in the database");
                }
        }
}
