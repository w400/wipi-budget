package fr.wipi.back.budget.purchaseorderstate.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import jakarta.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.PurchaseOrderStateDTO;
import fr.wipi.back.budget.purchaseorderstate.data.model.PurchaseOrderState;
import fr.wipi.back.budget.purchaseorderstate.data.repository.PurchaseOrderStateRepository;

@SpringBootTest
class PurchaseOrderStateApiDelegateImplTests implements GenericApiDelegateImplTests {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    PurchaseOrderStateRepository repository;

    private MockMvc mockMvc;

    private String asJsonString(final Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    void givenWac_whenServletContext_thenItProvidesPurchaseOrderStateApiDelegateImpl() {
        ServletContext servletContext = webApplicationContext.getServletContext();

        assertNotNull(servletContext);
        assertTrue(servletContext instanceof MockServletContext);
        assertNotNull(webApplicationContext.getBean("purchaseOrderStateApiController"));
    }

    @Test
    void givenEmptyPurchaseOrderState_whenCreating_thenError() throws Exception {
        // Given
        PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();

        // When
        ResultActions result = this.mockMvc.perform(post("/purchaseOrderState").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(status().isBadRequest());
        verifyOnlyOneFieldError(mapper, result, ApiError.PURCHASE_ORDER_STATE_NOT_VALID, "name");
    }

    @Test
    void givenPurchaseOrderStateWithoutName_whenCreating_thenError() throws Exception {
        // Given
        PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();

        // When
        ResultActions result = this.mockMvc.perform(post("/purchaseOrderState").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(status().isBadRequest());
        verifyOnlyOneFieldError(mapper, result, ApiError.PURCHASE_ORDER_STATE_NOT_VALID, "name");
    }

    @Test
    void givenCorrectPurchaseOrderState_whenCreating_thenCreated()
            throws Exception {
        // Given
        PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();
        dto.setName("Test name");

        // When
        ResultActions result = this.mockMvc.perform(post("/purchaseOrderState").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(status().isCreated());
        assertEquals(1, repository.count(),
                "The purchase order state should have been created but there is no purchase order state in the database");
    }

    @Test
    @Sql(scripts = {
            "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
        // Given

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState"));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(content().json("[]"));
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
        // Given

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState"));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(content().json(
                """
                        [
                            {\"id\": 1, \"name\": \"Purchase order state 1\"},
                            {\"id\": 2, \"name\": \"Purchase order state 2\"},
                            {\"id\": 3, \"name\": \"Active purchase order state\"}
                        ]"""));
    }

    @Test
    void givenActiveParams_whenListing_thenNotManagedParamExceptionThrown() throws Exception {
        // Given
        String active = "true";

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState").param("active", active));

        // Then
        result.andExpect(status().isBadRequest());
        verifyOnlyOneFieldError(mapper, result, ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING, "active");
    }

    @Test
    void givenNotActiveParams_whenListing_thenNotManagedParamExceptionThrown() throws Exception {
        // Given
        String active = "false";

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState").param("active", active));

        // Then
        result.andExpect(status().isBadRequest());
        verifyOnlyOneFieldError(mapper, result, ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING, "active");
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
        // Given

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState"));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(content().json(
                """
                        [
                            {\"name\": \"Purchase order state 1\"},
                            {\"name\": \"Purchase order state 2\"},
                            {\"name\": \"Purchase order state 3\"},
                            {\"name\": \"Purchase order state 4\"},
                            {\"name\": \"Purchase order state 5\"},
                            {\"name\": \"Purchase order state 6\"},
                            {\"name\": \"Purchase order state 7\"},
                            {\"name\": \"Purchase order state 8\"},
                            {\"name\": \"Purchase order state 9\"},
                            {\"name\": \"Purchase order state 10\"},
                            {\"name\": \"Purchase order state 11\"},
                            {\"name\": \"Purchase order state 12\"},
                            {\"name\": \"Purchase order state 13\"},
                            {\"name\": \"Purchase order state 14\"},
                            {\"name\": \"Purchase order state 15\"},
                            {\"name\": \"Purchase order state 16\"},
                            {\"name\": \"Purchase order state 17\"},
                            {\"name\": \"Purchase order state 18\"},
                            {\"name\": \"Purchase order state 19\"},
                            {\"name\": \"Purchase order state 20\"}
                        ]
                        """));
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
        // Given
        String page = "0";

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState?page={page}", page));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(content().json(
                """
                        [
                            {\"name\": \"Purchase order state 1\"},
                            {\"name\": \"Purchase order state 2\"},
                            {\"name\": \"Purchase order state 3\"},
                            {\"name\": \"Purchase order state 4\"},
                            {\"name\": \"Purchase order state 5\"},
                            {\"name\": \"Purchase order state 6\"},
                            {\"name\": \"Purchase order state 7\"},
                            {\"name\": \"Purchase order state 8\"},
                            {\"name\": \"Purchase order state 9\"},
                            {\"name\": \"Purchase order state 10\"},
                            {\"name\": \"Purchase order state 11\"},
                            {\"name\": \"Purchase order state 12\"},
                            {\"name\": \"Purchase order state 13\"},
                            {\"name\": \"Purchase order state 14\"},
                            {\"name\": \"Purchase order state 15\"},
                            {\"name\": \"Purchase order state 16\"},
                            {\"name\": \"Purchase order state 17\"},
                            {\"name\": \"Purchase order state 18\"},
                            {\"name\": \"Purchase order state 19\"},
                            {\"name\": \"Purchase order state 20\"}
                        ]
                        """));
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
        // Given
        String page = "1";

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState?page={page}", page));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(content().json(
                """
                        [
                            {\"name\": \"Purchase order state 21\"},
                            {\"name\": \"Purchase order state 22\"},
                            {\"name\": \"Purchase order state 23\"},
                            {\"name\": \"Purchase order state 24\"}
                        ]
                        """));
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
            throws Exception {
        // Given
        String page = "1";
        String nbElements = "10";

        // When
        ResultActions result = this.mockMvc
                .perform(get("/purchaseOrderState?page={page}&size={size}", page, nbElements));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(content().json(
                """
                        [
                            {\"name\": \"Purchase order state 11\"},
                            {\"name\": \"Purchase order state 12\"},
                            {\"name\": \"Purchase order state 13\"},
                            {\"name\": \"Purchase order state 14\"},
                            {\"name\": \"Purchase order state 15\"},
                            {\"name\": \"Purchase order state 16\"},
                            {\"name\": \"Purchase order state 17\"},
                            {\"name\": \"Purchase order state 18\"},
                            {\"name\": \"Purchase order state 19\"},
                            {\"name\": \"Purchase order state 20\"}
                        ]
                        """));
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
        // Given
        Long id = 1L;

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState/{id}", id));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(content().json(
                """
                            {\"name\": \"Purchase order state 1\"}
                        """));
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
        // Given
        Long id = 1000L;

        // When
        ResultActions result = this.mockMvc.perform(get("/purchaseOrderState/{id}", id));

        // Then
        result.andExpect(status().isNotFound());
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenExistingId_whenDeleting_thenDeletingEntity() throws Exception {
        // Given
        Long id = 1L;

        // When
        ResultActions result = this.mockMvc.perform(delete("/purchaseOrderState/{id}", id));

        // Then
        result.andExpect(status().isNoContent());
        assertFalse(repository.existsById(Long.valueOf(id)),
                "The purchase order state still exists in the database");
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
        // Given
        Long id = 1000L;

        // When
        ResultActions result = this.mockMvc.perform(delete("/purchaseOrderState/{id}", id));

        // Then
        result.andExpect(status().isNotFound());
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
        // Given
        Long id = 1000L;
        PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();

        // When
        ResultActions result = this.mockMvc.perform(
                put("/purchaseOrderState/{id}", id).content(asJsonString(dto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(status().isNotFound());
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenEmptyPurchaseOrderState_whenUpdating_thenError() throws Exception {
        // Given
        Long id = 1L;
        PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();

        // When
        ResultActions result = this.mockMvc.perform(put("/purchaseOrderState/{id}", id).content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(status().isBadRequest());
        verifyOnlyOneFieldError(mapper, result, ApiError.PURCHASE_ORDER_STATE_NOT_VALID, "name");
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenPurchaseOrderStateWithoutName_whenUpdating_thenError() throws Exception {
        // Given
        Long id = 1L;
        PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();

        // When
        ResultActions result = this.mockMvc.perform(put("/purchaseOrderState/{id}", id).content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(status().isBadRequest());
        verifyOnlyOneFieldError(mapper, result, ApiError.PURCHASE_ORDER_STATE_NOT_VALID, "name");
    }

    @Test
    @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
            "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    void givenCorrectPurchaseOrderState_whenUpdating_thenUpdated()
            throws Exception {
        // Given
        Long id = 1L;
        long nbRecords = repository.count();
        String newName = "Test name";
        PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();
        dto.setName(newName);

        // When
        ResultActions result = this.mockMvc.perform(put("/purchaseOrderState/{id}", id).content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(status().isOk());
        assertEquals(nbRecords, repository.count(),
                "No new records should have been created, nor any record deleted");
        Optional<PurchaseOrderState> optNewPurchaseOrderState = repository.findById(id);
        if (optNewPurchaseOrderState.isPresent()) {
            PurchaseOrderState purchaseOrderState = optNewPurchaseOrderState.get();
            assertEquals(newName, purchaseOrderState.getName());
        } else {
            assertFalse(true, "The purchase order state with the given id does not exists anymore in the database");
        }
    }
}
