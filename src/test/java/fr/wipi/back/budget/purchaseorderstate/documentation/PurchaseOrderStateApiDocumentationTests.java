package fr.wipi.back.budget.purchaseorderstate.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.PurchaseOrderStateDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class PurchaseOrderStateApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
                        "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void purchaseOrderStateDeleteExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/purchaseOrderState/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("purchase-order-state-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the purchase order state to delete"))));
        }

        @Test
        @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void purchaseOrderStateAddExample() throws Exception {
                // Given
                PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();
                dto.setName("Test name");

                // When
                ResultActions result = this.mockMvc.perform(post("/purchaseOrderState").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("purchase-order-state-add-example",
                                requestFields(purchaseOrderStateFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
                        "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void purchaseOrderStateGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/purchaseOrderState/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("purchase-order-state-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the purchase order state to retrieve")),
                                responseFields(purchaseOrderStateFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
                        "/db/purchaseOrderState/create-test-data-getting-purchase-order-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void purchaseOrderStateGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/purchaseOrderState?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("purchase-order-state-get-example", queryParameters(parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]").description("An array of purchase order states"))
                                                .andWithPrefix("[].", purchaseOrderStateFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql",
                        "/db/purchaseOrderState/create-test-data-getting-purchase-order-state.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/purchaseOrderState/delete-test-data-truncate-purchase-order-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void purchaseOrderStateUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                String newBudgetName = "Test name";
                PurchaseOrderStateDTO dto = new PurchaseOrderStateDTO();
                dto.setName(newBudgetName);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/purchaseOrderState/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("purchase-order-state-update-example", pathParameters(
                                parameterWithName("id").description("The id of the purchase order state to update")),
                                requestFields(purchaseOrderStateFieldDescriptors(true)),
                                responseFields(purchaseOrderStateFieldDescriptors(true))));
        }

        private FieldDescriptor[] purchaseOrderStateFieldDescriptors(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                list.add(fieldDescriptorForId(creation));
                list.add(fieldDescriptorForName());

                FieldDescriptor[] array = new FieldDescriptor[list.size()];
                list.toArray(array);
                return array;
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the purchase order state." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("name").description(
                                "Name of the purchase order state");
        }
}
