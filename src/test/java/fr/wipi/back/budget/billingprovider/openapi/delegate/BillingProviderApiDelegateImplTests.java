package fr.wipi.back.budget.billingprovider.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import jakarta.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import fr.wipi.back.budget.billingprovider.data.repository.BillingProviderRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingProviderDTO;

@SpringBootTest
class BillingProviderApiDelegateImplTests implements GenericApiDelegateImplTests {
        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        BillingProviderRepository repository;

        private MockMvc mockMvc;

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesBillingProviderApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("billingProviderApiController"));
        }

        @Test
        void givenEmptyProvider_whenCreating_thenError() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID,
                                List.of("name", "contactEmail", "validityStartDate"),
                                List.of("BillingProviderStartDateBeforeEndDateConstraint"));
        }

        @Test
        void givenProviderWithoutName_whenCreating_thenError() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setContactEmail("email@email.com");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID, "name");
        }

        @Test
        void givenProviderWithoutContactEmail_whenCreating_thenError() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID, "contactEmail");
        }

        @Test
        void givenProviderWithIncorrectContactEmail_whenCreating_thenError() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("contactEmail");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID, "contactEmail");
        }

        @Test
        void givenProviderWithoutStartDate_whenCreating_thenError() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("email@email.com");
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID, "validityStartDate");
        }

        @Test
        void givenProviderWithStartDateAfterEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("email@email.com");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID,
                                "BillingProviderStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderCorrect_whenCreating_thenCreated() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("email@email.com");
                dto.setValidityStartDate(LocalDate.of(2024, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2024, 12, 31));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The provider should have been created but there is no provider in the database");
        }

        @Test
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithoutEndDate_whenCreating_thenCreated() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("email@email.com");
                dto.setValidityStartDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The provider should have been created but there is no provider in the database");
        }

        @Test
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithStartDateEqualEndDate_whenCreating_thenCreated() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("email@email.com");
                dto.setValidityStartDate(LocalDate.of(2018, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2018, 1, 1));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The provider should have been created but there is no provider in the database");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Provider 1\", \"contactEmail\": \"contact@provider1.com\"},
                                                    {\"id\": 2, \"name\": \"Provider 2\", \"contactEmail\": \"contact@provider2.com\"},
                                                    {\"id\": 3, \"name\": \"Active provider\", \"contactEmail\": \"contact@provider3.com\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenActiveParams_whenListing_thenReturnActiveProvidersOnly() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                "[{\"id\": 3, \"name\": \"Active provider\", \"contactEmail\": \"contact@provider3.com\"}]"));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNotActiveParams_whenListing_thenReturnNotActiveProvidersOnly() throws Exception {
                // Given
                String active = "false";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Provider 1\", \"contactEmail\": \"contact@provider1.com\"},
                                                    {\"id\": 2, \"name\": \"Provider 2\", \"contactEmail\": \"contact@provider2.com\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Provider 1\"},
                                                    {\"name\": \"Provider 2\"},
                                                    {\"name\": \"Provider 3\"},
                                                    {\"name\": \"Provider 4\"},
                                                    {\"name\": \"Provider 5\"},
                                                    {\"name\": \"Provider 6\"},
                                                    {\"name\": \"Provider 7\"},
                                                    {\"name\": \"Provider 8\"},
                                                    {\"name\": \"Provider 9\"},
                                                    {\"name\": \"Provider 10\"},
                                                    {\"name\": \"Provider 11\"},
                                                    {\"name\": \"Provider 12\"},
                                                    {\"name\": \"Provider 13\"},
                                                    {\"name\": \"Provider 14\"},
                                                    {\"name\": \"Provider 15\"},
                                                    {\"name\": \"Provider 16\"},
                                                    {\"name\": \"Provider 17\"},
                                                    {\"name\": \"Provider 18\"},
                                                    {\"name\": \"Provider 19\"},
                                                    {\"name\": \"Provider 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Provider 1\"},
                                                    {\"name\": \"Provider 2\"},
                                                    {\"name\": \"Provider 3\"},
                                                    {\"name\": \"Provider 4\"},
                                                    {\"name\": \"Provider 5\"},
                                                    {\"name\": \"Provider 6\"},
                                                    {\"name\": \"Provider 7\"},
                                                    {\"name\": \"Provider 8\"},
                                                    {\"name\": \"Provider 9\"},
                                                    {\"name\": \"Provider 10\"},
                                                    {\"name\": \"Provider 11\"},
                                                    {\"name\": \"Provider 12\"},
                                                    {\"name\": \"Provider 13\"},
                                                    {\"name\": \"Provider 14\"},
                                                    {\"name\": \"Provider 15\"},
                                                    {\"name\": \"Provider 16\"},
                                                    {\"name\": \"Provider 17\"},
                                                    {\"name\": \"Provider 18\"},
                                                    {\"name\": \"Provider 19\"},
                                                    {\"name\": \"Provider 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Provider 21\"},
                                                    {\"name\": \"Provider 22\"},
                                                    {\"name\": \"Provider 23\"},
                                                    {\"name\": \"Provider 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/billingProvider?page=" + page + "&size=" + nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Provider 11\"},
                                                    {\"name\": \"Provider 12\"},
                                                    {\"name\": \"Provider 13\"},
                                                    {\"name\": \"Provider 14\"},
                                                    {\"name\": \"Provider 15\"},
                                                    {\"name\": \"Provider 16\"},
                                                    {\"name\": \"Provider 17\"},
                                                    {\"name\": \"Provider 18\"},
                                                    {\"name\": \"Provider 19\"},
                                                    {\"name\": \"Provider 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider/" + id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"name\": \"Provider 1\"}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider/" + id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingProvider/" + id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)),
                                "The billing provider still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingIdWithLinkedInvoice_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 2L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingProvider/" + id));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_DELETABLE_LINKED_TO_INVOICE, "id");
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingProvider/" + id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;
                BillingProviderDTO dto = new BillingProviderDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyProvider_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingProviderDTO dto = new BillingProviderDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID,
                                List.of("name", "contactEmail", "validityStartDate"),
                                List.of("BillingProviderStartDateBeforeEndDateConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithoutName_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setContactEmail("email@email.com");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID, "name");
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithoutContactEmail_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID, "contactEmail");
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithIncorrectContactEmail_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("contactEmail");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID, "contactEmail");
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithoutStartDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("email@email.com");
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID, "validityStartDate");
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithStartDateAfterEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("email@email.com");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_PROVIDER_NOT_VALID,
                                "BillingProviderStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderCorrect_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                String newContactEmail = "email@email.com";
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = LocalDate.of(2024, 12, 31);
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName(newName);
                dto.setContactEmail(newContactEmail);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingProvider> optNewProvider = repository.findById(id);
                if (optNewProvider.isPresent()) {
                        BillingProvider provider = optNewProvider.get();
                        assertEquals(newName, provider.getName());
                        assertEquals(newContactEmail, provider.getContactEmail());
                        assertEquals(newValidityStartDate, provider.getValidityStartDate());
                        assertEquals(newValidityEndDate, provider.getValidityEndDate());
                } else {
                        assertFalse(true, "The provider with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithoutEndDate_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                String newContactEmail = "email@email.com";
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = null;
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName(newName);
                dto.setContactEmail(newContactEmail);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingProvider> optNewProvider = repository.findById(id);
                if (optNewProvider.isPresent()) {
                        BillingProvider provider = optNewProvider.get();
                        assertEquals(newName, provider.getName());
                        assertEquals(newContactEmail, provider.getContactEmail());
                        assertEquals(newValidityStartDate, provider.getValidityStartDate());
                        assertEquals(newValidityEndDate, provider.getValidityEndDate());
                } else {
                        assertFalse(true, "The provider with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderWithStartDateEqualEndDate_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                String newContactEmail = "email@email.com";
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = newValidityStartDate;
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName(newName);
                dto.setContactEmail(newContactEmail);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingProvider> optNewProvider = repository.findById(id);
                if (optNewProvider.isPresent()) {
                        BillingProvider provider = optNewProvider.get();
                        assertEquals(newName, provider.getName());
                        assertEquals(newContactEmail, provider.getContactEmail());
                        assertEquals(newValidityStartDate, provider.getValidityStartDate());
                        assertEquals(newValidityEndDate, provider.getValidityEndDate());
                } else {
                        assertFalse(true, "The provider with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderCorrectButLinkedToInvoice_whenUpdating_thenBadRequest() throws Exception {
                // Given
                Long id = 2L;
                String newName = "Test name";
                String newContactEmail = "email@email.com";
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = LocalDate.of(2024, 12, 31);
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName(newName);
                dto.setContactEmail(newContactEmail);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PROVIDER_NOT_UPDATABLE_LINKED_TO_INVOICE, "id");
        }
}
