package fr.wipi.back.budget.billingprovider.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.BillingProviderDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class BillingProviderApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingProviderDeleteExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingProvider/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("billing-provider-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the billing provider to delete"))));
        }

        @Test
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingProviderAddExample() throws Exception {
                // Given
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName("Test name");
                dto.setContactEmail("contact@test.com");
                dto.setValidityStartDate(LocalDate.of(2024, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2024, 12, 31));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingProvider").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("billing-provider-add-example",
                                requestFields(billingProviderFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingProviderGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-provider-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the billing provider to retrieve")),
                                responseFields(billingProviderFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingProviderGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-provider-get-example", queryParameters(parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]").description("An array of billing providers"))
                                                .andWithPrefix("[].", billingProviderFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingProviderGetActiveExample() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingProvider?active=" + active));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-provider-get-active-example", queryParameters(parameterWithName(
                                "active")
                                .description("A boolean flag to control if only active providers should be returned")),
                                responseFields(fieldWithPath("[]").description("An array of billing providers"))
                                                .andWithPrefix("[].", billingProviderFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingProvider/delete-test-data-truncate-billing-provider.sql",
                        "/db/billingProvider/create-test-data-getting-billing-provider.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingProvider/delete-test-data-truncate-billing-provider.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingProviderUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                String newProviderName = "Test name";
                String newProviderContactEmail = "contact@test.com";
                LocalDate newStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newEndDate = LocalDate.of(2024, 12, 31);
                BillingProviderDTO dto = new BillingProviderDTO();
                dto.setName(newProviderName);
                dto.setContactEmail(newProviderContactEmail);
                dto.setValidityStartDate(newStartDate);
                dto.setValidityEndDate(newEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingProvider/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-provider-update-example", pathParameters(
                                parameterWithName("id").description("The id of the billing provider to update")),
                                requestFields(billingProviderFieldDescriptors(true)),
                                responseFields(billingProviderFieldDescriptors(false))));
        }

        private FieldDescriptor[] billingProviderFieldDescriptors(boolean creation) {
                return new FieldDescriptor[] { fieldDescriptorForId(creation), fieldDescriptorForName(),
                                fieldDescriptorForContactEmail(),
                                fieldDescriptorForValidityStartDate(), fieldDescriptorForEndingDate() };
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the billing provider." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("name").description(
                                "Name of the billing provider");
        }

        private FieldDescriptor fieldDescriptorForContactEmail() {
                return fieldWithPath("contactEmail").description(
                                "Email of contact of the billing provider");
        }

        private FieldDescriptor fieldDescriptorForValidityStartDate() {
                return fieldWithPath("validityStartDate").description(
                                "Validity start date of the provider. Before this date the provider could not be used");
        }

        private FieldDescriptor fieldDescriptorForEndingDate() {
                return fieldWithPath("validityEndDate").description(
                                "Validity end date of the provider. After this date the provider could not be used. If null the provider is always valid starting after the validity start date");
        }

}
