package fr.wipi.back.budget.invoice.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.BillingPeriodDTO;
import fr.wipi.back.budget.openapi.model.BillingProviderDTO;
import fr.wipi.back.budget.openapi.model.InvoiceDTO;
import fr.wipi.back.budget.openapi.model.InvoiceStateDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class InvoiceApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceDeleteExample() throws Exception {
                // Given
                Long id = 2L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/invoice/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("invoice-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the invoice to delete"))));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-creating-invoice.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceAddExample() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                dto.setInvoiceNumber("Invoice number 1");
                dto.setProviderInvoiceNumber("Provider invoice number 1");
                InvoiceStateDTO stateDto = new InvoiceStateDTO();
                stateDto.setId(1L);
                dto.setState(stateDto);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());
                dto.setDescription("Description invoice 1");
                BillingProviderDTO providerDto = new BillingProviderDTO();
                providerDto.setId(1L);
                dto.setProvider(providerDto);
                BillingPeriodDTO periodDto = new BillingPeriodDTO();
                periodDto.setId(1L);
                dto.setPeriod(periodDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("invoice-add-example",
                                requestFields(invoiceFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/invoice/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("invoice-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the invoice to retrieve")),
                                responseFields(invoiceFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceGetExample() throws Exception {
                // Given
                String page = "0";
                Long size = 20L;
                String invoiceNumber = "Invoice number 1";
                String providerInvoiceNumber = "Provider invoice number 1";
                Long state = 1L;
                String periodCoveredDate = "2023-02-12";
                String receptionDate = "2023-01-01";
                String providerSentDate = "2022-12-31";
                String creationDate = "2023-01-01";
                Long provider = 1L;
                Long period = 1L;
                Long budget = 1L;
                Long budgetType = 1L;
                Long budgetGroup = 1L;
                Long budgetManager = 1L;
                String purchaseOrderNumber = "purchaseOrderNumber";

                // When

                ResultActions result = this.mockMvc.perform(get(
                                "/invoice?page={page}&size={size}&invoiceNumber={invoiceNumber}&providerInvoiceNumber={providerInvoiceNumber}&state={state}&periodCoveredDate={periodCoveredDate}&receptionDate={receptionDate}&providerSentDate={providerSentDate}&creationDate={creationDate}&provider={provider}&period={period}&budget={budget}&budgetType={budgetType}&budgetGroup={budgetGroup}&budgetManager={budgetManager}&purchaseOrderNumber={purchaseOrderNumber}",
                                page, size, invoiceNumber, providerInvoiceNumber, state, periodCoveredDate,
                                receptionDate, providerSentDate, creationDate, provider, period, budget, budgetType,
                                budgetGroup, budgetManager, purchaseOrderNumber));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("invoice-get-example", queryParameters(invoiceSearchParameters()),
                                responseFields(fieldWithPath("[]").description("An array of invoices"))
                                                .andWithPrefix("[].", invoiceFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                dto.setInvoiceNumber("Invoice number 1");
                dto.setProviderInvoiceNumber("Provider invoice number 1");
                InvoiceStateDTO stateDto = new InvoiceStateDTO();
                stateDto.setId(1L);
                dto.setState(stateDto);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());
                dto.setDescription("Description invoice 1");
                BillingProviderDTO providerDto = new BillingProviderDTO();
                providerDto.setId(1L);
                dto.setProvider(providerDto);
                BillingPeriodDTO periodDto = new BillingPeriodDTO();
                periodDto.setId(1L);
                dto.setPeriod(periodDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("invoice-update-example", pathParameters(
                                parameterWithName("id").description("The id of the invoice to update")),
                                requestFields(invoiceFieldDescriptors(true)),
                                responseFields(invoiceFieldDescriptors(false))));
        }

        private ParameterDescriptor[] invoiceSearchParameters() {
                List<ParameterDescriptor> list = new ArrayList<>();
                list.add(invoiceSearchPageParameter());
                list.add(invoiceSearchSizeParameter());
                list.add(invoiceSearchInvoiceNumberParameter());
                list.add(invoiceSearchProviderInvoiceNumberParameter());
                list.add(invoiceSearchStatesParameter());
                list.add(invoiceSearchPeriodCoveredDatesParameter());
                list.add(invoiceSearchReceptionDatesParameter());
                list.add(invoiceSearchProviderSentDatesParameter());
                list.add(invoiceSearchCreationDatesParameter());
                list.add(invoiceSearchProvidersParameter());
                list.add(invoiceSearchPeriodsParameter());
                list.add(invoiceSearchBudgetsParameter());
                list.add(invoiceSearchBudgetGroupsParameter());
                list.add(invoiceSearchBudgetTypesParameter());
                list.add(invoiceSearchBudgetManagersParameter());
                list.add(invoiceSearchPurchaseOrderNumbersParameter());

                ParameterDescriptor[] array = new ParameterDescriptor[list.size()];
                list.toArray(array);
                return array;
        }

        private ParameterDescriptor invoiceSearchPageParameter() {
                return parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0");
        }

        private ParameterDescriptor invoiceSearchSizeParameter() {
                return parameterWithName("size")
                                .description("The size of the page of the paginated result to return, by default 20");
        }

        private ParameterDescriptor invoiceSearchInvoiceNumberParameter() {
                return parameterWithName("invoiceNumber")
                                .description("A string that should be contained in the invoice number");
        }

        private ParameterDescriptor invoiceSearchProviderInvoiceNumberParameter() {
                return parameterWithName("providerInvoiceNumber")
                                .description("A string that shoudl be contained in the provider invoice number");
        }

        private ParameterDescriptor invoiceSearchStatesParameter() {
                return parameterWithName("state")
                                .description("The identifier of the state of the invoice. If you want to search invoices at different states put as many 'state' parameter as needed");
        }

        private ParameterDescriptor invoiceSearchPeriodCoveredDatesParameter() {
                return parameterWithName("periodCoveredDate")
                                .description("""
                                                A filter on dates. The filter is compared to invoice detail lines. If at least one invoice detail line matches the filter then the invoice is returned. +
                                                To do the comparison you can specify the operator with the following syntax 'periodCoveredDate=operator:dateFilter'. The operator could have one of the following values : +
                                                * eq : strictly equal to +
                                                * gt : greather than +
                                                * gte : greather than or equal to +
                                                * lt : less than +
                                                * lte : less than or equal to +
                                                If no operator is defined or the operator is not one of the expected one, the comparison is done with the 'eq' (strictly equals to) operator. +

                                                If you want to search invoices with multiples filters on period covered dates, put as many 'periodCoveredDate' parameter as needed
                                                """);
        }

        private ParameterDescriptor invoiceSearchReceptionDatesParameter() {
                return parameterWithName("receptionDate")
                                .description("""
                                                A filter on dates. The filter is compared to the reception date of the invoice. +
                                                To do the comparison you can specify the operator with the following syntax 'receptionDate=operator:dateFilter'. The operator could have one of the following values : +
                                                * eq : strictly equal to +
                                                * gt : greather than +
                                                * gte : greather than or equal to +
                                                * lt : less than +
                                                * lte : less than or equal to +
                                                If no operator is defined or the operator is not one of the expected one, the comparison is done with the 'eq' (strictly equals to) operator. +

                                                If you want to search invoices with multiples filters on reception dates, put as many 'receptionDate' parameter as needed
                                                """);
        }

        private ParameterDescriptor invoiceSearchProviderSentDatesParameter() {
                return parameterWithName("providerSentDate")
                                .description("""
                                                A filter on dates. The filter is compared to the provider sent date of the invoice. +
                                                To do the comparison you can specify the operator with the following syntax 'providerSentDate=operator:dateFilter'. The operator could have one of the following values : +
                                                * eq : strictly equal to +
                                                * gt : greather than +
                                                * gte : greather than or equal to +
                                                * lt : less than +
                                                * lte : less than or equal to +
                                                If no operator is defined or the operator is not one of the expected one, the comparison is done with the 'eq' (strictly equals to) operator. +

                                                If you want to search invoices with multiples filters on provider sent dates, put as many 'providerSentDate' parameter as needed
                                                """);
        }

        private ParameterDescriptor invoiceSearchCreationDatesParameter() {
                return parameterWithName("creationDate")
                                .description("""
                                                A filter on dates. The filter is compared to the creation date of the invoice. +
                                                To do the comparison you can specify the operator with the following syntax 'creationDate=operator:dateFilter'. The operator could have one of the following values : +
                                                * eq : strictly equal to +
                                                * gt : greather than +
                                                * gte : greather than or equal to +
                                                * lt : less than +
                                                * lte : less than or equal to +
                                                If no operator is defined or the operator is not one of the expected one, the comparison is done with the 'eq' (strictly equals to) operator. +

                                                If you want to search invoices with multiples filters on creation dates, put as many 'creationDate' parameter as needed
                                                """);
        }

        private ParameterDescriptor invoiceSearchProvidersParameter() {
                return parameterWithName("provider")
                                .description("The identifier of the billing provider of the invoice. If you want to search invoices linked to different providers put as many 'provider' parameter as needed");
        }

        private ParameterDescriptor invoiceSearchPeriodsParameter() {
                return parameterWithName("period")
                                .description("The identifier of the billing period of the invoice. If you want to search invoices linked to dfiferent periods put as many 'period' parameter as needed");
        }

        private ParameterDescriptor invoiceSearchBudgetsParameter() {
                return parameterWithName("budget")
                                .description("""
                                                The identifier of the billing budget of an invoice detail line. One invoice if at least one invoice detail line is linked to the corresponding budget then the invoice is returned. +

                                                If you want to search invoices linked to different budgets, put as many 'budget' parameter as needed
                                                        """);
        }

        private ParameterDescriptor invoiceSearchBudgetGroupsParameter() {
                return parameterWithName("budgetGroup")
                                .description("""
                                                The identifier of the billing budget group to which the billing budget of an invoice detail line is linked. One invoice if at least one invoice detail line is linked to a budget linked to the corresponding budget group then the invoice is returned. +

                                                If you want to search invoices linked to different budget groups, put as many 'budgetGroup' parameter as needed
                                                        """);
        }

        private ParameterDescriptor invoiceSearchBudgetTypesParameter() {
                return parameterWithName("budgetType")
                                .description("""
                                                The identifier of the billing budget type to which the billing budget of an invoice detail line is linked. One invoice if at least one invoice detail line is linked to a budget linked to the corresponding budget type then the invoice is returned. +

                                                If you want to search invoices linked to different budget types, put as many 'budgetType' parameter as needed
                                                        """);
        }

        private ParameterDescriptor invoiceSearchBudgetManagersParameter() {
                return parameterWithName("budgetManager")
                                .description("""
                                                The identifier of the billing budget manager to which the billing budget of an invoice detail  is linked. One invoice if at least one invoice detail line is linked to a budget linked the corresponding budget manager then the invoice is returned. +

                                                If you want to search invoices linked to different budget managers, put as many 'budgetManager' parameter as needed
                                                        """);
        }

        private ParameterDescriptor invoiceSearchPurchaseOrderNumbersParameter() {
                return parameterWithName("purchaseOrderNumber")
                                .description("""
                                                A string that should be contained in the purchase order number of one of the purchase orders linked to the invoice. +

                                                If you want to search invoices linked to different purchase order numbers, put as many 'purchaseOrderNumber' parameter as needed
                                                        """);
        }

        private FieldDescriptor[] invoiceFieldDescriptors(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                list.add(fieldDescriptorForId(creation));
                list.add(fieldDescriptorForInvoiceNumber());
                list.add(fieldDescriptorForProviderInvoiceNumber());
                list.add(fieldDescriptorForReceptionDate());
                list.add(fieldDescriptorForProviderSentDate());
                list.add(fieldDescriptorForDescription());
                list.add(fieldDescriptorForCreationDate());
                list.addAll(invoiceFieldDescriptorsForProvider(creation));
                list.addAll(invoiceFieldDescriptorsForPeriod(creation));
                list.addAll(invoiceFieldDescriptorsForState(creation));

                FieldDescriptor[] array = new FieldDescriptor[list.size()];
                list.toArray(array);
                return array;
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                if (creation) {
                        return fieldWithPath("id").ignored();
                }
                return fieldWithPath("id").description(
                                "Id of the invoice." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForInvoiceNumber() {
                return fieldWithPath("invoiceNumber").description(
                                "Functional and internal number for the invoice");
        }

        private FieldDescriptor fieldDescriptorForProviderInvoiceNumber() {
                return fieldWithPath("providerInvoiceNumber").description(
                                "Invoice number given by the provider for the invoice");
        }

        private FieldDescriptor fieldDescriptorForReceptionDate() {
                return fieldWithPath("receptionDate").description(
                                "Internal reception date of the invoice");
        }

        private FieldDescriptor fieldDescriptorForProviderSentDate() {
                return fieldWithPath("providerSentDate").description(
                                "Date at which the provider sent the invoice");
        }

        private FieldDescriptor fieldDescriptorForDescription() {
                return fieldWithPath("description").description(
                                "Description of the invoice");
        }

        private FieldDescriptor fieldDescriptorForCreationDate() {
                return fieldWithPath("creationDate").ignored();
        }

        private List<FieldDescriptor> invoiceFieldDescriptorsForProvider(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                if (creation) {
                        list.add(fieldDescriptorForProviderId());
                } else {
                        list.add(fieldDescriptorForProviderIdIgnored());
                        list.add(fieldDescriptorForProvider());
                }

                list.add(fieldDescriptorForProviderName());
                list.add(fieldDescriptorForContactEmail());
                list.add(fieldDescriptorForValidityStartDate());
                list.add(fieldDescriptorForEndingDate());

                return list;
        }

        private FieldDescriptor fieldDescriptorForProvider() {
                return fieldWithPath("provider")
                                .description("Billing provider to which this invoice is linked");
        }

        private FieldDescriptor fieldDescriptorForProviderIdIgnored() {
                return fieldWithPath("provider.id").ignored();
        }

        private FieldDescriptor fieldDescriptorForProviderId() {
                return fieldWithPath("provider.id").description(
                                "Id of the billing provider to which this invoice is linked");
        }

        private FieldDescriptor fieldDescriptorForProviderName() {
                return fieldWithPath("provider.name").ignored();
        }

        private FieldDescriptor fieldDescriptorForContactEmail() {
                return fieldWithPath("provider.contactEmail").ignored();
        }

        private FieldDescriptor fieldDescriptorForValidityStartDate() {
                return fieldWithPath("provider.validityStartDate").ignored();
        }

        private FieldDescriptor fieldDescriptorForEndingDate() {
                return fieldWithPath("provider.validityEndDate").ignored();
        }

        private List<FieldDescriptor> invoiceFieldDescriptorsForPeriod(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                if (creation) {
                        list.add(fieldDescriptorForPeriodId());
                } else {
                        list.add(fieldDescriptorForPeriodIdIgnored());
                        list.add(fieldDescriptorForPeriod());
                }

                list.add(fieldDescriptorForPeriodName());
                list.add(fieldDescriptorForPeriodStartingDate());
                list.add(fieldDescriptorForPeriodEndingDate());

                return list;

        }

        private FieldDescriptor fieldDescriptorForPeriod() {
                return fieldWithPath("period")
                                .description("Billing period to which this invoice is linked");
        }

        private FieldDescriptor fieldDescriptorForPeriodIdIgnored() {
                return fieldWithPath("period.id").ignored();
        }

        private FieldDescriptor fieldDescriptorForPeriodId() {
                return fieldWithPath("period.id").description(
                                "Id of the billing period to which this invoice is linked");
        }

        private FieldDescriptor fieldDescriptorForPeriodName() {
                return fieldWithPath("period.name").ignored();
        }

        private FieldDescriptor fieldDescriptorForPeriodStartingDate() {
                return fieldWithPath("period.startDate").ignored();
        }

        private FieldDescriptor fieldDescriptorForPeriodEndingDate() {
                return fieldWithPath("period.endDate").ignored();
        }

        private List<FieldDescriptor> invoiceFieldDescriptorsForState(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                if (creation) {
                        list.add(fieldDescriptorForStateId());
                } else {
                        list.add(fieldDescriptorForStateIdIgnored());
                        list.add(fieldDescriptorForState());
                }

                list.add(fieldDescriptorForName());

                return list;
        }

        private FieldDescriptor fieldDescriptorForState() {
                return fieldWithPath("state")
                                .description("Invoice state to which this invoice is linked");
        }

        private FieldDescriptor fieldDescriptorForStateId() {
                return fieldWithPath("state.id").description(
                                "Id of the invoice state to which this invoice is linked");
        }

        private FieldDescriptor fieldDescriptorForStateIdIgnored() {
                return fieldWithPath("state.id").ignored();
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("state.name").ignored();
        }

}
