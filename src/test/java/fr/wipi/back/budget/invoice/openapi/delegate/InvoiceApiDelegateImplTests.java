package fr.wipi.back.budget.invoice.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoice.data.repository.InvoiceRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingPeriodDTO;
import fr.wipi.back.budget.openapi.model.BillingProviderDTO;
import fr.wipi.back.budget.openapi.model.InvoiceDTO;
import fr.wipi.back.budget.openapi.model.InvoiceStateDTO;
import jakarta.servlet.ServletContext;

@SpringBootTest
class InvoiceApiDelegateImplTests implements GenericApiDelegateImplTests {
        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        InvoiceRepository repository;

        private MockMvc mockMvc;

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesInvoiceApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("invoiceApiController"));
        }

        @Test
        void givenEmptyInvoice_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.INVOICE_NOT_VALID,
                                List.of("baseBillingItem.period", "baseBillingItem.description", "invoiceNumber",
                                                "providerInvoiceNumber",
                                                "receptionDate"),
                                List.of("InvoiceProviderSentDateBeforeReceptionDateConstraint",
                                                "InvoiceValidBillingProviderConstraint",
                                                "InvoiceValidBillingPeriodConstraint",
                                                "InvoiceValidInvoiceStateConstraint"));
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutProvider_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                dto.setProvider(null);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceValidBillingProviderConstraint");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithInexistingProvider_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(2L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceValidBillingProviderConstraint");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutPeriod_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                dto.setPeriod(null);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.INVOICE_NOT_VALID, null,
                                List.of("InvoiceValidBillingPeriodConstraint",
                                                "InvoiceValidBillingProviderConstraint"));
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithInexistingPeriod_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(2L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.INVOICE_NOT_VALID, null,
                                List.of("InvoiceValidBillingPeriodConstraint",
                                                "InvoiceValidBillingProviderConstraint"));
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutDescription_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription(null);
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_NOT_VALID, "baseBillingItem.description");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutInvoiceNumber_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber(null);
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_NOT_VALID, "invoiceNumber");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutProviderInvoiceNumber_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber(null);
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_NOT_VALID, "providerInvoiceNumber");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutInvoiceState_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                dto.setState(null);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceValidInvoiceStateConstraint");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithInexistingState_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(2L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceValidInvoiceStateConstraint");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutReceptionDate_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(null);
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_NOT_VALID, "receptionDate");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithReceptionDateBeforeProviderSentDate_whenCreating_thenError() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.of(2023, 2, 7));
                dto.setProviderSentDate(LocalDate.of(2023, 2, 8));

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceProviderSentDateBeforeReceptionDateConstraint");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithReceptionDateEqualsToProviderSentDate_whenCreating_thenSuccess() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The invoice should have been created but there is no invoice in the database");
        }

        @Test
        @Sql(scripts = "/db/invoice/create-test-data-creating-invoice.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceComplete_whenCreating_thenSuccess() throws Exception {
                // Given
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.of(2023, 2, 8));
                dto.setProviderSentDate(LocalDate.of(2023, 2, 7));

                // When
                ResultActions result = this.mockMvc.perform(post("/invoice").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The invoice should have been created but there is no invoice in the database");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/invoice"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/invoice"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"description\": \"Invoice 1\"},
                                                    {\"id\": 2, \"description\": \"Invoice 2\"},
                                                    {\"id\": 3, \"description\": \"Invoice 3\"},
                                                    {\"id\": 4, \"description\": \"Invoice 4\"},
                                                    {\"id\": 5, \"description\": \"Invoice 5\"},
                                                    {\"id\": 6, \"description\": \"Invoice 6\"},
                                                    {\"id\": 7, \"description\": \"Invoice 7\"},
                                                    {\"id\": 8, \"description\": \"Invoice 8\"},
                                                    {\"id\": 9, \"description\": \"Invoice 9\"},
                                                    {\"id\": 10, \"description\": \"Invoice 10\"},
                                                    {\"id\": 11, \"description\": \"Invoice 11\"},
                                                    {\"id\": 12, \"description\": \"Invoice 12\"},
                                                    {\"id\": 13, \"description\": \"Invoice 13\"},
                                                    {\"id\": 14, \"description\": \"Invoice 14\"},
                                                    {\"id\": 15, \"description\": \"Invoice 15\"},
                                                    {\"id\": 16, \"description\": \"Invoice 16\"},
                                                    {\"id\": 17, \"description\": \"Invoice 17\"},
                                                    {\"id\": 18, \"description\": \"Invoice 18\"},
                                                    {\"id\": 19, \"description\": \"Invoice 19\"},
                                                    {\"id\": 20, \"description\": \"Invoice 20\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/invoice?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/invoice?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 21, \"description\": \"Invoice 21\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 23, \"description\": \"Invoice 23\"},
                                                        {\"id\": 24, \"description\": \"Invoice 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?page={page}&size={size}", page, nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExactInvoiceNumber_whenListing_thenReturnOneInvoice() throws Exception {
                // Given
                String invoiceNumber = "Invoice number 24";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?invoiceNumber={invoiceNumber}", invoiceNumber));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 24, \"description\": \"Invoice 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPartialInvoiceNumber_whenListing_thenReturnCorrespondingInvoices() throws Exception {
                // Given
                String invoiceNumber = "Invoice number 2";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?invoiceNumber={invoiceNumber}", invoiceNumber));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"},
                                                        {\"id\": 21, \"description\": \"Invoice 21\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 23, \"description\": \"Invoice 23\"},
                                                        {\"id\": 24, \"description\": \"Invoice 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingInvoiceNumber_whenListing_thenReturnEmptyResult() throws Exception {
                // Given
                String invoiceNumber = "Invoice number 0";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?invoiceNumber={invoiceNumber}", invoiceNumber));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExactProviderInvoiceNumber_whenListing_thenReturnOneInvoice() throws Exception {
                // Given
                String providerInvoiceNumber = "Provider invoice number 24";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?providerInvoiceNumber={providerInvoiceNumber}",
                                                providerInvoiceNumber));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 24, \"description\": \"Invoice 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPartialProviderInvoiceNumber_whenListing_thenReturnCorrespondingInvoices() throws Exception {
                // Given
                String providerInvoiceNumber = "Provider invoice number 2";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?providerInvoiceNumber={providerInvoiceNumber}",
                                                providerInvoiceNumber));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"},
                                                        {\"id\": 21, \"description\": \"Invoice 21\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 23, \"description\": \"Invoice 23\"},
                                                        {\"id\": 24, \"description\": \"Invoice 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingProviderInvoiceNumber_whenListing_thenReturnEmptyResult() throws Exception {
                // Given
                String providerInvoiceNumber = "Invoice number 0";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?providerInvoiceNumber={providerInvoiceNumber}",
                                                providerInvoiceNumber));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyState_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?state="));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"}

                                                ]
                                                """));
        }
        
        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenOneExistingState_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given
                Long stateId = 1L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?state={state}", stateId));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenOneNonExistingState_whenListing_thenReturnEmptyResult() throws Exception {
                // Given
                Long stateId = -1L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?state={state}", stateId));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenMultipesExistingStates_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given
                Long stateId1 = 1L;
                Long stateId2 = 2L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?state={state1}&state={state2}", stateId1, stateId2));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 23, \"description\": \"Invoice 23\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenMultipesStatesIncludingNonExisting_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given
                Long stateId1 = 1L;
                Long stateId2 = 3L;
                Long stateId3 = 5L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?state={state1}&state={state2}&state={state3}", stateId1,
                                                stateId2, stateId3));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 21, \"description\": \"Invoice 21\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 24, \"description\": \"Invoice 24\"}
                                                ]
                                                """));
        }

        @ParameterizedTest
        @CsvSource(delimiter = '|', textBlock = """
                        #givenReceptionDateWithoutOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-01' | '/invoice?receptionDate={receptionDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        #givenReceptionDateWithEqualOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-01' | '/invoice?receptionDate=eq:{receptionDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        #givenReceptionDateWithLessThanOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-05' | '/invoice?receptionDate=lt:{receptionDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"},{\"id\": 2, \"description\": \"Invoice 2\"},{\"id\": 3, \"description\": \"Invoice 3\"},{\"id\": 4, \"description\": \"Invoice 4\"}]
                        #givenReceptionDateWithLessThanOrEqualToOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-05' | '/invoice?receptionDate=lte:{receptionDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"},{\"id\": 2, \"description\": \"Invoice 2\"},{\"id\": 3, \"description\": \"Invoice 3\"},{\"id\": 4, \"description\": \"Invoice 4\"},{\"id\": 5, \"description\": \"Invoice 5\"}]
                        #givenReceptionDateWithGreaterThanOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-20' | '/invoice?receptionDate=gt:{receptionDate}' | [{\"id\": 21, \"description\": \"Invoice 21\"},{\"id\": 22, \"description\": \"Invoice 22\"},{\"id\": 23, \"description\": \"Invoice 23\"},{\"id\": 24, \"description\": \"Invoice 24\"}]
                        #givenReceptionDateWithGreaterThanOrEqualToOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-20' | '/invoice?receptionDate=gte:{receptionDate}' | [{\"id\": 20, \"description\": \"Invoice 20\"},{\"id\": 21, \"description\": \"Invoice 21\"},{\"id\": 22, \"description\": \"Invoice 22\"},{\"id\": 23, \"description\": \"Invoice 23\"},{\"id\": 24, \"description\": \"Invoice 24\"}]
                        #givenReceptionDateWithInexistingOperator_whenListing_thenReturnCorrespondingEntitiesUsingEqualOperator
                        '2023-01-01' | '/invoice?receptionDate=nonexisting:{receptionDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        """)
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenReceptionDate_whenListing_thenReturnCorrespondingEntities(String filterDate,
                        String filter, String expectedResults) throws Exception {
                // Given
                String receptionDate = filterDate;

                // When
                ResultActions result = this.mockMvc
                                .perform(get(filter, receptionDate));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(expectedResults));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyReceptionDate_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?receptionDate="));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"}

                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenReceptionDateWithMultipleOperators_whenListing_thenReturnCorrespondingEntities() throws Exception {
                // Given
                String receptionDate1 = "2023-01-01";
                String receptionDate2 = "2023-01-07";
                String receptionDate3 = "2023-01-24";
                String receptionDate4 = "2023-01-15";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?receptionDate=gt:{receptionDate1}&receptionDate=gte:{receptionDate2}&receptionDate=lt:{receptionDate3}&receptionDate=lte:{receptionDate4}",
                                                receptionDate1, receptionDate2, receptionDate3, receptionDate4));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"}
                                                ]
                                                """));
        }

        @ParameterizedTest
        @CsvSource(delimiter = '|', textBlock = """
                        #givenProviderSentDateWithoutOperator_whenListing_thenReturnCorrespondingEntities
                        '2022-12-31' | '/invoice?providerSentDate={providerSentDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        #givenProviderSentDateWithEqualOperator_whenListing_thenReturnCorrespondingEntities
                        '2022-12-31' | '/invoice?providerSentDate=eq:{providerSentDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        #givenProviderSentDateWithLessThanOperator_whenListing_thenReturnCorrespondingEntities
                        '2022-12-12' | '/invoice?providerSentDate=lt:{providerSentDate}' | [{\"id\": 21, \"description\": \"Invoice 21\"},{\"id\": 22, \"description\": \"Invoice 22\"},{\"id\": 23, \"description\": \"Invoice 23\"},{\"id\": 24, \"description\": \"Invoice 24\"}]
                        #givenProviderSentDateWithLessThanOrEqualToOperator_whenListing_thenReturnCorrespondingEntities
                        '2022-12-12' | '/invoice?providerSentDate=lte:{providerSentDate}' | [{\"id\": 20, \"description\": \"Invoice 20\"},{\"id\": 21, \"description\": \"Invoice 21\"},{\"id\": 22, \"description\": \"Invoice 22\"},{\"id\": 23, \"description\": \"Invoice 23\"},{\"id\": 24, \"description\": \"Invoice 24\"}]
                        #givenProviderSentDateWithGreaterThanOperator_whenListing_thenReturnCorrespondingEntities
                        '2022-12-27' | '/invoice?providerSentDate=gt:{providerSentDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"},{\"id\": 2, \"description\": \"Invoice 2\"},{\"id\": 3, \"description\": \"Invoice 3\"},{\"id\": 4, \"description\": \"Invoice 4\"}]
                        #givenProviderSentDateWithGreaterThanOrEqualToOperator_whenListing_thenReturnCorrespondingEntities
                        '2022-12-27' | '/invoice?providerSentDate=gte:{providerSentDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"},{\"id\": 2, \"description\": \"Invoice 2\"},{\"id\": 3, \"description\": \"Invoice 3\"},{\"id\": 4, \"description\": \"Invoice 4\"},{\"id\": 5, \"description\": \"Invoice 5\"}]
                        #givenProviderSentDateWithInexistingOperator_whenListing_thenReturnCorrespondingEntitiesUsingEqualOperator
                        '2022-12-31' | '/invoice?providerSentDate=nonexisting:{providerSentDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        """)
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderSentDate_whenListing_thenReturnCorrespondingEntities(String filterDate,
                        String filter, String expectedResults) throws Exception {
                // Given
                String providerSentDate = filterDate;

                // When
                ResultActions result = this.mockMvc
                                .perform(get(filter, providerSentDate));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(expectedResults));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyProviderSentDate_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?providerSentDate="));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"}

                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenProviderSentDateWithMultipleOperators_whenListing_thenReturnCorrespondingEntities() throws Exception {
                // Given
                String providerSentDate1 = "2022-12-08";
                String providerSentDate2 = "2022-12-17";
                String providerSentDate3 = "2022-12-31";
                String providerSentDate4 = "2022-12-25";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?providerSentDate=gt:{providerSentDate1}&providerSentDate=gte:{providerSentDate2}&providerSentDate=lt:{providerSentDate3}&providerSentDate=lte:{providerSentDate4}",
                                                providerSentDate1, providerSentDate2, providerSentDate3,
                                                providerSentDate4));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"}
                                                ]
                                                """));
        }

        @ParameterizedTest
        @CsvSource(delimiter = '|', textBlock = """
                        #givenCreationDateWithoutOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-01' | '/invoice?creationDate={creationDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        #givenCreationDateWithEqualOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-01' | '/invoice?creationDate=eq:{creationDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        #givenCreationDateWithLessThanOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-05' | '/invoice?creationDate=lt:{creationDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"},{\"id\": 2, \"description\": \"Invoice 2\"},{\"id\": 3, \"description\": \"Invoice 3\"},{\"id\": 4, \"description\": \"Invoice 4\"}]
                        #givenCreationDateWithLessThanOrEqualToOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-05' | '/invoice?creationDate=lte:{creationDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"},{\"id\": 2, \"description\": \"Invoice 2\"},{\"id\": 3, \"description\": \"Invoice 3\"},{\"id\": 4, \"description\": \"Invoice 4\"},{\"id\": 5, \"description\": \"Invoice 5\"}]
                        #givenCreationDateWithGreaterThanOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-20' | '/invoice?creationDate=gt:{creationDate}' | [{\"id\": 21, \"description\": \"Invoice 21\"},{\"id\": 22, \"description\": \"Invoice 22\"},{\"id\": 23, \"description\": \"Invoice 23\"},{\"id\": 24, \"description\": \"Invoice 24\"}]
                        #givenCreationDateWithGreaterThanOrEqualToOperator_whenListing_thenReturnCorrespondingEntities
                        '2023-01-20' | '/invoice?creationDate=gte:{creationDate}' | [{\"id\": 20, \"description\": \"Invoice 20\"},{\"id\": 21, \"description\": \"Invoice 21\"},{\"id\": 22, \"description\": \"Invoice 22\"},{\"id\": 23, \"description\": \"Invoice 23\"},{\"id\": 24, \"description\": \"Invoice 24\"}]
                        #givenCreationDateWithInexistingOperator_whenListing_thenReturnCorrespondingEntitiesUsingEqualOperator
                        '2023-01-01' | '/invoice?creationDate=nonexisting:{creationDate}' | [{\"id\": 1, \"description\": \"Invoice 1\"}]
                        """)
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenCreationDate_whenListing_thenReturnCorrespondingEntities(String filterDate,
                        String filter, String expectedResults) throws Exception {
                // Given
                String creationDate = filterDate;

                // When
                ResultActions result = this.mockMvc
                                .perform(get(filter, creationDate));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(expectedResults));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyCreationDate_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?creationDate="));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"}

                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenCreationDateWithMultipleOperators_whenListing_thenReturnCorrespondingEntities() throws Exception {
                // Given
                String creationDate1 = "2023-01-01";
                String creationDate2 = "2023-01-07";
                String creationDate3 = "2023-01-24";
                String creationDate4 = "2023-01-15";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?creationDate=gt:{creationDate1}&creationDate=gte:{creationDate2}&creationDate=lt:{creationDate3}&creationDate=lte:{creationDate4}",
                                                creationDate1, creationDate2, creationDate3, creationDate4));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenOneNonExistingProvider_whenListing_thenReturnEmptyResult() throws Exception {
                // Given
                Long providerId = -1L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?provider={provider}", providerId));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyProvider_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?provider="));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"}

                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenMultipesExistingProviders_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given
                Long providerId1 = 1L;
                Long providerId2 = 2L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?provider={provider1}&provider={provider2}", providerId1,
                                                providerId2));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 23, \"description\": \"Invoice 23\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenMultipesProvidersIncludingNonExisting_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given
                Long providerId1 = 1L;
                Long providerId2 = 3L;
                Long providerId3 = 5L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?provider={provider1}&provider={provider2}&provider={provider3}",
                                                providerId1,
                                                providerId2, providerId3));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 21, \"description\": \"Invoice 21\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 24, \"description\": \"Invoice 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenOneNonExistingPeriod_whenListing_thenReturnEmptyResult() throws Exception {
                // Given
                Long periodId = -1L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?period={period}", periodId));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyPeriod_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?period="));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"}

                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenMultipesExistingPeriods_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given
                Long periodId1 = 1L;
                Long periodId2 = 2L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?period={period1}&period={period2}", periodId1, periodId2));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 2, \"description\": \"Invoice 2\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 5, \"description\": \"Invoice 5\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 8, \"description\": \"Invoice 8\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 11, \"description\": \"Invoice 11\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 14, \"description\": \"Invoice 14\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 17, \"description\": \"Invoice 17\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 20, \"description\": \"Invoice 20\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 23, \"description\": \"Invoice 23\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenMultipesPeriodsIncludingNonExisting_whenListing_thenReturnCorrespondingResults() throws Exception {
                // Given
                Long periodId1 = 1L;
                Long periodId2 = 3L;
                Long periodId3 = 5L;

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoice?period={period1}&period={period2}&period={period3}", periodId1,
                                                periodId2, periodId3));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                        {\"id\": 1, \"description\": \"Invoice 1\"},
                                                        {\"id\": 3, \"description\": \"Invoice 3\"},
                                                        {\"id\": 4, \"description\": \"Invoice 4\"},
                                                        {\"id\": 6, \"description\": \"Invoice 6\"},
                                                        {\"id\": 7, \"description\": \"Invoice 7\"},
                                                        {\"id\": 9, \"description\": \"Invoice 9\"},
                                                        {\"id\": 10, \"description\": \"Invoice 10\"},
                                                        {\"id\": 12, \"description\": \"Invoice 12\"},
                                                        {\"id\": 13, \"description\": \"Invoice 13\"},
                                                        {\"id\": 15, \"description\": \"Invoice 15\"},
                                                        {\"id\": 16, \"description\": \"Invoice 16\"},
                                                        {\"id\": 18, \"description\": \"Invoice 18\"},
                                                        {\"id\": 19, \"description\": \"Invoice 19\"},
                                                        {\"id\": 21, \"description\": \"Invoice 21\"},
                                                        {\"id\": 22, \"description\": \"Invoice 22\"},
                                                        {\"id\": 24, \"description\": \"Invoice 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/invoice/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"description\": \"Invoice 1\"}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/invoice/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingIdOfNonValidatedInvoice_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 2L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/invoice/{id}", id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)),
                                "The invoice still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingIdOfValidatedInvoice_whenDeleting_thenBadRequest() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/invoice/{id}", id));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_VALIDATED_NOT_DELETABLE, "state");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/invoice/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;
                InvoiceDTO dto = new InvoiceDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyInvoice_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.INVOICE_NOT_VALID,
                                List.of("baseBillingItem.period", "baseBillingItem.description", "invoiceNumber",
                                                "providerInvoiceNumber",
                                                "receptionDate"),
                                List.of("InvoiceProviderSentDateBeforeReceptionDateConstraint",
                                                "InvoiceValidBillingProviderConstraint",
                                                "InvoiceValidBillingPeriodConstraint",
                                                "InvoiceValidInvoiceStateConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutProvider_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                dto.setProvider(null);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceValidBillingProviderConstraint");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithInexistingProvider_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1000L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceValidBillingProviderConstraint");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutPeriod_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                dto.setPeriod(null);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.INVOICE_NOT_VALID, null,
                                List.of("InvoiceValidBillingPeriodConstraint",
                                                "InvoiceValidBillingProviderConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithInexistingPeriod_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1000L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.INVOICE_NOT_VALID, null,
                                List.of("InvoiceValidBillingPeriodConstraint",
                                                "InvoiceValidBillingProviderConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutDescription_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription(null);
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_NOT_VALID, "baseBillingItem.description");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutInvoiceNumber_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber(null);
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_NOT_VALID, "invoiceNumber");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutProviderInvoiceNumber_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber(null);
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_NOT_VALID, "providerInvoiceNumber");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutInvoiceState_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                dto.setState(null);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceValidInvoiceStateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithInexistingState_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1000L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.now());
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceValidInvoiceStateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithoutReceptionDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(null);
                dto.setProviderSentDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_NOT_VALID, "receptionDate");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithReceptionDateBeforeProviderSentDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(1L);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(1L);
                dto.setPeriod(periodDTO);
                dto.setDescription("Description");
                dto.setInvoiceNumber("1");
                dto.setProviderInvoiceNumber("1");
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(1L);
                dto.setState(stateDTO);
                dto.setReceptionDate(LocalDate.of(2023, 2, 7));
                dto.setProviderSentDate(LocalDate.of(2023, 2, 8));

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.INVOICE_NOT_VALID,
                                "InvoiceProviderSentDateBeforeReceptionDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceWithReceptionDateEqualsToProviderSentDate_whenUpdating_thenSuccess() throws Exception {
                // Given
                Long id = 1L;
                Long nbRecords = repository.count();
                Long newProviderId = 2L;
                Long newPeriodId = 2L;
                String newDescription = "New description";
                String newInvoiceNumber = "2";
                String newProviderInvoiceNumber = "2";
                Long newStateId = 2L;
                LocalDate newReceptionDate = LocalDate.of(2023, 2, 11);
                LocalDate newProviderSentDate = LocalDate.of(2023, 2, 11);

                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(newProviderId);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(newPeriodId);
                dto.setPeriod(periodDTO);
                dto.setDescription(newDescription);
                dto.setInvoiceNumber(newInvoiceNumber);
                dto.setProviderInvoiceNumber(newProviderInvoiceNumber);
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(newStateId);
                dto.setState(stateDTO);
                dto.setReceptionDate(newReceptionDate);
                dto.setProviderSentDate(newProviderSentDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<Invoice> optNewInvoice = repository.findById(id);
                if (optNewInvoice.isPresent()) {
                        Invoice invoice = optNewInvoice.get();
                        assertEquals(newProviderId, invoice.getBaseBillingItem().getProvider().getId());
                        assertEquals(newPeriodId, invoice.getBaseBillingItem().getPeriod().getId());
                        assertEquals(newDescription, invoice.getBaseBillingItem().getDescription());
                        assertEquals(newInvoiceNumber, invoice.getInvoiceNumber());
                        assertEquals(newProviderInvoiceNumber, invoice.getProviderInvoiceNumber());
                        assertEquals(newStateId, invoice.getState().getId());
                        assertEquals(newReceptionDate, invoice.getReceptionDate());
                        assertEquals(newProviderSentDate, invoice.getProviderSentDate());
                } else {
                        assertFalse(true, "The invoice with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/invoice/delete-test-data-truncate-invoice.sql",
                        "/db/invoice/create-test-data-getting-invoice-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoice/delete-test-data-truncate-invoice.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceComplete_whenUpdating_thenSuccess() throws Exception {
                // Given
                Long id = 1L;
                Long nbRecords = repository.count();
                Long newProviderId = 2L;
                Long newPeriodId = 2L;
                String newDescription = "New description";
                String newInvoiceNumber = "2";
                String newProviderInvoiceNumber = "2";
                Long newStateId = 2L;
                LocalDate newReceptionDate = LocalDate.of(2023, 2, 11);
                LocalDate newProviderSentDate = LocalDate.of(2023, 2, 10);

                InvoiceDTO dto = new InvoiceDTO();
                BillingProviderDTO providerDTO = new BillingProviderDTO();
                providerDTO.setId(newProviderId);
                dto.setProvider(providerDTO);
                BillingPeriodDTO periodDTO = new BillingPeriodDTO();
                periodDTO.setId(newPeriodId);
                dto.setPeriod(periodDTO);
                dto.setDescription(newDescription);
                dto.setInvoiceNumber(newInvoiceNumber);
                dto.setProviderInvoiceNumber(newProviderInvoiceNumber);
                InvoiceStateDTO stateDTO = new InvoiceStateDTO();
                stateDTO.setId(newStateId);
                dto.setState(stateDTO);
                dto.setReceptionDate(newReceptionDate);
                dto.setProviderSentDate(newProviderSentDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/invoice/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<Invoice> optNewInvoice = repository.findById(id);
                if (optNewInvoice.isPresent()) {
                        Invoice invoice = optNewInvoice.get();
                        assertEquals(newProviderId, invoice.getBaseBillingItem().getProvider().getId());
                        assertEquals(newPeriodId, invoice.getBaseBillingItem().getPeriod().getId());
                        assertEquals(newDescription, invoice.getBaseBillingItem().getDescription());
                        assertEquals(newInvoiceNumber, invoice.getInvoiceNumber());
                        assertEquals(newProviderInvoiceNumber, invoice.getProviderInvoiceNumber());
                        assertEquals(newStateId, invoice.getState().getId());
                        assertEquals(newReceptionDate, invoice.getReceptionDate());
                        assertEquals(newProviderSentDate, invoice.getProviderSentDate());
                } else {
                        assertFalse(true, "The invoice with the given id does not exists anymore in the database");
                }
        }
}
