package fr.wipi.back.budget.invoice.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingperiod.data.repository.BillingPeriodRepository;
import fr.wipi.back.budget.invoice.data.model.Invoice;

@SpringBootTest
class InvoiceValidBillingPeriodValidatorTests {
    @Autowired
    private InvoiceValidBillingPeriodValidator validator;

    @MockBean
    BillingPeriodRepository periodRepository;

    @Test
    void whenInvoiceIsNull_thenTestFailed() {
        // Given
        Invoice invoice = null;

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the Invoice object is null, the validator should fail");
    }

    @Test
    void whenInvoiceNotLinkedToBillingPeriod_thenTestFailed() {
        // Given
        Invoice invoice = new Invoice();
        invoice.getBaseBillingItem().setPeriod(null);

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid,
                "When the period of the Invoice object is null, the validator should fail");
    }

    @Test
    void whenInvoiceNotLinkedToBillingPeriodWithoutId_thenTestFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setId(null);
        Invoice invoice = new Invoice();
        invoice.getBaseBillingItem().setPeriod(period);

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid,
                "When the period of the Invoice object is null, the validator should fail");
    }

    @Test
    void whenInvoiceLinkedToNonExistingPeriod_thenTestFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setId(1L);
        Invoice invoice = new Invoice();
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(periodRepository.findById(1L)).thenReturn(Optional.empty());

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the period of the Invoice object do not exists, the validator should fail");
    }

    @Test
    void whenInvoiceLinkedToExistingPeriod_thenTestSucceeded() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setId(1L);
        Invoice invoice = new Invoice();
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(periodRepository.findById(1L)).thenReturn(Optional.of(period));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid, "When the period of the Invoice object exists, the validator should succeed");
    }
}
