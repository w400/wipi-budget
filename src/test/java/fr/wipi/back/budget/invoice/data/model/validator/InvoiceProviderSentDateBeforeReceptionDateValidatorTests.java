package fr.wipi.back.budget.invoice.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import fr.wipi.back.budget.invoice.data.model.Invoice;

@SpringBootTest
class InvoiceProviderSentDateBeforeReceptionDateValidatorTests {

    private InvoiceProviderSentDateBeforeReceptionDateValidator validator = new InvoiceProviderSentDateBeforeReceptionDateValidator();
    
    @Test
    void whenInvoiceIsNull_thenTestFailed() {
        // Given
        Invoice invoice = null;

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the Invoice object is null, the validator should failed");
    }

    @Test
    void whenEndDateIsNull_thenTestSucceed() {
        // Given
        Invoice invoice = new Invoice();
        invoice.setProviderSentDate(LocalDate.now());
        invoice.setReceptionDate(null);

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid, "When the reception date is null, the validator should succeed");
    }

    @Test
    void whenStartDateIsNull_thenTestFailed() {
        // Given
        Invoice invoice = new Invoice();
        invoice.setProviderSentDate(null);
        invoice.setReceptionDate(LocalDate.now());

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the provider sent date is null, the validator should failed");
    }

    @Test
    void whenStartDateBeforeEndDate_thenTestSucceeded() {
        // Given
        Invoice invoice = new Invoice();
        invoice.setProviderSentDate(LocalDate.of(2022, 01, 01));
        invoice.setReceptionDate(LocalDate.of(2022, 12, 31));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid, "When the provider sent date is before the reception date, the validator should succeed");
    }

    @Test
    void whenStartDateEqualEndDate_thenTestSucceed() {
        // Given
        Invoice invoice = new Invoice();
        invoice.setProviderSentDate(LocalDate.of(2022, 01, 01));
        invoice.setReceptionDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid, "When the provider sent date is strictly equal to the reception date, the validator should succeed");
    }

    @Test
    void whenStartDateAfterEndDate_thenTestFailed() {
        // Given
        Invoice invoice = new Invoice();
        invoice.setProviderSentDate(LocalDate.of(2023, 01, 01));
        invoice.setReceptionDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the provider sent date is after the reception date, the validator should failed");
    }
}
