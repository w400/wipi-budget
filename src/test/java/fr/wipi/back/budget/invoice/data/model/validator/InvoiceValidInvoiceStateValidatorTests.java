package fr.wipi.back.budget.invoice.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.wipi.back.budget.invoice.data.model.Invoice;
import fr.wipi.back.budget.invoicestate.data.model.InvoiceState;
import fr.wipi.back.budget.invoicestate.data.repository.InvoiceStateRepository;

@SpringBootTest
class InvoiceValidInvoiceStateValidatorTests {
    @Autowired
    private InvoiceValidInvoiceStateValidator validator;

    @MockBean
    InvoiceStateRepository stateRepository;

    @Test
    void whenInvoiceIsNull_thenTestFailed() {
        // Given
        Invoice invoice = null;

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the Invoice object is null, the validator should fail");
    }

    @Test
    void whenInvoiceNotLinkedToInvoiceState_thenTestFailed() {
        // Given
        Invoice invoice = new Invoice();
        invoice.setState(null);

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid,
                "When the state of the Invoice object is null, the validator should fail");
    }

    @Test
    void whenInvoiceLinkedToNonExistingState_thenTestFailed() {
        // Given
        InvoiceState state = new InvoiceState();
        state.setId(1L);
        Invoice invoice = new Invoice();
        invoice.setState(state);
        Mockito.when(stateRepository.findById(1L)).thenReturn(Optional.empty());

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the state of the Invoice object do not exists, the validator should fail");
    }

    @Test
    void whenInvoiceLinkedToExistingState_thenTestSucceeded() {
        // Given
        InvoiceState state = new InvoiceState();
        state.setId(1L);
        Invoice invoice = new Invoice();
        invoice.setState(state);
        Mockito.when(stateRepository.findById(1L)).thenReturn(Optional.of(state));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid, "When the state of the Invoice object exists, the validator should succeed");
    }
}
