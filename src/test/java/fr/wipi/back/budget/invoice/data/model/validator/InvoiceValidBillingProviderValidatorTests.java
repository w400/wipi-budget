package fr.wipi.back.budget.invoice.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingprovider.data.model.BillingProvider;
import fr.wipi.back.budget.billingprovider.data.repository.BillingProviderRepository;
import fr.wipi.back.budget.invoice.data.model.Invoice;

@SpringBootTest
class InvoiceValidBillingProviderValidatorTests {
    @Autowired
    private InvoiceValidBillingProviderValidator validator;

    @MockBean
    BillingProviderRepository providerRepository;

    @Test
    void whenInvoiceIsNull_thenTestFailed() {
        // Given
        Invoice invoice = null;

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the Invoice object is null, the validator should failed");
    }

    @Test
    void whenInvoiceNotLinkedToBillingProvider_thenTestFailed() {
        // Given
        Invoice invoice = new Invoice();
        invoice.getBaseBillingItem().setProvider(null);

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid,
                "When the provider of the Invoice object is null, the validator should failed");
    }

    @Test
    void whenInvoiceNotLinkedToPeriod_thenTestFailed() {
        // Given
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        Invoice invoice = new Invoice();
        invoice.getBaseBillingItem().setProvider(provider);
        invoice.getBaseBillingItem().setPeriod(null);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid, "When the period of the Invoice object is null, the validator should failed");
    }

    @Test
    void whenInvoicePeriodStartDateBeforeBillingProviderValidityStartDate_thenTestFailed() {
        // Given
        Invoice invoice = new Invoice();
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        invoice.getBaseBillingItem().setProvider(provider);
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2021, 12, 31));
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid,
                "When the start date of the period of the Invoice object is before the validity start date of the BillingProvider object, the validator should failed");
    }

    @Test
    void whenInvoicePeriodStartDateEqualsBillingProviderValidityStartDate_thenTestSucceed() {
        // Given
        Invoice invoice = new Invoice();
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        invoice.getBaseBillingItem().setProvider(provider);
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 1, 1));
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid,
                "When the start date of the period of the Invoice object is equal to the validity start date of the BillingProvider object, the validator should succeed");
    }

    @Test
    void whenInvoicePeriodStartDateAfterBillingProviderValidityStartDate_thenTestSucceed() {
        // Given
        Invoice invoice = new Invoice();
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        invoice.getBaseBillingItem().setProvider(provider);
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 1, 2));
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid,
                "When the start date of the period of the Invoice object is after to the validity start date of the BillingProvider object, the validator should succeed");
    }

    @Test
    void whenInvoicePeriodEndDateNullButNotBillingProviderValidityEndDate_thenTestSucceed() {
        // Given
        Invoice invoice = new Invoice();
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        provider.setValidityEndDate(LocalDate.of(2022, 12, 31));
        invoice.getBaseBillingItem().setProvider(provider);
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 1, 1));
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid,
                "When the end date of the period of the Invoice object is null but not the validity end date of the BillingProvider object, the validator should failed");
    }

    @Test
    void whenInvoicePeriodEndDateBeforeBillingProviderValidityEndDate_thenTestSucceed() {
        // Given
        Invoice invoice = new Invoice();
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        provider.setValidityEndDate(LocalDate.of(2022, 12, 31));
        invoice.getBaseBillingItem().setProvider(provider);
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 1, 1));
        period.setEndDate(LocalDate.of(2022, 12, 30));
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid,
                "When the end date of the period of the Invoice object is before the validity end date of the BillingProvider object, the validator should succeed");
    }

    @Test
    void whenInvoicePeriodEndDateEqualsBillingProviderValidityEndDate_thenTestSucceed() {
        // Given
        Invoice invoice = new Invoice();
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        provider.setValidityEndDate(LocalDate.of(2022, 12, 31));
        invoice.getBaseBillingItem().setProvider(provider);
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 1, 1));
        period.setEndDate(LocalDate.of(2022, 12, 31));
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid,
                "When the end date of the period of the Invoice object is equal to the validity end date of the BillingProvider object, the validator should succeed");
    }

    @Test
    void whenInvoicePeriodEndDateAfterBillingProviderValidityEndDate_thenTestFailed() {
        // Given
        Invoice invoice = new Invoice();
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        provider.setValidityEndDate(LocalDate.of(2022, 12, 31));
        invoice.getBaseBillingItem().setProvider(provider);
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 1, 1));
        period.setEndDate(LocalDate.of(2023, 1, 1));
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid,
                "When the end date of the period of the Invoice object is after the validity end date of the BillingProvider object, the validator should failed");
    }

    @Test
    void whenInvoicePeriodEndDateNotNullButBillingProviderValidityEndDateNull_thenTestSucceed() {
        // Given
        Invoice invoice = new Invoice();
        BillingProvider provider = new BillingProvider();
        provider.setId(1L);
        provider.setValidityStartDate(LocalDate.of(2022, 1, 1));
        provider.setValidityEndDate(null);
        invoice.getBaseBillingItem().setProvider(provider);
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 1, 1));
        period.setEndDate(LocalDate.of(2022, 12, 31));
        invoice.getBaseBillingItem().setPeriod(period);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertTrue(isValid,
                "When the end date of the period of the Invoice object is not null but the validity end date of the BillingProvider object is null, the validator should succeed");
    }
}
