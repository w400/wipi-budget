package fr.wipi.back.budget.invoicestate.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.InvoiceStateDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class InvoiceStateApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceStateDeleteExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/invoiceState/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("invoice-state-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the invoice state to delete"))));
        }

        @Test
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceStateAddExample() throws Exception {
                // Given
                InvoiceStateDTO dto = new InvoiceStateDTO();
                dto.setName("Test name");

                // When
                ResultActions result = this.mockMvc.perform(post("/invoiceState").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("invoice-state-add-example",
                                requestFields(invoiceStateFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceStateGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("invoice-state-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the invoice state to retrieve")),
                                responseFields(invoiceStateFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceStateGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("invoice-state-get-example", queryParameters(parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]").description("An array of invoice states"))
                                                .andWithPrefix("[].", invoiceStateFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void invoiceStateUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                String newBudgetName = "Test name";
                InvoiceStateDTO dto = new InvoiceStateDTO();
                dto.setName(newBudgetName);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/invoiceState/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("invoice-state-update-example", pathParameters(
                                parameterWithName("id").description("The id of the invoice state to update")),
                                requestFields(invoiceStateFieldDescriptors(true)),
                                responseFields(invoiceStateFieldDescriptors(true))));
        }

        private FieldDescriptor[] invoiceStateFieldDescriptors(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                list.add(fieldDescriptorForId(creation));
                list.add(fieldDescriptorForName());

                FieldDescriptor[] array = new FieldDescriptor[list.size()];
                list.toArray(array);
                return array;
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the invoice state." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("name").description(
                                "Name of the invoice state");
        }
}
