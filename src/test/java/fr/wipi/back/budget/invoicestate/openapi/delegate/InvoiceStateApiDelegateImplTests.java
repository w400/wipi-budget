package fr.wipi.back.budget.invoicestate.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.invoicestate.data.model.InvoiceState;
import fr.wipi.back.budget.invoicestate.data.repository.InvoiceStateRepository;
import fr.wipi.back.budget.invoicestate.service.InvoiceStateService;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.InvoiceStateDTO;
import jakarta.servlet.ServletContext;

@SpringBootTest
class InvoiceStateApiDelegateImplTests implements GenericApiDelegateImplTests {
        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        InvoiceStateRepository repository;

        @Autowired
        @Spy
        InvoiceStateService service;

        private MockMvc mockMvc;

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesInvoiceStateApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("invoiceStateApiController"));
        }

        @Test
        void givenEmptyInvoiceState_whenCreating_thenError() throws Exception {
                // Given
                InvoiceStateDTO dto = new InvoiceStateDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/invoiceState").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_STATE_NOT_VALID, "name");
        }

        @Test
        void givenInvoiceStateWithoutName_whenCreating_thenError() throws Exception {
                // Given
                InvoiceStateDTO dto = new InvoiceStateDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/invoiceState").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_STATE_NOT_VALID, "name");
        }

        @Test
        void givenCorrectInvoiceState_whenCreating_thenCreated()
                        throws Exception {
                // Given
                InvoiceStateDTO dto = new InvoiceStateDTO();
                dto.setName("Test name");

                // When
                ResultActions result = this.mockMvc.perform(post("/invoiceState").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The invoice state should have been created but there is no invoice state in the database");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Invoice state 1\"},
                                                    {\"id\": 2, \"name\": \"Invoice state 2\"},
                                                    {\"id\": 3, \"name\": \"Active invoice state\"}
                                                ]"""));
        }

        @Test
        void givenActiveParams_whenListing_thenNotManagedParamExceptionThrown() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState").param("active", active));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING, "active");
        }

        @Test
        void givenActiveParams_whenListing_thenNotManagedParamExceptionMethodCalled() throws Exception {
                // Given
                InvoiceStateService spy = Mockito.spy(service);

                // When
                try {
                        spy.findObjects(true, null);
                } catch (Exception e) {
                        // Normal exception
                }

                // Then
                Mockito.verify(spy, times(1)).buildError(ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING,
                                List.of("active"), null, "active parameter not managed", mapper);
        }

        @Test
        void givenNotActiveParams_whenListing_thenNotManagedParamExceptionThrown() throws Exception {
                // Given
                String active = "false";

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState").param("active", active));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING, "active");
        }

        @Test
        void givenNotActiveParams_whenListing_thenNotManagedParamExceptionMethodCalled() throws Exception {
                // Given
                InvoiceStateService spy = Mockito.spy(service);

                // When
                try {
                        spy.findObjects(false, null);
                } catch (Exception e) {
                        // Normal exception
                }

                // Then
                Mockito.verify(spy, times(1)).buildError(ApiError.ACTIVE_PARAM_NOT_MANAGED_IN_LISTING,
                                List.of("active"), null, "active parameter not managed", mapper);
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Invoice state 1\"},
                                                    {\"name\": \"Invoice state 2\"},
                                                    {\"name\": \"Invoice state 3\"},
                                                    {\"name\": \"Invoice state 4\"},
                                                    {\"name\": \"Invoice state 5\"},
                                                    {\"name\": \"Invoice state 6\"},
                                                    {\"name\": \"Invoice state 7\"},
                                                    {\"name\": \"Invoice state 8\"},
                                                    {\"name\": \"Invoice state 9\"},
                                                    {\"name\": \"Invoice state 10\"},
                                                    {\"name\": \"Invoice state 11\"},
                                                    {\"name\": \"Invoice state 12\"},
                                                    {\"name\": \"Invoice state 13\"},
                                                    {\"name\": \"Invoice state 14\"},
                                                    {\"name\": \"Invoice state 15\"},
                                                    {\"name\": \"Invoice state 16\"},
                                                    {\"name\": \"Invoice state 17\"},
                                                    {\"name\": \"Invoice state 18\"},
                                                    {\"name\": \"Invoice state 19\"},
                                                    {\"name\": \"Invoice state 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Invoice state 1\"},
                                                    {\"name\": \"Invoice state 2\"},
                                                    {\"name\": \"Invoice state 3\"},
                                                    {\"name\": \"Invoice state 4\"},
                                                    {\"name\": \"Invoice state 5\"},
                                                    {\"name\": \"Invoice state 6\"},
                                                    {\"name\": \"Invoice state 7\"},
                                                    {\"name\": \"Invoice state 8\"},
                                                    {\"name\": \"Invoice state 9\"},
                                                    {\"name\": \"Invoice state 10\"},
                                                    {\"name\": \"Invoice state 11\"},
                                                    {\"name\": \"Invoice state 12\"},
                                                    {\"name\": \"Invoice state 13\"},
                                                    {\"name\": \"Invoice state 14\"},
                                                    {\"name\": \"Invoice state 15\"},
                                                    {\"name\": \"Invoice state 16\"},
                                                    {\"name\": \"Invoice state 17\"},
                                                    {\"name\": \"Invoice state 18\"},
                                                    {\"name\": \"Invoice state 19\"},
                                                    {\"name\": \"Invoice state 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Invoice state 21\"},
                                                    {\"name\": \"Invoice state 22\"},
                                                    {\"name\": \"Invoice state 23\"},
                                                    {\"name\": \"Invoice state 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/invoiceState?page={page}&size={size}", page, nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Invoice state 11\"},
                                                    {\"name\": \"Invoice state 12\"},
                                                    {\"name\": \"Invoice state 13\"},
                                                    {\"name\": \"Invoice state 14\"},
                                                    {\"name\": \"Invoice state 15\"},
                                                    {\"name\": \"Invoice state 16\"},
                                                    {\"name\": \"Invoice state 17\"},
                                                    {\"name\": \"Invoice state 18\"},
                                                    {\"name\": \"Invoice state 19\"},
                                                    {\"name\": \"Invoice state 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"name\": \"Invoice state 1\"}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/invoiceState/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/invoiceState/{id}", id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)),
                                "The invoice state still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingIdLinkedToInvoice_whenDeleting_thenBadRequest() throws Exception {
                // Given
                Long id = 2L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/invoiceState/{id}", id));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_STATE_NOT_DELETABLE_LINKED_TO_INVOICE, "id");
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/invoiceState/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;
                InvoiceStateDTO dto = new InvoiceStateDTO();

                // When
                ResultActions result = this.mockMvc.perform(
                                put("/invoiceState/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyInvoiceState_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceStateDTO dto = new InvoiceStateDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/invoiceState/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_STATE_NOT_VALID, "name");
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenInvoiceStateWithoutName_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                InvoiceStateDTO dto = new InvoiceStateDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/invoiceState/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.INVOICE_STATE_NOT_VALID, "name");
        }

        @Test
        @Sql(scripts = { "/db/invoiceState/delete-test-data-truncate-invoice-state.sql",
                        "/db/invoiceState/create-test-data-getting-invoice-state-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/invoiceState/delete-test-data-truncate-invoice-state.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenCorrectInvoiceState_whenUpdating_thenUpdated()
                        throws Exception {
                // Given
                Long id = 1L;
                long nbRecords = repository.count();
                String newName = "Test name";
                InvoiceStateDTO dto = new InvoiceStateDTO();
                dto.setName(newName);

                // When
                ResultActions result = this.mockMvc.perform(put("/invoiceState/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<InvoiceState> optNewInvoiceState = repository.findById(id);
                if (optNewInvoiceState.isPresent()) {
                        InvoiceState budget = optNewInvoiceState.get();
                        assertEquals(newName, budget.getName());
                } else {
                        assertFalse(true,
                                        "The invoice state with the given id does not exists anymore in the database");
                }
        }
}
