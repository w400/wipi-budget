package fr.wipi.back.budget;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.ApiErrorResponseDTO;

public interface GenericApiDelegateImplTests {
    public default void verifyOnlyOneFieldError(ObjectMapper mapper, ResultActions resultActions,
            ApiError expectedApiError, String expectedIncorrectFieldName) throws Exception {
        ApiErrorResponseDTO error = extractApiError(mapper, resultActions);
        if (error != null) {
            assertEquals(expectedApiError.getType(), error.getType());
            assertNotNull(error.getIncorrectFields());
            assertEquals(1, error.getIncorrectFields().size(), "We expected only one field to be incorrect");
            assertEquals(expectedIncorrectFieldName, error.getIncorrectFields().get(0),
                    "The incorrect field should be the " + expectedIncorrectFieldName + " field");
        }
    }

    public default void verifyOnlyOneConstraintNotValidated(ObjectMapper mapper, ResultActions resultActions,
            ApiError expectedApiError, String expectedNotValidatedConstraint) throws Exception {
        ApiErrorResponseDTO error = extractApiError(mapper, resultActions);
        if (error != null) {
            assertEquals(expectedApiError.getType(), error.getType());
            assertNotNull(error.getNotValidatedConstraints());
            assertEquals(1, error.getNotValidatedConstraints().size(),
                    "We expected only one constraint to be not validated");
            assertEquals(expectedNotValidatedConstraint, error.getNotValidatedConstraints().get(0),
                    "The not validated constraint should be the " + expectedNotValidatedConstraint + " field");
        }
    }

    public default void verifyMultipleIncorrectFieldsAndConstraints(ObjectMapper mapper, ResultActions resultActions,
            ApiError expectedApiError, List<String> expectedIncorrectFieldNames,
            List<String> expectedNotValidatedConstraints) throws Exception {
        ApiErrorResponseDTO error = extractApiError(mapper, resultActions);
        if (error != null) {
            assertEquals(expectedApiError.getType(), error.getType());

            assertNotNull(error.getIncorrectFields());
            if (expectedIncorrectFieldNames != null) {
                expectedIncorrectFieldNames.forEach(expectedIncorrectFieldName -> assertTrue(
                        error.getIncorrectFields().contains(expectedIncorrectFieldName),
                        "The field " + expectedIncorrectFieldName
                                + " is not marked as invalid as it should have been"));
                error.getIncorrectFields()
                        .forEach(incorrectField -> assertTrue(expectedIncorrectFieldNames.contains(incorrectField),
                                "The field " + incorrectField + " is marked as incorrect but it should not have been"));
            }

            assertNotNull(error.getNotValidatedConstraints());
            if (expectedNotValidatedConstraints != null) {
                expectedNotValidatedConstraints.forEach(expectedNotValidatedConstraint -> assertTrue(
                        error.getNotValidatedConstraints().contains(expectedNotValidatedConstraint),
                        "The constraint " + expectedNotValidatedConstraint
                                + " is not marked as not respected as it should have been"));
                error.getNotValidatedConstraints()
                        .forEach(notValidatedConstraint -> assertTrue(
                                expectedNotValidatedConstraints.contains(notValidatedConstraint),
                                "The constraint " + notValidatedConstraint
                                        + " is marked as not respected but it should not have been"));
            }
        }
    }

    private ApiErrorResponseDTO extractApiError(ObjectMapper mapper, ResultActions resultActions) throws Exception {
        var wrapper = new Object() {
            ApiErrorResponseDTO error = null;
        };

        resultActions.andExpect(mvcResult -> {
            assertTrue(mvcResult.getResolvedException() instanceof ResponseStatusException,
                    "The resolved exception should be of class ResponseStatusException");
            ResponseStatusException internalException = (ResponseStatusException) mvcResult.getResolvedException();
            assertNotNull(internalException);
            assertNotNull(internalException.getReason(),
                    "The reason of the ResponseStatusException should not be null");
            try {
                wrapper.error = mapper.readValue(internalException.getReason(), ApiErrorResponseDTO.class);
            } catch (JsonProcessingException e) {
                assertTrue(false, "The reason is not a string representing an ApiErrorResponseDTO");
            }
        });
        return wrapper.error;
    }
}
