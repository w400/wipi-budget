package fr.wipi.back.budget.billingrepartitionentity.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;

@SpringBootTest
class BillingRepartitionEntityStartDateBeforeEndValidatorTests {

    private BillingRepartitionEntityStartDateBeforeEndDateValidator validator = new BillingRepartitionEntityStartDateBeforeEndDateValidator();
    
    @Test
    void whenRepartitionEntityIsNull_thenTestFailed() {
        // Given
        BillingRepartitionEntity repartitionEntity = null;

        // When
        boolean isValid = validator.isValid(repartitionEntity, null);

        // Then
        assertFalse(isValid, "When the BillingRepartitionEntity object is null, the validator should failed");
    }

    @Test
    void whenEndDateIsNull_thenTestSucceed() {
        // Given
        BillingRepartitionEntity repartitionEntity = new BillingRepartitionEntity();
        repartitionEntity.setValidityStartDate(LocalDate.now());
        repartitionEntity.setValidityEndDate(null);

        // When
        boolean isValid = validator.isValid(repartitionEntity, null);

        // Then
        assertTrue(isValid, "When the end date is null, the validator should succeed");
    }

    @Test
    void whenStartDateIsNull_thenTestFailed() {
        // Given
        BillingRepartitionEntity repartitionEntity = new BillingRepartitionEntity();
        repartitionEntity.setValidityStartDate(null);
        repartitionEntity.setValidityEndDate(LocalDate.now());

        // When
        boolean isValid = validator.isValid(repartitionEntity, null);

        // Then
        assertFalse(isValid, "When the start date is null, the validator should failed");
    }

    @Test
    void whenStartDateBeforeEndDate_thenTestSucceeded() {
        // Given
        BillingRepartitionEntity repartitionEntity = new BillingRepartitionEntity();
        repartitionEntity.setValidityStartDate(LocalDate.of(2022, 01, 01));
        repartitionEntity.setValidityEndDate(LocalDate.of(2022, 12, 31));

        // When
        boolean isValid = validator.isValid(repartitionEntity, null);

        // Then
        assertTrue(isValid, "When the start date is before the end date, the validator should succeed");
    }

    @Test
    void whenStartDateEqualEndDate_thenTestSucceed() {
        // Given
        BillingRepartitionEntity repartitionEntity = new BillingRepartitionEntity();
        repartitionEntity.setValidityStartDate(LocalDate.of(2022, 01, 01));
        repartitionEntity.setValidityEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(repartitionEntity, null);

        // Then
        assertTrue(isValid, "When the start date is strictly equal to the end date, the validator should succeed");
    }

    @Test
    void whenStartDateAfterEndDate_thenTestFailed() {
        // Given
        BillingRepartitionEntity repartitionEntity = new BillingRepartitionEntity();
        repartitionEntity.setValidityStartDate(LocalDate.of(2023, 01, 01));
        repartitionEntity.setValidityEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(repartitionEntity, null);

        // Then
        assertFalse(isValid, "When the start date is after the end date, the validator should failed");
    }
}
