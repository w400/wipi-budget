package fr.wipi.back.budget.billingrepartitionentity.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class BillingRepartitionEntityApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityDeleteExample() throws Exception {
                // Given
                Long id = 2L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingRepartitionEntity/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("billing-repartition-entity-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the billing repartition entity to delete"))));
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityAddExample() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidatorId(1L);
                dto.setValidityStartDate(LocalDate.of(2024, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2024, 12, 31));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("billing-repartition-entity-add-example",
                                requestFields(billingRepartitionEntityFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-repartition-entity-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the billing repartition entity to retrieve")),
                                responseFields(billingRepartitionEntityFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-repartition-entity-get-example", queryParameters(parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]").description("An array of billing repartition entitys"))
                                                .andWithPrefix("[].", billingRepartitionEntityFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityGetActiveExample() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity?active=" + active));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-repartition-entity-get-active-example", queryParameters(parameterWithName(
                                "active")
                                .description("A boolean flag to control if only active repartition entitys should be returned")),
                                responseFields(fieldWithPath("[]").description("An array of billing repartition entitys"))
                                                .andWithPrefix("[].", billingRepartitionEntityFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                String newRepartitionEntityName = "Test name";
                Long newRepartitionValidatorId = 2L;
                LocalDate newStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newEndDate = LocalDate.of(2024, 12, 31);
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName(newRepartitionEntityName);
                dto.setValidatorId(newRepartitionValidatorId);
                dto.setValidityStartDate(newStartDate);
                dto.setValidityEndDate(newEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-repartition-entity-update-example", pathParameters(
                                parameterWithName("id").description("The id of the billing repartition entity to update")),
                                requestFields(billingRepartitionEntityFieldDescriptors(true)),
                                responseFields(billingRepartitionEntityFieldDescriptors(false))));
        }

        private FieldDescriptor[] billingRepartitionEntityFieldDescriptors(boolean creation) {
                return new FieldDescriptor[] { fieldDescriptorForId(creation), fieldDescriptorForName(),
                                fieldDescriptorForValidatorId(),
                                fieldDescriptorForValidityStartDate(), fieldDescriptorForEndingDate() };
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the billing repartition entity." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("name").description(
                                "Name of the billing repartition entity");
        }

        private FieldDescriptor fieldDescriptorForValidatorId() {
                return fieldWithPath("validatorId").description(
                                "Id of the user which acts as validator for the billing repartition entity");
        }

        private FieldDescriptor fieldDescriptorForValidityStartDate() {
                return fieldWithPath("validityStartDate").description(
                                "Validity start date of the repartition entity. Before this date the repartition entity could not be used");
        }

        private FieldDescriptor fieldDescriptorForEndingDate() {
                return fieldWithPath("validityEndDate").description(
                                "Validity end date of the repartition entity. After this date the repartition entity could not be used. If null the repartition entity is always valid starting after the validity start date");
        }

}
