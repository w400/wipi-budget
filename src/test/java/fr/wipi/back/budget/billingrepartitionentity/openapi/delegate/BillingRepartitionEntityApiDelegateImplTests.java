package fr.wipi.back.budget.billingrepartitionentity.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;
import fr.wipi.back.budget.billingrepartitionentity.data.repository.BillingRepartitionEntityRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityDTO;
import jakarta.servlet.ServletContext;

@SpringBootTest
class BillingRepartitionEntityApiDelegateImplTests implements GenericApiDelegateImplTests {
        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        BillingRepartitionEntityRepository repository;

        private MockMvc mockMvc;

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesBillingRepartitionEntityApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("billingRepartitionEntityApiController"));
        }

        @Test
        void givenEmptyRepartitionEntity_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result,
                                ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID,
                                List.of("name", "validatorId", "validityStartDate"),
                                List.of("BillingRepartitionEntityStartDateBeforeEndDateConstraint"));
        }

        @Test
        void givenRepartitionEntityWithoutName_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setValidatorId(1L);
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID, "name");
        }

        @Test
        void givenRepartitionEntityWithoutValidatorId_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID, "validatorId");
        }

        @Test
        void givenRepartitionEntityWithoutStartDate_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidatorId(1L);
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID,
                                "validityStartDate");
        }

        @Test
        void givenRepartitionEntityWithStartDateAfterEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidatorId(1L);
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID,
                                "BillingRepartitionEntityStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityCorrect_whenCreating_thenCreated() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidatorId(1L);
                dto.setValidityStartDate(LocalDate.of(2024, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2024, 12, 31));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The repartition entity should have been created but there is no repartition entity in the database");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityWithoutEndDate_whenCreating_thenCreated() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidatorId(1L);
                dto.setValidityStartDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The repartition entity should have been created but there is no repartition entity in the database");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityWithStartDateEqualEndDate_whenCreating_thenCreated() throws Exception {
                // Given
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidatorId(1L);
                dto.setValidityStartDate(LocalDate.of(2018, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2018, 1, 1));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingRepartitionEntity").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The repartition entity should have been created but there is no repartition entity in the database");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Repartition entity 1\"},
                                                    {\"id\": 2, \"name\": \"Repartition entity 2\"},
                                                    {\"id\": 3, \"name\": \"Active repartition entity\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenActiveParams_whenListing_thenReturnActiveRepartitionEntitysOnly() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                "[{\"id\": 3, \"name\": \"Active repartition entity\"}]"));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNotActiveParams_whenListing_thenReturnNotActiveRepartitionEntitysOnly() throws Exception {
                // Given
                String active = "false";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Repartition entity 1\"},
                                                    {\"id\": 2, \"name\": \"Repartition entity 2\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Repartition entity 1\"},
                                                    {\"name\": \"Repartition entity 2\"},
                                                    {\"name\": \"Repartition entity 3\"},
                                                    {\"name\": \"Repartition entity 4\"},
                                                    {\"name\": \"Repartition entity 5\"},
                                                    {\"name\": \"Repartition entity 6\"},
                                                    {\"name\": \"Repartition entity 7\"},
                                                    {\"name\": \"Repartition entity 8\"},
                                                    {\"name\": \"Repartition entity 9\"},
                                                    {\"name\": \"Repartition entity 10\"},
                                                    {\"name\": \"Repartition entity 11\"},
                                                    {\"name\": \"Repartition entity 12\"},
                                                    {\"name\": \"Repartition entity 13\"},
                                                    {\"name\": \"Repartition entity 14\"},
                                                    {\"name\": \"Repartition entity 15\"},
                                                    {\"name\": \"Repartition entity 16\"},
                                                    {\"name\": \"Repartition entity 17\"},
                                                    {\"name\": \"Repartition entity 18\"},
                                                    {\"name\": \"Repartition entity 19\"},
                                                    {\"name\": \"Repartition entity 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Repartition entity 1\"},
                                                    {\"name\": \"Repartition entity 2\"},
                                                    {\"name\": \"Repartition entity 3\"},
                                                    {\"name\": \"Repartition entity 4\"},
                                                    {\"name\": \"Repartition entity 5\"},
                                                    {\"name\": \"Repartition entity 6\"},
                                                    {\"name\": \"Repartition entity 7\"},
                                                    {\"name\": \"Repartition entity 8\"},
                                                    {\"name\": \"Repartition entity 9\"},
                                                    {\"name\": \"Repartition entity 10\"},
                                                    {\"name\": \"Repartition entity 11\"},
                                                    {\"name\": \"Repartition entity 12\"},
                                                    {\"name\": \"Repartition entity 13\"},
                                                    {\"name\": \"Repartition entity 14\"},
                                                    {\"name\": \"Repartition entity 15\"},
                                                    {\"name\": \"Repartition entity 16\"},
                                                    {\"name\": \"Repartition entity 17\"},
                                                    {\"name\": \"Repartition entity 18\"},
                                                    {\"name\": \"Repartition entity 19\"},
                                                    {\"name\": \"Repartition entity 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Repartition entity 21\"},
                                                    {\"name\": \"Repartition entity 22\"},
                                                    {\"name\": \"Repartition entity 23\"},
                                                    {\"name\": \"Repartition entity 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/billingRepartitionEntity?page=" + page + "&size=" + nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Repartition entity 11\"},
                                                    {\"name\": \"Repartition entity 12\"},
                                                    {\"name\": \"Repartition entity 13\"},
                                                    {\"name\": \"Repartition entity 14\"},
                                                    {\"name\": \"Repartition entity 15\"},
                                                    {\"name\": \"Repartition entity 16\"},
                                                    {\"name\": \"Repartition entity 17\"},
                                                    {\"name\": \"Repartition entity 18\"},
                                                    {\"name\": \"Repartition entity 19\"},
                                                    {\"name\": \"Repartition entity 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity/" + id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"name\": \"Repartition entity 1\"}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity/" + id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingIdLinkedToRate_whenDeleting_thenError() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingRepartitionEntity/" + id));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result,
                                ApiError.BILLING_REPARTITION_ENTITY_NOT_DELETABLE_LINKED_TO_RATE,
                                "id");
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingIdNotLinkedToRate_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 2L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingRepartitionEntity/" + id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)),
                                "The billing repartition entity still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingRepartitionEntity/" + id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/" + id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyRepartitionEntity_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result,
                                ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID,
                                List.of("name", "validatorId", "validityStartDate"),
                                List.of("BillingRepartitionEntityStartDateBeforeEndDateConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityWithoutName_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setValidatorId(1L);
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID, "name");
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityWithoutValidatorId_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID, "validatorId");
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityWithoutStartDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidatorId(1L);
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID,
                                "validityStartDate");
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityWithStartDateAfterEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName("Test name");
                dto.setValidatorId(1L);
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_NOT_VALID,
                                "BillingRepartitionEntityStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityCorrect_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                Long newValidatorId = 2L;
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = LocalDate.of(2024, 12, 31);
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName(newName);
                dto.setValidatorId(newValidatorId);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingRepartitionEntity> optNewRepartitionEntity = repository.findById(id);
                if (optNewRepartitionEntity.isPresent()) {
                        BillingRepartitionEntity repartitionEntity = optNewRepartitionEntity.get();
                        assertEquals(newName, repartitionEntity.getName());
                        assertEquals(newValidatorId, repartitionEntity.getValidatorId());
                        assertEquals(newValidityStartDate, repartitionEntity.getValidityStartDate());
                        assertEquals(newValidityEndDate, repartitionEntity.getValidityEndDate());
                } else {
                        assertFalse(true,
                                        "The repartition entity with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityWithoutEndDate_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                Long newValidatorId = 2L;
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = null;
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName(newName);
                dto.setValidatorId(newValidatorId);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingRepartitionEntity> optNewRepartitionEntity = repository.findById(id);
                if (optNewRepartitionEntity.isPresent()) {
                        BillingRepartitionEntity repartitionEntity = optNewRepartitionEntity.get();
                        assertEquals(newName, repartitionEntity.getName());
                        assertEquals(newValidatorId, repartitionEntity.getValidatorId());
                        assertEquals(newValidityStartDate, repartitionEntity.getValidityStartDate());
                        assertEquals(newValidityEndDate, repartitionEntity.getValidityEndDate());
                } else {
                        assertFalse(true,
                                        "The repartition entity with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql",
                        "/db/billingRepartitionEntity/create-test-data-getting-billing-repartition-entity.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntity/delete-test-data-truncate-billing-repartition-entity.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityWithStartDateEqualEndDate_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newName = "Test name";
                Long newValidatorId = 2L;
                LocalDate newValidityStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newValidityEndDate = newValidityStartDate;
                BillingRepartitionEntityDTO dto = new BillingRepartitionEntityDTO();
                dto.setName(newName);
                dto.setValidatorId(newValidatorId);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntity/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingRepartitionEntity> optNewRepartitionEntity = repository.findById(id);
                if (optNewRepartitionEntity.isPresent()) {
                        BillingRepartitionEntity repartitionEntity = optNewRepartitionEntity.get();
                        assertEquals(newName, repartitionEntity.getName());
                        assertEquals(newValidatorId, repartitionEntity.getValidatorId());
                        assertEquals(newValidityStartDate, repartitionEntity.getValidityStartDate());
                        assertEquals(newValidityEndDate, repartitionEntity.getValidityEndDate());
                } else {
                        assertFalse(true,
                                        "The repartition entity with the given id does not exists anymore in the database");
                }
        }
}
