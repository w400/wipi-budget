package fr.wipi.back.budget.billingbudgetgroup.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import jakarta.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.billingbudgetgroup.data.repository.BillingBudgetGroupRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingBudgetGroupDTO;
import fr.wipi.back.budget.openapi.model.BillingBudgetTypeDTO;

@SpringBootTest
class BillingBudgetGroupApiDelegateImplTests implements GenericApiDelegateImplTests {
        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        BillingBudgetGroupRepository repository;

        private MockMvc mockMvc;

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesBillingBudgetGroupApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("billingBudgetGroupApiController"));
        }

        @Test
        void givenEmptyBudgetGroup_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID,
                                List.of("name", "validityStartDate"),
                                List.of("BillingBudgetGroupStartDateBeforeEndDateConstraint",
                                                "BillingBudgetGroupValidBudgetTypeConstraint"));
        }

        @Test
        void givenBudgetGroupWithoutName_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "name");
        }

        @Test
        void givenBudgetGroupWithoutStartDate_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "validityStartDate");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithStartDateAfterEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(1L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID,
                                "BillingBudgetGroupStartDateBeforeEndDateConstraint");
        }

        @Test
        void givenBudgetGroupWithoutBudgetGroup_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setType(null);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID,
                                "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithInexistingBudgetGroup_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(40L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID,
                                "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithValidityStartDateBeforeBudgetGroupValidityStartDate_whenCreating_thenError()
                        throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(2L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID,
                                "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithValidityStartDateEqualsBudgetGroupValidityStartDate_whenCreating_thenCreated()
                        throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(null);
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(3L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budgetGroup should have been created but there is no budgetGroup in the database");

        }

        @Test
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithValidityStartDateAfterBudgetGroupValidityStartDate_whenCreating_thenCreated()
                        throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 2));
                dto.setValidityEndDate(null);
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(3L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budgetGroup should have been created but there is no budgetGroup in the database");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithValidityEndDateNullButNotBudgetGroupValidityEndDate_whenCreating_thenError()
                        throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(null);
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(1L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID,
                                "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @ParameterizedTest
        @CsvSource({
                        "1, 2, 1", // givenBudgetGroupWithValidityEndDateBeforeBudgetGroupValidityEndDate_whenCreating_thenCreated
                        "12, 31, 1", // givenBudgetGroupWithValidityEndDateEqualsBudgetGroupValidityEndDate_whenCreating_thenCreated
                        "12, 31, 3" // givenBudgetGroupWithValidityEndDateNotNullButBudgetGroupValidityEndDateNull_whenCreating_thenCreated

        })
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithIncorrectValidityEndDate_whenCreating_thenCreated(int month, int dayOfMonth,
                        Long budgetTypeId)
                        throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, month, dayOfMonth));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(budgetTypeId);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budgetGroup should have been created but there is no budgetGroup in the database");
        }

        @Test
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithValidityEndDateAfterBudgetTypeValidityEndDate_whenCreating_thenError()
                        throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2023, 1, 1));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(1L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID,
                                "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"BudgetGroup 1\"},
                                                    {\"id\": 2, \"name\": \"BudgetGroup 2\"},
                                                    {\"id\": 3, \"name\": \"Active budgetGroup\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenActiveParams_whenListing_thenReturnActiveBudgetGroupsOnly() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                "[{\"id\": 3, \"name\": \"Active budgetGroup\"}]"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNotActiveParams_whenListing_thenReturnNotActiveBudgetGroupsOnly() throws Exception {
                // Given
                String active = "false";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"BudgetGroup 1\"},
                                                    {\"id\": 2, \"name\": \"BudgetGroup 2\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"BudgetGroup 1\"},
                                                    {\"name\": \"BudgetGroup 2\"},
                                                    {\"name\": \"BudgetGroup 3\"},
                                                    {\"name\": \"BudgetGroup 4\"},
                                                    {\"name\": \"BudgetGroup 5\"},
                                                    {\"name\": \"BudgetGroup 6\"},
                                                    {\"name\": \"BudgetGroup 7\"},
                                                    {\"name\": \"BudgetGroup 8\"},
                                                    {\"name\": \"BudgetGroup 9\"},
                                                    {\"name\": \"BudgetGroup 10\"},
                                                    {\"name\": \"BudgetGroup 11\"},
                                                    {\"name\": \"BudgetGroup 12\"},
                                                    {\"name\": \"BudgetGroup 13\"},
                                                    {\"name\": \"BudgetGroup 14\"},
                                                    {\"name\": \"BudgetGroup 15\"},
                                                    {\"name\": \"BudgetGroup 16\"},
                                                    {\"name\": \"BudgetGroup 17\"},
                                                    {\"name\": \"BudgetGroup 18\"},
                                                    {\"name\": \"BudgetGroup 19\"},
                                                    {\"name\": \"BudgetGroup 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"BudgetGroup 1\"},
                                                    {\"name\": \"BudgetGroup 2\"},
                                                    {\"name\": \"BudgetGroup 3\"},
                                                    {\"name\": \"BudgetGroup 4\"},
                                                    {\"name\": \"BudgetGroup 5\"},
                                                    {\"name\": \"BudgetGroup 6\"},
                                                    {\"name\": \"BudgetGroup 7\"},
                                                    {\"name\": \"BudgetGroup 8\"},
                                                    {\"name\": \"BudgetGroup 9\"},
                                                    {\"name\": \"BudgetGroup 10\"},
                                                    {\"name\": \"BudgetGroup 11\"},
                                                    {\"name\": \"BudgetGroup 12\"},
                                                    {\"name\": \"BudgetGroup 13\"},
                                                    {\"name\": \"BudgetGroup 14\"},
                                                    {\"name\": \"BudgetGroup 15\"},
                                                    {\"name\": \"BudgetGroup 16\"},
                                                    {\"name\": \"BudgetGroup 17\"},
                                                    {\"name\": \"BudgetGroup 18\"},
                                                    {\"name\": \"BudgetGroup 19\"},
                                                    {\"name\": \"BudgetGroup 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"BudgetGroup 21\"},
                                                    {\"name\": \"BudgetGroup 22\"},
                                                    {\"name\": \"BudgetGroup 23\"},
                                                    {\"name\": \"BudgetGroup 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/billingBudgetGroup?page={page}&size={size}", page, nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"BudgetGroup 11\"},
                                                    {\"name\": \"BudgetGroup 12\"},
                                                    {\"name\": \"BudgetGroup 13\"},
                                                    {\"name\": \"BudgetGroup 14\"},
                                                    {\"name\": \"BudgetGroup 15\"},
                                                    {\"name\": \"BudgetGroup 16\"},
                                                    {\"name\": \"BudgetGroup 17\"},
                                                    {\"name\": \"BudgetGroup 18\"},
                                                    {\"name\": \"BudgetGroup 19\"},
                                                    {\"name\": \"BudgetGroup 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"name\": \"BudgetGroup 1\"}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudgetGroup/{id}", id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)),
                                "The billing budget type still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudgetGroup/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();

                // When
                ResultActions result = this.mockMvc.perform(
                                put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyBudgetGroup_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, List.of("name", "validityStartDate"), List.of("BillingBudgetGroupStartDateBeforeEndDateConstraint", "BillingBudgetGroupValidBudgetTypeConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithoutName_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "name");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithoutStartDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "validityStartDate");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithStartDateAfterEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(1L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "BillingBudgetGroupStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithoutBudgetGroup_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setType(null);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithInexistingBudgetGroup_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(40L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithValidityStartDateBeforeBudgetTypeValidityStartDate_whenUpdating_thenError()
                        throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(2L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @ParameterizedTest
        @CsvSource({
                        "2, 30, 4", // givenBudgetGroupWithValidityStartDateEqualsBudgetTypeValidityStartDate_whenUpdating_thenUpdated
                        "3, 30, 4", // givenBudgetGroupWithValidityStartDateAfterBudgetTypeValidityStartDate_whenUpdating_thenUpdated
                        "2, 29, 5", // givenBudgetGroupWithValidityEndDateBeforeBudgetTypeValidityEndDate_whenUpdating_thenUpdated
                        "2, 30, 5", // givenBudgetGroupWithValidityEndDateEqualsBudgetTypeValidityEndDate_whenUpdating_thenUpdated
                        "2, 30, 4" // givenBudgetGroupWithValidityEndDateNotNullButBudgetTypeValidityEndDateNull_whenUpdating_thenUpdated
        })
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithCorrectDates_whenUpdating_thenUpdated(int startDateDayOfMonth, int endDateDayOfMonth,
                        Long budgetTypeId)
                        throws Exception {
                // Given
                Long id = 1L;
                long nbRecords = repository.count();
                String newName = "Test name";
                LocalDate newValidityStartDate = LocalDate.of(2022, 2, startDateDayOfMonth);
                LocalDate newValidityEndDate = LocalDate.of(2022, 11, endDateDayOfMonth);
                Long newTypeId = budgetTypeId;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName(newName);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(newTypeId);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingBudgetGroup> optNewBudgetGroup = repository.findById(id);
                if (optNewBudgetGroup.isPresent()) {
                        BillingBudgetGroup budgetGroup = optNewBudgetGroup.get();
                        assertEquals(newName, budgetGroup.getName());
                        assertEquals(newValidityStartDate, budgetGroup.getValidityStartDate());
                        assertEquals(newValidityEndDate, budgetGroup.getValidityEndDate());
                        assertEquals(newTypeId, budgetGroup.getType().getId());
                } else {
                        assertFalse(true, "The budget group with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithValidityEndDateNullButNotBudgetTypeValidityEndDate_whenUpdating_thenError()
                        throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(null);
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(1L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "BillingBudgetGroupValidBudgetTypeConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetGroupWithValidityEndDateAfterBudgetGroupValidityEndDate_whenUpdating_thenError()
                        throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2023, 1, 1));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(1L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_GROUP_NOT_VALID, "BillingBudgetGroupValidBudgetTypeConstraint");
        }
}
