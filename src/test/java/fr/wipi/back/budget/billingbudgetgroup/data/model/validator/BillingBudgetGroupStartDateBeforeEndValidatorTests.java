package fr.wipi.back.budget.billingbudgetgroup.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;

@SpringBootTest
class BillingBudgetGroupStartDateBeforeEndValidatorTests {

    private BillingBudgetGroupStartDateBeforeEndDateValidator validator = new BillingBudgetGroupStartDateBeforeEndDateValidator();
    
    @Test
    void whenBudgetGroupIsNull_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetType = null;

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertFalse(isValid, "When the BillingBudgetGroup object is null, the validator should failed");
    }

    @Test
    void whenEndDateIsNull_thenTestSucceed() {
        // Given
        BillingBudgetGroup budgetType = new BillingBudgetGroup();
        budgetType.setValidityStartDate(LocalDate.now());
        budgetType.setValidityEndDate(null);

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertTrue(isValid, "When the end date is null, the validator should succeed");
    }

    @Test
    void whenStartDateIsNull_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetType = new BillingBudgetGroup();
        budgetType.setValidityStartDate(null);
        budgetType.setValidityEndDate(LocalDate.now());

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertFalse(isValid, "When the start date is null, the validator should failed");
    }

    @Test
    void whenStartDateBeforeEndDate_thenTestSucceeded() {
        // Given
        BillingBudgetGroup budgetType = new BillingBudgetGroup();
        budgetType.setValidityStartDate(LocalDate.of(2022, 01, 01));
        budgetType.setValidityEndDate(LocalDate.of(2022, 12, 31));

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertTrue(isValid, "When the start date is before the end date, the validator should succeed");
    }

    @Test
    void whenStartDateEqualEndDate_thenTestSucceed() {
        // Given
        BillingBudgetGroup budgetType = new BillingBudgetGroup();
        budgetType.setValidityStartDate(LocalDate.of(2022, 01, 01));
        budgetType.setValidityEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertTrue(isValid, "When the start date is strictly equal to the end date, the validator should succeed");
    }

    @Test
    void whenStartDateAfterEndDate_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetType = new BillingBudgetGroup();
        budgetType.setValidityStartDate(LocalDate.of(2023, 01, 01));
        budgetType.setValidityEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(budgetType, null);

        // Then
        assertFalse(isValid, "When the start date is after the end date, the validator should failed");
    }
}
