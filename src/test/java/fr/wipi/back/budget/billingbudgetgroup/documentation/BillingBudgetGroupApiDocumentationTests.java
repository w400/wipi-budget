package fr.wipi.back.budget.billingbudgetgroup.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.BillingBudgetGroupDTO;
import fr.wipi.back.budget.openapi.model.BillingBudgetTypeDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class BillingBudgetGroupApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGroupDeleteExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudgetGroup/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("billing-budget-group-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the billing budget group to delete"))));
        }

        @Test
        @Sql(scripts = "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGroupAddExample() throws Exception {
                // Given
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(1L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudgetGroup").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("billing-budget-group-add-example",
                                requestFields(billingBudgetGroupFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGroupGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-group-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the billing budget group to retrieve")),
                                responseFields(billingBudgetGroupFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGroupGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-group-get-example", queryParameters(parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]").description("An array of billing budget groups"))
                                                .andWithPrefix("[].", billingBudgetGroupFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGroupGetActiveExample() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudgetGroup?active={active}", active));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-group-get-active-example", queryParameters(parameterWithName(
                                "active")
                                .description("A boolean flag to control if only active budget groups should be returned")),
                                responseFields(fieldWithPath("[]").description("An array of billing budget groups"))
                                                .andWithPrefix("[].", billingBudgetGroupFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-getting-billing-budget-group.sql",
                        "/db/billingBudgetGroup/create-test-data-creating-billing-budget-group.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudgetGroup/delete-test-data-truncate-billing-budget-group.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGroupUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                String newBudgetGroupName = "Test name";
                LocalDate newStartDate = LocalDate.of(2022, 1, 1);
                LocalDate newEndDate = LocalDate.of(2022, 12, 31);
                BillingBudgetGroupDTO dto = new BillingBudgetGroupDTO();
                dto.setName(newBudgetGroupName);
                dto.setValidityStartDate(newStartDate);
                dto.setValidityEndDate(newEndDate);
                BillingBudgetTypeDTO typeDto = new BillingBudgetTypeDTO();
                typeDto.setId(1L);
                dto.setType(typeDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudgetGroup/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-group-update-example", pathParameters(
                                parameterWithName("id").description("The id of the billing budget group to update")),
                                requestFields(billingBudgetGroupFieldDescriptors(true)),
                                responseFields(billingBudgetGroupFieldDescriptors(true))));
        }

        private FieldDescriptor[] billingBudgetGroupFieldDescriptors(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                list.add(fieldDescriptorForId(creation));
                list.add(fieldDescriptorForName());
                list.add(fieldDescriptorForValidityStartDate());
                list.add(fieldDescriptorForEndingDate());
                list.addAll(billingBudgetGroupFieldDescriptorsForBudgetType(creation));

                FieldDescriptor[] array = new FieldDescriptor[list.size()];
                list.toArray(array);
                return array;
        }

        private List<FieldDescriptor> billingBudgetGroupFieldDescriptorsForBudgetType(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                if(creation) {
                        list.add(fieldDescriptorForBudgetTypeId());
                        list.add(fieldDescriptorForBudgetTypeName());
                        list.add(fieldDescriptorForBudgetTypeValidityStartDate());
                        list.add(fieldDescriptorForBudgetTypeValidityEndDate());
                } else {
                        list.add(fieldDescriptorForBudgetType());
                }

                return list;
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the billing budget group." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("name").description(
                                "Name of the billing budget group");
        }

        private FieldDescriptor fieldDescriptorForValidityStartDate() {
                return fieldWithPath("validityStartDate").description(
                                "Validity start date of the budget group. Before this date the budget group could not be used");
        }

        private FieldDescriptor fieldDescriptorForEndingDate() {
                return fieldWithPath("validityEndDate").description(
                                "Validity end date of the budget group. After this date the budget group could not be used. If null the budget group is always valid starting after the validity start date");
        }

        private FieldDescriptor fieldDescriptorForBudgetType() {
                return fieldWithPath("type")
                                .description("Budget type to which this budget group is linked");
        }

        private FieldDescriptor fieldDescriptorForBudgetTypeId() {
                return fieldWithPath("type.id")
                                .description("Id of the budget type to which this budget group is linked");
        }

        private FieldDescriptor fieldDescriptorForBudgetTypeName() {
                return fieldWithPath("type.name").ignored();
        }

        private FieldDescriptor fieldDescriptorForBudgetTypeValidityStartDate() {
                return fieldWithPath("type.validityStartDate").ignored();
        }

        private FieldDescriptor fieldDescriptorForBudgetTypeValidityEndDate() {
                return fieldWithPath("type.validityEndDate").ignored();
        }

}
