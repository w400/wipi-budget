package fr.wipi.back.budget.billingbudgetgroup.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.billingbudgettype.data.model.BillingBudgetType;
import fr.wipi.back.budget.billingbudgettype.data.repository.BillingBudgetTypeRepository;

@SpringBootTest
class BillingBudgetGroupValidBudgetTypeValidatorTests {
    @Autowired
    private BillingBudgetGroupValidBudgetTypeValidator validator;

    @MockBean
    BillingBudgetTypeRepository budgetTypeRepository;

    @Test
    void whenBudgetGroupIsNull_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetGroup = null;

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertFalse(isValid, "When the BillingBudgetGroup object is null, the validator should failed");
    }

    @Test
    void whenBudgetGroupNotLinkedToBudgetType_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(null);

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertFalse(isValid,
                "When the validity start date of the BillingbudgetGroup object is null, the validator should failed");
    }

    @Test
    void whenBudgetGroupValidityStartDateIsnull_thenTestFailed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(null);
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertFalse(isValid,
                "When the validity start date of the BillingbudgetGroup object is null, the validator should failed");
    }

    @Test
    void whenBudgetGroupValidityStartDateBeforeBudgetTypeValidityStartDate_thenTestFailed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(LocalDate.of(2021, 12, 31));
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertFalse(isValid,
                "When the validity start date of the BillingbudgetGroup object is before the validity start date of the BillingBudgetType object, the validator should failed");
    }

    @Test
    void whenBudgetGroupValidityStartDateEqualsBudgetTypeValidityStartDate_thenTestSucceed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertTrue(isValid,
                "When the validity start date of the BillingbudgetGroup object is equal to the validity start date of the BillingBudgetType object, the validator should succeed");
    }

    @Test
    void whenBudgetGroupValidityStartDateAfterBudgetTypeValidityStartDate_thenTestSucceed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 2));
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertTrue(isValid,
                "When the validity start date of the BillingbudgetGroup object is after to the validity start date of the BillingBudgetType object, the validator should succeed");
    }

    @Test
    void whenBudgetGroupValidityEndDateNullButNotBudgetTypeValidityEndDate_thenTestFailed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetType.setValidityEndDate(LocalDate.of(2022, 12, 31));
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 2));
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertFalse(isValid,
                "When the validity end date of the BillingbudgetGroup object is null but not the validity end date of the BillingBudgetType object, the validator should failed");
    }

    @Test
    void whenBudgetGroupValidityEndDateBeforeBudgetTypeValidityEndDate_thenTestSucceed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetType.setValidityEndDate(LocalDate.of(2022, 12, 31));
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 2));
        budgetGroup.setValidityEndDate(LocalDate.of(2022, 12, 30));
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertTrue(isValid,
                "When the validity end date of the BillingbudgetGroup object is before the validity end date of the BillingBudgetType object, the validator should succeed");
    }

    @Test
    void whenBudgetGroupValidityEndDateEqualsBudgetTypeValidityEndDate_thenTestSucceed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetType.setValidityEndDate(LocalDate.of(2022, 12, 31));
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 2));
        budgetGroup.setValidityEndDate(LocalDate.of(2022, 12, 31));
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertTrue(isValid,
                "When the validity end date of the BillingbudgetGroup object is equal to the validity end date of the BillingBudgetType object, the validator should succeed");
    }

    @Test
    void whenBudgetGroupValidityEndDateAfterBudgetTypeValidityEndDate_thenTestFailed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetType.setValidityEndDate(LocalDate.of(2022, 12, 31));
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 2));
        budgetGroup.setValidityEndDate(LocalDate.of(2023, 1, 1));
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertFalse(isValid,
                "When the validity end date of the BillingbudgetGroup object is after the validity end date of the BillingBudgetType object, the validator should failed");
    }

    @Test
    void whenBudgetGroupValidityEndDateNotNullButBudgetTypeValidityEndDateNull_thenTestSucceed() {
        // Given
        BillingBudgetType budgetType = new BillingBudgetType();
        budgetType.setId(1L);
        budgetType.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetType.setValidityEndDate(null);
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setType(budgetType);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 2));
        budgetGroup.setValidityEndDate(LocalDate.of(2022, 12, 31));
        Mockito.when(budgetTypeRepository.findById(1L)).thenReturn(Optional.of(budgetType));

        // When
        boolean isValid = validator.isValid(budgetGroup, null);

        // Then
        assertTrue(isValid,
                "When the validity end date of the BillingbudgetGroup object is not null but the validity end date of the BillingBudgetType object is null, the validator should succeed");
    }
}
