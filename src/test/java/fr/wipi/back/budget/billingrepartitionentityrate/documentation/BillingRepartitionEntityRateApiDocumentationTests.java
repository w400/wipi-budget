package fr.wipi.back.budget.billingrepartitionentityrate.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityDTO;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityRateDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class BillingRepartitionEntityRateApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityRateDeleteExample() throws Exception {
                // Given
                Long id = 2L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingRepartitionEntityRate/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("billing-repartition-entity-rate-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the billing repartition entity rate to delete"))));
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityRateAddExample() throws Exception {
                // Given
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                dto.setRate(BigDecimal.ONE);
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1L);
                dto.setEntity(entityDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(post("/billingRepartitionEntityRate").content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("billing-repartition-entity-rate-add-example",
                                requestFields(billingRepartitionEntityRateFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityRateGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntityRate/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-repartition-entity-rate-get-by-id-example", pathParameters(
                                parameterWithName("id").description(
                                                "The id of the billing repartition entity rate to retrieve")),
                                responseFields(billingRepartitionEntityRateFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityRateGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntityRate?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-repartition-entity-rate-get-example", queryParameters(parameterWithName(
                                "page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]")
                                                .description("An array of billing repartition entity rates"))
                                                .andWithPrefix("[].",
                                                                billingRepartitionEntityRateFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingRepartitionEntityRateUpdateExample() throws Exception {
                // Given
                Long id = 4L;
                BigDecimal newRate = BigDecimal.valueOf(2L);
                Long newEntityId = 2L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                dto.setRate(newRate);
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(newEntityId);
                dto.setEntity(entityDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-repartition-entity-rate-update-example", pathParameters(
                                parameterWithName("id").description(
                                                "The id of the billing repartition entity rate to update")),
                                requestFields(billingRepartitionEntityRateFieldDescriptors(true)),
                                responseFields(billingRepartitionEntityRateFieldDescriptors(false))));
        }

        private FieldDescriptor[] billingRepartitionEntityRateFieldDescriptors(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                list.add(fieldDescriptorForId(creation));
                list.add(fieldDescriptorForRate());
                list.addAll(rateFieldDescriptorsForEntity(creation));

                FieldDescriptor[] array = new FieldDescriptor[list.size()];
                list.toArray(array);
                return array;
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the billing repartition entity rate." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForRate() {
                return fieldWithPath("rate").description(
                                "Rate of the billing repartition entity rate");
        }

        private List<FieldDescriptor> rateFieldDescriptorsForEntity(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                if (creation) {
                        list.add(fieldDescriptorForEntityId());
                } else {
                        list.add(fieldDescriptorForEntityIdIgnored());
                        list.add(fieldDescriptorForEntity());
                }

                list.add(fieldDescriptorForEntityName());
                list.add(fieldDescriptorForEntityValidatorId());
                list.add(fieldDescriptorForEntityValidityStartDate());
                list.add(fieldDescriptorForEntityEndingDate());

                return list;
        }

        private FieldDescriptor fieldDescriptorForEntity() {
                return fieldWithPath("entity")
                                .description("Billing repartition entity to which this billing entity repartition rate is linked");
        }

        private FieldDescriptor fieldDescriptorForEntityId() {
                return fieldWithPath("entity.id").description(
                                "Id of the billing repartition entity to which this billing entity repartition rate is linked");
        }

        private FieldDescriptor fieldDescriptorForEntityIdIgnored() {
                return fieldWithPath("entity.id").ignored();
        }

        private FieldDescriptor fieldDescriptorForEntityName() {
                return fieldWithPath("entity.name").ignored();
        }

        private FieldDescriptor fieldDescriptorForEntityValidatorId() {
                return fieldWithPath("entity.validatorId").ignored();
        }

        private FieldDescriptor fieldDescriptorForEntityValidityStartDate() {
                return fieldWithPath("entity.validityStartDate").ignored();
        }

        private FieldDescriptor fieldDescriptorForEntityEndingDate() {
                return fieldWithPath("entity.validityEndDate").ignored();
        }

}
