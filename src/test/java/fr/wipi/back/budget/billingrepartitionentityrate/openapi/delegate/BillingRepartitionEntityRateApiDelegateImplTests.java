package fr.wipi.back.budget.billingrepartitionentityrate.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.billingrepartitionentityrate.data.model.BillingRepartitionEntityRate;
import fr.wipi.back.budget.billingrepartitionentityrate.data.repository.BillingRepartitionEntityRateRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityDTO;
import fr.wipi.back.budget.openapi.model.BillingRepartitionEntityRateDTO;
import jakarta.servlet.ServletContext;

@SpringBootTest
class BillingRepartitionEntityRateApiDelegateImplTests implements GenericApiDelegateImplTests {
        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        BillingRepartitionEntityRateRepository repository;

        private MockMvc mockMvc;

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesBillingRepartitionEntityApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("billingRepartitionEntityRateApiController"));
        }

        @Test
        void givenEmptyRepartitionEntityRate_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();

                // When
                ResultActions result = this.mockMvc
                                .perform(post("/billingRepartitionEntityRate").content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result,
                                ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID,
                                List.of("rate"),
                                List.of("BillingRepartitionEntityRateValidEntityConstraint"));
        }

        @Test
        void givenRepartitionEntityRateWithoutEntity_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                dto.setEntity(null);
                dto.setRate(BigDecimal.ONE);

                // When
                ResultActions result = this.mockMvc
                                .perform(post("/billingRepartitionEntityRate").content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID,
                                "BillingRepartitionEntityRateValidEntityConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithInexistingEntity_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1000L);
                dto.setEntity(entityDto);
                dto.setRate(BigDecimal.ONE);

                // When
                ResultActions result = this.mockMvc
                                .perform(post("/billingRepartitionEntityRate").content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID,
                                "BillingRepartitionEntityRateValidEntityConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithoutRate_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1L);
                dto.setEntity(entityDto);
                dto.setRate(null);

                // When
                ResultActions result = this.mockMvc
                                .perform(post("/billingRepartitionEntityRate").content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID, "rate");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithNegativeRate_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1L);
                dto.setEntity(entityDto);
                dto.setRate(BigDecimal.valueOf(-1L));

                // When
                ResultActions result = this.mockMvc
                                .perform(post("/billingRepartitionEntityRate").content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID, "rate");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithRateGreaterThanOneHundred_whenCreating_thenError() throws Exception {
                // Given
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1L);
                dto.setEntity(entityDto);
                dto.setRate(BigDecimal.valueOf(101L));

                // When
                ResultActions result = this.mockMvc
                                .perform(post("/billingRepartitionEntityRate").content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID, "rate");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateCorrect_whenCreating_thenCreated() throws Exception {
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1L);
                dto.setEntity(entityDto);
                dto.setRate(BigDecimal.valueOf(50L));

                // When
                ResultActions result = this.mockMvc
                                .perform(post("/billingRepartitionEntityRate").content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(4, repository.count(),
                                "The repartition entity rate should have been created but there is no repartition entity rate in the database");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntity"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntityRate"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 4, \"rate\": 1},
                                                    {\"id\": 5, \"rate\": 2},
                                                    {\"id\": 6, \"rate\": 3}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenActiveParams_whenListing_thenReturnActiveRepartitionEntitysOnly() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/billingRepartitionEntityRate").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                "[{\"id\": 6, \"rate\": 3}]"));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNotActiveParams_whenListing_thenReturnNotActiveRepartitionEntitysOnly() throws Exception {
                // Given
                String active = "false";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/billingRepartitionEntityRate").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 4, \"rate\": 1},
                                                    {\"id\": 5, \"rate\": 2}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntityRate"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1,\"rate\": 1},
                                                    {\"id\": 2,\"rate\": 2},
                                                    {\"id\": 3,\"rate\": 3},
                                                    {\"id\": 4,\"rate\": 4},
                                                    {\"id\": 5,\"rate\": 5},
                                                    {\"id\": 6,\"rate\": 6},
                                                    {\"id\": 7,\"rate\": 7},
                                                    {\"id\": 8,\"rate\": 8},
                                                    {\"id\": 9,\"rate\": 9},
                                                    {\"id\": 10,\"rate\": 10},
                                                    {\"id\": 11,\"rate\": 11},
                                                    {\"id\": 12,\"rate\": 12},
                                                    {\"id\": 13,\"rate\": 13},
                                                    {\"id\": 14,\"rate\": 14},
                                                    {\"id\": 15,\"rate\": 15},
                                                    {\"id\": 16,\"rate\": 16},
                                                    {\"id\": 17,\"rate\": 17},
                                                    {\"id\": 18,\"rate\": 18},
                                                    {\"id\": 19,\"rate\": 19},
                                                    {\"id\": 20,\"rate\": 20}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntityRate?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"rate\": 1},
                                                    {\"id\": 2, \"rate\": 2},
                                                    {\"id\": 3, \"rate\": 3},
                                                    {\"id\": 4, \"rate\": 4},
                                                    {\"id\": 5, \"rate\": 5},
                                                    {\"id\": 6, \"rate\": 6},
                                                    {\"id\": 7, \"rate\": 7},
                                                    {\"id\": 8, \"rate\": 8},
                                                    {\"id\": 9, \"rate\": 9},
                                                    {\"id\": 10, \"rate\": 10},
                                                    {\"id\": 11, \"rate\": 11},
                                                    {\"id\": 12, \"rate\": 12},
                                                    {\"id\": 13, \"rate\": 13},
                                                    {\"id\": 14, \"rate\": 14},
                                                    {\"id\": 15, \"rate\": 15},
                                                    {\"id\": 16, \"rate\": 16},
                                                    {\"id\": 17, \"rate\": 17},
                                                    {\"id\": 18, \"rate\": 18},
                                                    {\"id\": 19, \"rate\": 19},
                                                    {\"id\": 20, \"rate\": 20}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntityRate?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 21, \"rate\": 21},
                                                    {\"id\": 22, \"rate\": 22},
                                                    {\"id\": 23, \"rate\": 23},
                                                    {\"id\": 24, \"rate\": 24}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/billingRepartitionEntityRate?page=" + page + "&size=" + nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 11, \"rate\": 11},
                                                    {\"id\": 12, \"rate\": 12},
                                                    {\"id\": 13, \"rate\": 13},
                                                    {\"id\": 14, \"rate\": 14},
                                                    {\"id\": 15, \"rate\": 15},
                                                    {\"id\": 16, \"rate\": 16},
                                                    {\"id\": 17, \"rate\": 17},
                                                    {\"id\": 18, \"rate\": 18},
                                                    {\"id\": 19, \"rate\": 19},
                                                    {\"id\": 20, \"rate\": 20}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntityRate/" + id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"id\": 1, \"rate\": 1}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingRepartitionEntityRate/" + id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingRepartitionEntityRate/" + id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)),
                                "The billing repartition entity still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingRepartitionEntityRate/" + id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql",
                        "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/" + id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }
        
        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyRepartitionEntityRate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 4L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result,
                                ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID,
                                List.of("rate"),
                                List.of("BillingRepartitionEntityRateValidEntityConstraint"));
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithoutEntity_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 4L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                dto.setEntity(null);
                dto.setRate(BigDecimal.ONE);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID,
                                "BillingRepartitionEntityRateValidEntityConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithInexistingEntity_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 4L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1000L);
                dto.setEntity(entityDto);
                dto.setRate(BigDecimal.ONE);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID,
                                "BillingRepartitionEntityRateValidEntityConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithoutRate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 4L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1L);
                dto.setEntity(entityDto);
                dto.setRate(null);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID, "rate");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithNegativeRate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 4L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1L);
                dto.setEntity(entityDto);
                dto.setRate(BigDecimal.valueOf(-1L));

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID, "rate");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateWithRateGreaterThanOneHundred_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 4L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(1L);
                dto.setEntity(entityDto);
                dto.setRate(BigDecimal.valueOf(101L));

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_REPARTITION_ENTITY_RATE_NOT_VALID, "rate");
        }

        @Test
        @Sql(scripts = "/db/billingRepartitionEntityRate/create-test-data-getting-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingRepartitionEntityRate/delete-test-data-truncate-billing-repartition-entity-rate.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenRepartitionEntityRateCorrect_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 4L;
                Long newEntityId = 2L;
                Long newRate = 60L;
                BillingRepartitionEntityRateDTO dto = new BillingRepartitionEntityRateDTO();
                BillingRepartitionEntityDTO entityDto = new BillingRepartitionEntityDTO();
                entityDto.setId(newEntityId);
                dto.setEntity(entityDto);
                dto.setRate(BigDecimal.valueOf(newRate));

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingRepartitionEntityRate/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingRepartitionEntityRate> optNewRepartitionEntityRate = repository.findById(id);
                if (optNewRepartitionEntityRate.isPresent()) {
                        BillingRepartitionEntityRate repartitionEntityRate = optNewRepartitionEntityRate.get();
                        assertEquals(newRate, repartitionEntityRate.getRate().longValue());
                        assertEquals(newEntityId, repartitionEntityRate.getEntity().getId());
                } else {
                        assertFalse(true,
                                        "The repartition entity rate with the given id does not exists anymore in the database");
                }
        }
}
