package fr.wipi.back.budget.billingrepartitionentityrate.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.wipi.back.budget.billingrepartitionentity.data.model.BillingRepartitionEntity;
import fr.wipi.back.budget.billingrepartitionentity.data.repository.BillingRepartitionEntityRepository;
import fr.wipi.back.budget.billingrepartitionentityrate.data.model.BillingRepartitionEntityRate;

@SpringBootTest
class BillingRepartitionEntityRateValidEntityValidatorTests {
    @Autowired
    private BillingRepartitionEntityRateValidEntityValidator validator;

    @MockBean
    BillingRepartitionEntityRepository repartitionEntityRepository;

    @Test
    void whenBillingRepartitionEntityRateIsNull_thenTestFailed() {
        // Given
        BillingRepartitionEntityRate billingRepartitionEntityRate = null;

        // When
        boolean isValid = validator.isValid(billingRepartitionEntityRate, null);

        // Then
        assertFalse(isValid, "When the BillingRepartitionEntityRate object is null, the validator should fail");
    }

    @Test
    void whenBillingRepartitionEntityRateNotLinkedToEntity_thenTestFailed() {
        // Given
        BillingRepartitionEntityRate billingRepartitionEntityRate = new BillingRepartitionEntityRate();
        billingRepartitionEntityRate.setEntity(null);

        // When
        boolean isValid = validator.isValid(billingRepartitionEntityRate, null);

        // Then
        assertFalse(isValid,
                "When the entity of the BillingRepartitionEntityRate object is null, the validator should fail");
    }

    @Test
    void whenBillingRepartitionEntityRateLinkedToEntityWithoutId_thenTestFailed() {
        // Given
        BillingRepartitionEntity billingRepartitionEntity = new BillingRepartitionEntity();
        billingRepartitionEntity.setId(null);
        BillingRepartitionEntityRate invoice = new BillingRepartitionEntityRate();
        invoice.setEntity(billingRepartitionEntity);

        // When
        boolean isValid = validator.isValid(invoice, null);

        // Then
        assertFalse(isValid,
                "When the entity of the BillingRepartitionEntityRate object has a null id, the validator should fail");
    }

    @Test
    void whenBillingRepartitionEntityRateLinkedToNonExistingEntity_thenTestFailed() {
        // Given
        BillingRepartitionEntity billingRepartitionEntity = new BillingRepartitionEntity();
        billingRepartitionEntity.setId(1L);
        BillingRepartitionEntityRate billingRepartitionEntityRate = new BillingRepartitionEntityRate();
        billingRepartitionEntityRate.setEntity(billingRepartitionEntity);
        Mockito.when(repartitionEntityRepository.findById(1L)).thenReturn(Optional.empty());

        // When
        boolean isValid = validator.isValid(billingRepartitionEntityRate, null);

        // Then
        assertFalse(isValid, "When the entity of the BillingRepartitionEntityRate object do not exists, the validator should fail");
    }

    @Test
    void whenBillingRepartitionEntityRateLinkedToExistingEntity_thenTestSucceeded() {
        // Given
        BillingRepartitionEntity billingRepartitionEntity = new BillingRepartitionEntity();
        billingRepartitionEntity.setId(1L);
        BillingRepartitionEntityRate billingRepartitionEntityRate = new BillingRepartitionEntityRate();
        billingRepartitionEntityRate.setEntity(billingRepartitionEntity);
        Mockito.when(repartitionEntityRepository.findById(1L)).thenReturn(Optional.of(billingRepartitionEntity));

        // When
        boolean isValid = validator.isValid(billingRepartitionEntityRate, null);

        // Then
        assertTrue(isValid, "When the entity of the BillingRepartitionEntityRate object exists, the validator should succeed");
    }
}
