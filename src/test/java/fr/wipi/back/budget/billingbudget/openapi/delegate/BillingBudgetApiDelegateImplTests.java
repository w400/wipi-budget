package fr.wipi.back.budget.billingbudget.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import jakarta.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.billingbudget.data.model.BillingBudget;
import fr.wipi.back.budget.billingbudget.data.repository.BillingBudgetRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingBudgetDTO;
import fr.wipi.back.budget.openapi.model.BillingBudgetGroupDTO;

@SpringBootTest
class BillingBudgetApiDelegateImplTests implements GenericApiDelegateImplTests {
        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        BillingBudgetRepository repository;

        private MockMvc mockMvc;

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesBillingBudgetApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("billingBudgetApiController"));
        }

        @Test
        void givenEmptyBudget_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                List.of("name", "code", "managerId", "validityStartDate"),
                                List.of("BillingBudgetStartDateBeforeEndDateConstraint",
                                                "BillingBudgetValidBudgetGroupConstraint"));
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutName_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID, "name");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutCode_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID, "code");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutStartDate_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityEndDate(LocalDate.now());
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID, "validityStartDate");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithStartDateAfterEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutManager_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID, "managerId");
        }

        @Test
        void givenBudgetWithoutBudgetGroup_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setManagerId(1L);
                dto.setGroup(null);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithInexistingBudgetGroup_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(40L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithValidityStartDateBeforeBudgetGroupValidityStartDate_whenCreating_thenError()
                        throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(2L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithValidityStartDateEqualsBudgetGroupValidityStartDate_whenCreating_thenCreated()
                        throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(null);
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(3L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budget should have been created but there is no budget in the database");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithValidityStartDateAfterBudgetGroupValidityStartDate_whenCreating_thenCreated()
                        throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 2));
                dto.setValidityEndDate(null);
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(3L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budget should have been created but there is no budget in the database");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithValidityEndDateNullButNotBudgetGroupValidityEndDate_whenCreating_thenError()
                        throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(null);
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @ParameterizedTest
        @CsvSource({
                        "1, 2, 1", // givenBudgetWithValidityEndDateBeforeBudgetGroupValidityEndDate_whenCreating_thenCreated
                        "12, 31, 1", // givenBudgetWithValidityEndDateEqualsBudgetGroupValidityEndDate_whenCreating_thenCreated
                        "12, 31, 3" // givenBudgetWithValidityEndDateNotNullButBudgetGroupValidityEndDateNull_whenCreating_thenCreated

        })
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithIncorrectValidityEndDate_whenCreating_thenCreated(int month, int dayOfMonth,
                        Long budgetGroupId)
                        throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, month, dayOfMonth));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(budgetGroupId);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The budget should have been created but there is no budget in the database");
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithValidityEndDateAfterBudgetGroupValidityEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2023, 1, 1));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Budget 1\"},
                                                    {\"id\": 2, \"name\": \"Budget 2\"},
                                                    {\"id\": 3, \"name\": \"Active budget\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenActiveParams_whenListing_thenReturnActiveBudgetsOnly() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                "[{\"id\": 3, \"name\": \"Active budget\"}]"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNotActiveParams_whenListing_thenReturnNotActiveBudgetsOnly() throws Exception {
                // Given
                String active = "false";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget").param("active", active));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Budget 1\"},
                                                    {\"id\": 2, \"name\": \"Budget 2\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Budget 1\"},
                                                    {\"name\": \"Budget 2\"},
                                                    {\"name\": \"Budget 3\"},
                                                    {\"name\": \"Budget 4\"},
                                                    {\"name\": \"Budget 5\"},
                                                    {\"name\": \"Budget 6\"},
                                                    {\"name\": \"Budget 7\"},
                                                    {\"name\": \"Budget 8\"},
                                                    {\"name\": \"Budget 9\"},
                                                    {\"name\": \"Budget 10\"},
                                                    {\"name\": \"Budget 11\"},
                                                    {\"name\": \"Budget 12\"},
                                                    {\"name\": \"Budget 13\"},
                                                    {\"name\": \"Budget 14\"},
                                                    {\"name\": \"Budget 15\"},
                                                    {\"name\": \"Budget 16\"},
                                                    {\"name\": \"Budget 17\"},
                                                    {\"name\": \"Budget 18\"},
                                                    {\"name\": \"Budget 19\"},
                                                    {\"name\": \"Budget 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Budget 1\"},
                                                    {\"name\": \"Budget 2\"},
                                                    {\"name\": \"Budget 3\"},
                                                    {\"name\": \"Budget 4\"},
                                                    {\"name\": \"Budget 5\"},
                                                    {\"name\": \"Budget 6\"},
                                                    {\"name\": \"Budget 7\"},
                                                    {\"name\": \"Budget 8\"},
                                                    {\"name\": \"Budget 9\"},
                                                    {\"name\": \"Budget 10\"},
                                                    {\"name\": \"Budget 11\"},
                                                    {\"name\": \"Budget 12\"},
                                                    {\"name\": \"Budget 13\"},
                                                    {\"name\": \"Budget 14\"},
                                                    {\"name\": \"Budget 15\"},
                                                    {\"name\": \"Budget 16\"},
                                                    {\"name\": \"Budget 17\"},
                                                    {\"name\": \"Budget 18\"},
                                                    {\"name\": \"Budget 19\"},
                                                    {\"name\": \"Budget 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Budget 21\"},
                                                    {\"name\": \"Budget 22\"},
                                                    {\"name\": \"Budget 23\"},
                                                    {\"name\": \"Budget 24\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc
                                .perform(get("/billingBudget?page={page}&size={size}", page, nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Budget 11\"},
                                                    {\"name\": \"Budget 12\"},
                                                    {\"name\": \"Budget 13\"},
                                                    {\"name\": \"Budget 14\"},
                                                    {\"name\": \"Budget 15\"},
                                                    {\"name\": \"Budget 16\"},
                                                    {\"name\": \"Budget 17\"},
                                                    {\"name\": \"Budget 18\"},
                                                    {\"name\": \"Budget 19\"},
                                                    {\"name\": \"Budget 20\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenGetting_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"name\": \"Budget 1\"}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenGetting_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudget/{id}", id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)),
                                "The billing budget type still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudget/{id}", id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1000L;
                BillingBudgetDTO dto = new BillingBudgetDTO();

                // When
                ResultActions result = this.mockMvc.perform(
                                put("/billingBudget/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyBudget_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                List.of("name", "code", "managerId", "validityStartDate"),
                                List.of("BillingBudgetStartDateBeforeEndDateConstraint",
                                                "BillingBudgetValidBudgetGroupConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutName_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID, "name");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutCode_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setValidityStartDate(LocalDate.now());
                dto.setValidityEndDate(LocalDate.now());
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID, "code");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutStartDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityEndDate(LocalDate.now());
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID, "validityStartDate");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithStartDateAfterEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2023, 3, 11));
                dto.setValidityEndDate(LocalDate.of(2022, 2, 20));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutManager_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID, "managerId");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithoutBudgetGroup_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setManagerId(1L);
                dto.setGroup(null);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithInexistingBudgetGroup_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(40L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithValidityStartDateBeforeBudgetGroupValidityStartDate_whenUpdating_thenError()
                        throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(2L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @ParameterizedTest
        @CsvSource({
                        "2, 30, 4", // givenBudgetWithValidityStartDateEqualsBudgetGroupValidityStartDate_whenUpdating_thenUpdated
                        "3, 30, 4", // givenBudgetWithValidityStartDateAfterBudgetGroupValidityStartDate_whenUpdating_thenUpdated
                        "2, 29, 5", // givenBudgetWithValidityEndDateBeforeBudgetGroupValidityEndDate_whenUpdating_thenUpdated
                        "2, 30, 5", // givenBudgetWithValidityEndDateEqualsBudgetGroupValidityEndDate_whenUpdating_thenUpdated
                        "2, 30, 4" // givenBudgetWithValidityEndDateNotNullButBudgetGroupValidityEndDateNull_whenUpdating_thenUpdated
        })
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithCorrectDates_whenUpdating_thenUpdated(int startDateDayOfMonth, int endDateDayOfMonth,
                        Long budgetGroupId)
                        throws Exception {
                // Given
                Long id = 1L;
                long nbRecords = repository.count();
                String newName = "Test name";
                String newCode = "Test code";
                LocalDate newValidityStartDate = LocalDate.of(2022, 2, startDateDayOfMonth);
                LocalDate newValidityEndDate = LocalDate.of(2022, 11, endDateDayOfMonth);
                Long newManagerId = 1L;
                Long newTypeId = budgetGroupId;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName(newName);
                dto.setCode(newCode);
                dto.setValidityStartDate(newValidityStartDate);
                dto.setValidityEndDate(newValidityEndDate);
                dto.setManagerId(newManagerId);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(newTypeId);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingBudget> optNewBudget = repository.findById(id);
                if (optNewBudget.isPresent()) {
                        BillingBudget budget = optNewBudget.get();
                        assertEquals(newName, budget.getName());
                        assertEquals(newCode, budget.getCode());
                        assertEquals(newValidityStartDate, budget.getValidityStartDate());
                        assertEquals(newValidityEndDate, budget.getValidityEndDate());
                        assertEquals(newManagerId, budget.getManagerId());
                        assertEquals(newTypeId, budget.getGroup().getId());
                } else {
                        assertFalse(true, "The budget with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithValidityEndDateNullButNotBudgetGroupValidityEndDate_whenUpdating_thenError()
                        throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(null);
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenBudgetWithValidityEndDateAfterBudgetGroupValidityEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2023, 1, 1));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO typeDto = new BillingBudgetGroupDTO();
                typeDto.setId(1L);
                dto.setGroup(typeDto);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_BUDGET_NOT_VALID,
                                "BillingBudgetValidBudgetGroupConstraint");
        }
}
