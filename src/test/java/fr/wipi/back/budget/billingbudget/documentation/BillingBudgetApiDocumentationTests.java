package fr.wipi.back.budget.billingbudget.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.BillingBudgetDTO;
import fr.wipi.back.budget.openapi.model.BillingBudgetGroupDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class BillingBudgetApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetDeleteExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingBudget/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("billing-budget-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the billing budget to delete"))));
        }

        @Test
        @Sql(scripts = "/db/billingBudget/create-test-data-creating-billing-budget.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetAddExample() throws Exception {
                // Given
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName("Test name");
                dto.setCode("Test code");
                dto.setValidityStartDate(LocalDate.of(2022, 1, 1));
                dto.setValidityEndDate(LocalDate.of(2022, 12, 31));
                dto.setManagerId(1L);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(1L);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc.perform(post("/billingBudget").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("billing-budget-add-example",
                                requestFields(billingBudgetFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the billing budget to retrieve")),
                                responseFields(billingBudgetFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget?page={page}", page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-get-example", queryParameters(parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]").description("An array of billing budgets"))
                                                .andWithPrefix("[].", billingBudgetFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetGetActiveExample() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingBudget?active={active}", active));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-get-active-example", queryParameters(parameterWithName(
                                "active")
                                .description("A boolean flag to control if only active budgets should be returned")),
                                responseFields(fieldWithPath("[]").description("An array of billing budgets"))
                                                .andWithPrefix("[].", billingBudgetFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingBudget/delete-test-data-truncate-billing-budget.sql",
                        "/db/billingBudget/create-test-data-getting-billing-budget.sql",
                        "/db/billingBudget/create-test-data-creating-billing-budget.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingBudget/delete-test-data-truncate-billing-budget.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingBudgetUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                String newBudgetName = "Test name";
                String newCode = "Test code";
                LocalDate newStartDate = LocalDate.of(2022, 1, 1);
                LocalDate newEndDate = LocalDate.of(2022, 12, 31);
                Long newManagerId = 1L;
                Long newGroupId = 1L;
                BillingBudgetDTO dto = new BillingBudgetDTO();
                dto.setName(newBudgetName);
                dto.setCode(newCode);
                dto.setValidityStartDate(newStartDate);
                dto.setValidityEndDate(newEndDate);
                dto.setManagerId(newManagerId);
                BillingBudgetGroupDTO groupDto = new BillingBudgetGroupDTO();
                groupDto.setId(newGroupId);
                dto.setGroup(groupDto);

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingBudget/{id}", id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-budget-update-example", pathParameters(
                                parameterWithName("id").description("The id of the billing budget to update")),
                                requestFields(billingBudgetFieldDescriptors(true)),
                                responseFields(billingBudgetFieldDescriptors(true))));
        }

        private FieldDescriptor[] billingBudgetFieldDescriptors(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                list.add(fieldDescriptorForId(creation));
                list.add(fieldDescriptorForName());
                list.add(fieldDescriptorForCode());
                list.add(fieldDescriptorForValidityStartDate());
                list.add(fieldDescriptorForEndingDate());
                list.add(fieldDescriptorForManager());
                list.addAll(billingBudgetFieldDescriptorsForBudgetGroup(creation));

                FieldDescriptor[] array = new FieldDescriptor[list.size()];
                list.toArray(array);
                return array;
        }

        private List<FieldDescriptor> billingBudgetFieldDescriptorsForBudgetGroup(boolean creation) {
                List<FieldDescriptor> list = new ArrayList<>();
                if(creation) {
                        list.add(fieldDescriptorForBudgetGroupId());
                        list.add(fieldDescriptorForBudgetGroupName());
                        list.add(fieldDescriptorForBudgetGroupValidityStartDate());
                        list.add(fieldDescriptorForBudgetGroupValidityEndDate());
                        list.add(fieldDescriptorForBudgetGroupType());
                } else {
                        list.add(fieldDescriptorForBudgetGroup());
                }

                return list;
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the billing budget." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("name").description(
                                "Name of the billing budget");
        }

        private FieldDescriptor fieldDescriptorForCode() {
                return fieldWithPath("code").description(
                                "Code of the billing budget");
        }

        private FieldDescriptor fieldDescriptorForValidityStartDate() {
                return fieldWithPath("validityStartDate").description(
                                "Validity start date of the budget. Before this date the budget could not be used");
        }

        private FieldDescriptor fieldDescriptorForEndingDate() {
                return fieldWithPath("validityEndDate").description(
                                "Validity end date of the budget. After this date the budget could not be used. If null the budget is always valid starting after the validity start date");
        }

        private FieldDescriptor fieldDescriptorForManager() {
                return fieldWithPath("managerId").description(
                                "Id of the user responsible of this budget");
        }

        private FieldDescriptor fieldDescriptorForBudgetGroup() {
                return fieldWithPath("group")
                                .description("Budget group to which this budget is linked");
        }

        private FieldDescriptor fieldDescriptorForBudgetGroupId() {
                return fieldWithPath("group.id")
                                .description("Id of the budget group to which this budget is linked");
        }

        private FieldDescriptor fieldDescriptorForBudgetGroupName() {
                return fieldWithPath("group.name").ignored();
        }

        private FieldDescriptor fieldDescriptorForBudgetGroupValidityStartDate() {
                return fieldWithPath("group.validityStartDate").ignored();
        }

        private FieldDescriptor fieldDescriptorForBudgetGroupValidityEndDate() {
                return fieldWithPath("group.validityEndDate").ignored();
        }

        private FieldDescriptor fieldDescriptorForBudgetGroupType() {
                return fieldWithPath("group.type").ignored();
        }

}
