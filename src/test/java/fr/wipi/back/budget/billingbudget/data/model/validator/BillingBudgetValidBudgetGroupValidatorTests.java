package fr.wipi.back.budget.billingbudget.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.wipi.back.budget.billingbudget.data.model.BillingBudget;
import fr.wipi.back.budget.billingbudgetgroup.data.model.BillingBudgetGroup;
import fr.wipi.back.budget.billingbudgetgroup.data.repository.BillingBudgetGroupRepository;

@SpringBootTest
class BillingBudgetValidBudgetValidatorTests {
    @Autowired
    private BillingBudgetValidBudgetGroupValidator validator;

    @MockBean
    BillingBudgetGroupRepository budgetGroupRepository;

    @Test
    void whenBudgetIsNull_thenTestFailed() {
        // Given
        BillingBudget budget = null;

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid, "When the BillingBudget object is null, the validator should failed");
    }

    @Test
    void whenBudgetNotLinkedToBudgetGroup_thenTestFailed() {
        // Given
        BillingBudget budget = new BillingBudget();
        budget.setGroup(null);

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid,
                "When the BillingBudget object is not linked to a budget group, the validator should failed");
    }

    @Test
    void whenBudgetValidityStartDateIsNull_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(null);
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid,
                "When the validity start date of the BillingBudget object is null, the validator should failed");
    }

    @Test
    void whenBudgetValidityStartDateBeforeBudgetGroupValidityStartDate_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(LocalDate.of(2021, 12, 31));
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid,
                "When the validity start date of the BillingBudget object is before the validity start date of the BillingBudgetGroup object, the validator should failed");
    }

    @Test
    void whenBudgetValidityStartDateEqualsBudgetGroupValidityStartDate_thenTestSucceed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(LocalDate.of(2022, 1, 1));
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertTrue(isValid,
                "When the validity start date of the BillingBudget object is equal to the validity start date of the BillingBudgetGroup object, the validator should succeed");
    }

    @Test
    void whenBudgetValidityStartDateAfterBudgetGroupValidityStartDate_thenTestSucceed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(LocalDate.of(2022, 1, 2));
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertTrue(isValid,
                "When the validity start date of the BillingBudget object is after to the validity start date of the BillingBudgetGroup object, the validator should succeed");
    }

    @Test
    void whenBudgetValidityEndDateNullButNotBudgetGroupValidityEndDate_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetGroup.setValidityEndDate(LocalDate.of(2022, 12, 31));
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(LocalDate.of(2022, 1, 2));
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid,
                "When the validity end date of the BillingBudget object is null but not the validity end date of the BillingBudgetGroup object, the validator should failed");
    }

    @Test
    void whenBudgetValidityEndDateBeforeBudgetGroupValidityEndDate_thenTestSucceed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetGroup.setValidityEndDate(LocalDate.of(2022, 12, 31));
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(LocalDate.of(2022, 1, 2));
        budget.setValidityEndDate(LocalDate.of(2022, 12, 30));
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertTrue(isValid,
                "When the validity end date of the BillingBudget object is before the validity end date of the BillingBudgetGroup object, the validator should succeed");
    }

    @Test
    void whenBudgetValidityEndDateEqualsBudgetGroupValidityEndDate_thenTestSucceed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetGroup.setValidityEndDate(LocalDate.of(2022, 12, 31));
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(LocalDate.of(2022, 1, 2));
        budget.setValidityEndDate(LocalDate.of(2022, 12, 31));
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertTrue(isValid,
                "When the validity end date of the BillingBudget object is equal to the validity end date of the BillingBudgetGroup object, the validator should succeed");
    }

    @Test
    void whenBudgetValidityEndDateAfterBudgetGroupValidityEndDate_thenTestFailed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetGroup.setValidityEndDate(LocalDate.of(2022, 12, 31));
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(LocalDate.of(2022, 1, 2));
        budget.setValidityEndDate(LocalDate.of(2023, 1, 1));
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid,
                "When the validity end date of the BillingBudget object is after the validity end date of the BillingBudgetGroup object, the validator should failed");
    }

    @Test
    void whenBudgetValidityEndDateNotNullButBudgetGroupValidityEndDateNull_thenTestSucceed() {
        // Given
        BillingBudgetGroup budgetGroup = new BillingBudgetGroup();
        budgetGroup.setId(1L);
        budgetGroup.setValidityStartDate(LocalDate.of(2022, 1, 1));
        budgetGroup.setValidityEndDate(null);
        BillingBudget budget = new BillingBudget();
        budget.setGroup(budgetGroup);
        budget.setValidityStartDate(LocalDate.of(2022, 1, 2));
        budget.setValidityEndDate(LocalDate.of(2022, 12, 31));
        Mockito.when(budgetGroupRepository.findById(1L)).thenReturn(Optional.of(budgetGroup));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertTrue(isValid,
                "When the validity end date of the BillingBudget object is not null but the validity end date of the BillingBudgetGroup object is null, the validator should succeed");
    }
}
