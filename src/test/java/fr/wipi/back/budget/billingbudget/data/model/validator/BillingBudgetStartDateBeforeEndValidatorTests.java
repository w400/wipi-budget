package fr.wipi.back.budget.billingbudget.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import fr.wipi.back.budget.billingbudget.data.model.BillingBudget;

@SpringBootTest
class BillingBudgetStartDateBeforeEndValidatorTests {

    private BillingBudgetStartDateBeforeEndDateValidator validator = new BillingBudgetStartDateBeforeEndDateValidator();
    
    @Test
    void whenBudgetIsNull_thenTestFailed() {
        // Given
        BillingBudget budget = null;

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid, "When the BillingBudget object is null, the validator should failed");
    }

    @Test
    void whenEndDateIsNull_thenTestSucceed() {
        // Given
        BillingBudget budget = new BillingBudget();
        budget.setValidityStartDate(LocalDate.now());
        budget.setValidityEndDate(null);

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertTrue(isValid, "When the end date is null, the validator should succeed");
    }

    @Test
    void whenStartDateIsNull_thenTestFailed() {
        // Given
        BillingBudget budget = new BillingBudget();
        budget.setValidityStartDate(null);
        budget.setValidityEndDate(LocalDate.now());

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid, "When the start date is null, the validator should failed");
    }

    @Test
    void whenStartDateBeforeEndDate_thenTestSucceeded() {
        // Given
        BillingBudget budget = new BillingBudget();
        budget.setValidityStartDate(LocalDate.of(2022, 01, 01));
        budget.setValidityEndDate(LocalDate.of(2022, 12, 31));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertTrue(isValid, "When the start date is before the end date, the validator should succeed");
    }

    @Test
    void whenStartDateEqualEndDate_thenTestSucceed() {
        // Given
        BillingBudget budget = new BillingBudget();
        budget.setValidityStartDate(LocalDate.of(2022, 01, 01));
        budget.setValidityEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertTrue(isValid, "When the start date is strictly equal to the end date, the validator should succeed");
    }

    @Test
    void whenStartDateAfterEndDate_thenTestFailed() {
        // Given
        BillingBudget budget = new BillingBudget();
        budget.setValidityStartDate(LocalDate.of(2023, 01, 01));
        budget.setValidityEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(budget, null);

        // Then
        assertFalse(isValid, "When the start date is after the end date, the validator should failed");
    }
}
