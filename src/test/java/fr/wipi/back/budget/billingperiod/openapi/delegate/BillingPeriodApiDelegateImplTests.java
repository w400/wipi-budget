package fr.wipi.back.budget.billingperiod.openapi.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import jakarta.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.GenericApiDelegateImplTests;
import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;
import fr.wipi.back.budget.billingperiod.data.repository.BillingPeriodRepository;
import fr.wipi.back.budget.openapi.error.ApiError;
import fr.wipi.back.budget.openapi.model.BillingPeriodDTO;

@SpringBootTest
class BillingPeriodApiDelegateImplTests implements GenericApiDelegateImplTests {

        @Autowired
        private WebApplicationContext webApplicationContext;

        @Autowired
        ObjectMapper mapper;

        @Autowired
        BillingPeriodRepository repository;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() throws Exception {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        }

        @Test
        void givenWac_whenServletContext_thenItProvidesBillingPeriodApiDelegateImpl() {
                ServletContext servletContext = webApplicationContext.getServletContext();

                assertNotNull(servletContext);
                assertTrue(servletContext instanceof MockServletContext);
                assertNotNull(webApplicationContext.getBean("billingPeriodApiController"));
        }

        @Test
        void givenEmptyPeriod_whenCreating_thenError() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID,
                                List.of("label", "startDate", "endDate"),
                                List.of("BillingPeriodStartDateBeforeEndDateConstraint",
                                                "BillingPeriodNotOverlappingConstraint"));
        }

        @Test
        void givenPeriodWithoutName_whenCreating_thenError() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setStartDate(LocalDate.now());
                dto.setEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID, "label");
        }

        @Test
        void givenPeriodWithoutStartDate_whenCreating_thenError() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID, "startDate");
        }

        @Test
        void givenPeriodWithoutEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID, "endDate");
        }

        @Test
        void givenPeriodWithStartDateEqualEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.of(2018, 1, 1));
                dto.setEndDate(LocalDate.of(2018, 1, 1));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID,
                                "BillingPeriodStartDateBeforeEndDateConstraint");
        }

        @Test
        void givenPeriodWithStartDateAfterEndDate_whenCreating_thenError() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.of(2023, 3, 11));
                dto.setEndDate(LocalDate.of(2022, 2, 20));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID,
                                "BillingPeriodStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-overlapping-validator.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPeriodWithOverlappingPeriod_whenCreating_thenError() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.of(2022, 3, 1));
                dto.setEndDate(LocalDate.of(2023, 2, 28));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID,
                                "BillingPeriodNotOverlappingConstraint");
        }

        @Test
        void givenPeriodCorrect_whenCreating_thenCreated() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.of(2024, 1, 1));
                dto.setEndDate(LocalDate.of(2024, 12, 31));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                assertEquals(1, repository.count(),
                                "The period should have been created but there is no period in the database");
        }

        @Test
        void givenNoData_whenListing_thenReturnEmptyResult() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json("[]"));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNoParams_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Year 1921\"},
                                                    {\"id\": 2, \"name\": \"Year 1922\"},
                                                    {\"id\": 3, \"name\": \"Active period\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenActiveParams_whenListing_thenReturnActivePeriodsOnly() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod").param("active", active));

                // Then
                result.andExpect(status().isOk());
                // Je ne comprends pas trop pourquoi c'est 4 comme id, ça devrait être 1 mais je
                // n'arrive pas à resetter l'identité correctement
                // Du coup j'enlève le test sur l'id et laisse juste le test sur le "name"
                result.andExpect(content().json(
                                "[{\"name\": \"Active period\"}]"));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNotActiveParams_whenListing_thenReturnActivePeriodsOnly() throws Exception {
                // Given
                String active = "false";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod").param("active", active));

                // Then
                result.andExpect(status().isOk());
                // Je ne comprends pas trop pourquoi c'est 4 comme id, ça devrait être 1 mais je
                // n'arrive pas à resetter l'identité correctement
                // Du coup j'enlève le test sur l'id et laisse juste le test sur le "name"
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"id\": 1, \"name\": \"Year 1921\"},
                                                    {\"id\": 2, \"name\": \"Year 1922\"},
                                                    {\"id\": 3, \"name\": \"Active period\"}
                                                ]"""));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndNoPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod"));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Year 1921\"},
                                                    {\"name\": \"Year 1922\"},
                                                    {\"name\": \"Year 1923\"},
                                                    {\"name\": \"Year 1924\"},
                                                    {\"name\": \"Year 1925\"},
                                                    {\"name\": \"Year 1926\"},
                                                    {\"name\": \"Year 1927\"},
                                                    {\"name\": \"Year 1928\"},
                                                    {\"name\": \"Year 1929\"},
                                                    {\"name\": \"Year 1930\"},
                                                    {\"name\": \"Year 1931\"},
                                                    {\"name\": \"Year 1932\"},
                                                    {\"name\": \"Year 1933\"},
                                                    {\"name\": \"Year 1934\"},
                                                    {\"name\": \"Year 1935\"},
                                                    {\"name\": \"Year 1936\"},
                                                    {\"name\": \"Year 1937\"},
                                                    {\"name\": \"Year 1938\"},
                                                    {\"name\": \"Year 1939\"},
                                                    {\"name\": \"Year 1940\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndFirstPage_whenListing_thenReturnFirstPage() throws Exception {
                // Given
                String page = "0";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Year 1921\"},
                                                    {\"name\": \"Year 1922\"},
                                                    {\"name\": \"Year 1923\"},
                                                    {\"name\": \"Year 1924\"},
                                                    {\"name\": \"Year 1925\"},
                                                    {\"name\": \"Year 1926\"},
                                                    {\"name\": \"Year 1927\"},
                                                    {\"name\": \"Year 1928\"},
                                                    {\"name\": \"Year 1929\"},
                                                    {\"name\": \"Year 1930\"},
                                                    {\"name\": \"Year 1931\"},
                                                    {\"name\": \"Year 1932\"},
                                                    {\"name\": \"Year 1933\"},
                                                    {\"name\": \"Year 1934\"},
                                                    {\"name\": \"Year 1935\"},
                                                    {\"name\": \"Year 1936\"},
                                                    {\"name\": \"Year 1937\"},
                                                    {\"name\": \"Year 1938\"},
                                                    {\"name\": \"Year 1939\"},
                                                    {\"name\": \"Year 1940\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPage_whenListing_thenReturnSecondPage() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Year 1941\"},
                                                    {\"name\": \"Year 1942\"},
                                                    {\"name\": \"Year 1943\"},
                                                    {\"name\": \"Year 1944\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenLargeDatasetAndSecondPageAndTenElementsPerPage_whenListing_thenReturnSecondPageOfTenElements()
                        throws Exception {
                // Given
                String page = "1";
                String nbElements = "10";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod?page=" + page + "&size=" + nbElements));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                [
                                                    {\"name\": \"Year 1931\"},
                                                    {\"name\": \"Year 1932\"},
                                                    {\"name\": \"Year 1933\"},
                                                    {\"name\": \"Year 1934\"},
                                                    {\"name\": \"Year 1935\"},
                                                    {\"name\": \"Year 1936\"},
                                                    {\"name\": \"Year 1937\"},
                                                    {\"name\": \"Year 1938\"},
                                                    {\"name\": \"Year 1939\"},
                                                    {\"name\": \"Year 1940\"}
                                                ]
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_thenReturningEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod/" + id));

                // Then
                result.andExpect(status().isOk());
                result.andExpect(content().json(
                                """
                                                    {\"name\": \"Year 1921\"}
                                                """));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_thenReturningNotFound() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod/" + id));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingId_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingPeriod/" + id));

                // Then
                result.andExpect(status().isNoContent());
                assertFalse(repository.existsById(Long.valueOf(id)), "The billing period still exists in the database");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingIdWithLinkedInvoice_whenDeleting_thenBadRequest() throws Exception {
                // Given
                Long id = 2L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingPeriod/" + id));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PERIOD_NOT_DELETABLE_LINKED_TO_INVOICE, "id");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenNonExistingId_whenDeleting_thenDeletingEntity() throws Exception {
                // Given
                Long id = 1000L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingPeriod/" + id));

                // Then
                result.andExpect(status().isNotFound());
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        void givenNonExistingId_whenUpdating_thenNotFound() throws Exception {
                // Given
                Long id = 1L;
                BillingPeriodDTO dto = new BillingPeriodDTO();

                // When
                ResultActions result = this.mockMvc
                                .perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isNotFound());
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenEmptyPeriod_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingPeriodDTO dto = new BillingPeriodDTO();

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyMultipleIncorrectFieldsAndConstraints(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID,
                                List.of("label", "startDate", "endDate"),
                                List.of("BillingPeriodStartDateBeforeEndDateConstraint",
                                                "BillingPeriodNotOverlappingConstraint"));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPeriodWithoutName_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setStartDate(LocalDate.now());
                dto.setEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID, "label");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPeriodWithoutStartDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setEndDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID, "startDate");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPeriodWithoutEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.now());

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID, "endDate");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPeriodWithStartDateEqualEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.of(2018, 1, 1));
                dto.setEndDate(LocalDate.of(2018, 1, 1));

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID,
                                "BillingPeriodStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPeriodWithStartDateAfterEndDate_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.of(2023, 3, 11));
                dto.setEndDate(LocalDate.of(2022, 2, 20));

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID,
                                "BillingPeriodStartDateBeforeEndDateConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPeriodWithOverlappingPeriod_whenUpdating_thenError() throws Exception {
                // Given
                Long id = 1L;
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.of(1921, 1, 1));
                dto.setEndDate(LocalDate.of(1922, 2, 1));

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneConstraintNotValidated(mapper, result, ApiError.BILLING_PERIOD_NOT_VALID,
                                "BillingPeriodNotOverlappingConstraint");
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenPeriodCorrect_whenUpdating_thenUpdated() throws Exception {
                // Given
                long nbRecords = repository.count();
                Long id = 1L;
                String newPeriodName = "Test name";
                LocalDate newStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newEndDate = LocalDate.of(2024, 12, 31);
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName(newPeriodName);
                dto.setStartDate(newStartDate);
                dto.setEndDate(newEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                assertEquals(nbRecords, repository.count(),
                                "No new records should have been created, nor any record deleted");
                Optional<BillingPeriod> optNewPeriod = repository.findById(id);
                if (optNewPeriod.isPresent()) {
                        BillingPeriod period = optNewPeriod.get();
                        assertEquals(newPeriodName, period.getLabel());
                        assertEquals(newStartDate, period.getStartDate());
                        assertEquals(newEndDate, period.getEndDate());
                } else {
                        assertFalse(true, "The period with the given id does not exists anymore in the database");
                }
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void givenExistingIdWithLinkedInvoice_whenUpdating_thenBadRequest() throws Exception {
                // Given
                Long id = 2L;
                String newPeriodName = "Test name";
                LocalDate newStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newEndDate = LocalDate.of(2024, 12, 31);
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName(newPeriodName);
                dto.setStartDate(newStartDate);
                dto.setEndDate(newEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/" + id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isBadRequest());
                verifyOnlyOneFieldError(mapper, result, ApiError.BILLING_PERIOD_NOT_UPDATABLE_LINKED_TO_INVOICE, "id");
        }

}
