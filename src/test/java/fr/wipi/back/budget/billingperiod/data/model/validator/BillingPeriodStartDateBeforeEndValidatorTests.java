package fr.wipi.back.budget.billingperiod.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;

@SpringBootTest
class BillingPeriodStartDateBeforeEndValidatorTests {

    private BillingPeriodStartDateBeforeEndDateValidator validator = new BillingPeriodStartDateBeforeEndDateValidator();
    
    @Test
    void whenPeriodIsNull_thenTestFailed() {
        // Given
        BillingPeriod period = null;

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "When the BillingPeriod object is null, the validator should failed");
    }

    @Test
    void whenEndDateIsNull_thenTestFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.now());
        period.setEndDate(null);

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "When the end date is null, the validator should failed");
    }

    @Test
    void whenStartDateIsNull_thenTestFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(null);
        period.setEndDate(LocalDate.now());

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "When the start date is null, the validator should failed");
    }

    @Test
    void whenStartDateBeforeEndDate_thenTestSucceeded() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 01, 01));
        period.setEndDate(LocalDate.of(2022, 12, 31));

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertTrue(isValid, "When the start date is before the end date, the validator should succeed");
    }

    @Test
    void whenStartDateEqualEndDate_thenTestFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 01, 01));
        period.setEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "When the start date is strictly equal to the end date, the validator should failed");
    }

    @Test
    void whenStartDateAfterEndDate_thenTestFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2023, 01, 01));
        period.setEndDate(LocalDate.of(2022, 01, 01));

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "When the start date is after the end date, the validator should failed");
    }
}
