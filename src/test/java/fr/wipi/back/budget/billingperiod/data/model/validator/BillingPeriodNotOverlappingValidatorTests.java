package fr.wipi.back.budget.billingperiod.data.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import fr.wipi.back.budget.billingperiod.data.model.BillingPeriod;

@SpringBootTest
@Sql(scripts = "/db/billingPeriod/create-test-data-overlapping-validator.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
class BillingPeriodNotOverlappingValidatorTests {
    
    @Autowired
    BillingPeriodNotOverlappingValidator validator;

    @Test
    void givenNullPeriod_thenValidatorFailed() {
        // Given
        BillingPeriod period = null;

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "As the period tested is null, the validator should failed");
    }

    @Test
    void givenPeriodWithStartDateNull_thenValidatorFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(null);
        period.setEndDate(LocalDate.now());

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "As the period tested has a null start date, the validator should failed");
    }

    @Test
    void givenPeriodWithEndDateNull_thenValidatorFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.now());
        period.setEndDate(null);

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "As the period tested has a null end date, the validator should failed");
    }

    @Test
    void givenPeriodInsideExistingPeriod_thenValidatorFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 2, 1));
        period.setEndDate(LocalDate.of(2022, 3, 3));

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "As the period tested is inside an existing one, the validator should failed");
    }

    @Test
    void givenPeriodEndingExistingPeriod_thenValidatorFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2019, 1, 1));
        period.setEndDate(LocalDate.of(2020, 1, 2));

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "As the period tested start before but end inside an existing one, the validator should failed");
    }

    @Test
    void givenPeriodStartingExistingPeriod_thenValidatorFailed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2022, 11, 1));
        period.setEndDate(LocalDate.of(2023, 3, 2));

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertFalse(isValid, "As the period tested starts inside an existing one, the validator should failed");
    }

    @Test
    void givenPeriodNotOverlapping_thenValidatorSucceed() {
        // Given
        BillingPeriod period = new BillingPeriod();
        period.setStartDate(LocalDate.of(2027, 1, 1));
        period.setEndDate(LocalDate.of(2027, 2, 2));

        // When
        boolean isValid = validator.isValid(period, null);

        // Then
        assertTrue(isValid, "As the period do not overlap any existing one, the validator should succeed");
    }
}
