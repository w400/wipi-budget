package fr.wipi.back.budget.billingperiod.documentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.queryParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.wipi.back.budget.openapi.model.BillingPeriodDTO;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest
class BillingPeriodApiDocumentationTests {
        private MockMvc mockMvc;

        @Autowired
        ObjectMapper mapper;

        @BeforeEach
        public void setUp(WebApplicationContext webApplicationContext,
                        RestDocumentationContextProvider restDocumentation) {
                this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                .apply(documentationConfiguration(restDocumentation))
                                .alwaysDo(
                                                document("{method-name}", preprocessRequest(prettyPrint()),
                                                                preprocessResponse(prettyPrint())))
                                .build();
        }

        private String asJsonString(final Object obj) {
                try {
                        return mapper.writeValueAsString(obj);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingPeriodDeleteExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(delete("/billingPeriod/{id}", id));

                // Then
                result.andExpect(status().isNoContent())
                                .andDo(document("billing-period-delete-example",
                                                pathParameters(parameterWithName("id").description(
                                                                "The id of the billing period to delete"))));
        }

        @Test
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingPeriodAddExample() throws Exception {
                // Given
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName("Test name");
                dto.setStartDate(LocalDate.of(2024, 1, 1));
                dto.setEndDate(LocalDate.of(2024, 12, 31));

                // When
                ResultActions result = this.mockMvc.perform(post("/billingPeriod").content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isCreated());
                result.andDo(document("billing-period-add-example",
                                requestFields(billingPeriodFieldDescriptors(true))));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingPeriodGetByIdExample() throws Exception {
                // Given
                Long id = 1L;

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod/{id}", id));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-period-get-by-id-example", pathParameters(
                                parameterWithName("id").description("The id of the billing period to retrieve")),
                                responseFields(billingPeriodFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period-paged.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingPeriodGetExample() throws Exception {
                // Given
                String page = "1";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod?page=" + page));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-period-get-example", queryParameters(parameterWithName("page")
                                .description("The page of the paginated result to return, by default 0")),
                                responseFields(fieldWithPath("[]").description("An array of billing periods"))
                                                .andWithPrefix("[].", billingPeriodFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingPeriodGetActiveExample() throws Exception {
                // Given
                String active = "true";

                // When
                ResultActions result = this.mockMvc.perform(get("/billingPeriod?active=" + active));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-period-get-active-example", queryParameters(parameterWithName("active")
                                .description("A boolean flag to control if only active periods should be returned")),
                                responseFields(fieldWithPath("[]").description("An array of billing periods"))
                                                .andWithPrefix("[].", billingPeriodFieldDescriptors(false))));
        }

        @Test
        @Sql(scripts = { "/db/billingPeriod/delete-test-data-truncate-billing-period.sql",
                        "/db/billingPeriod/create-test-data-getting-billing-period.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
        @Sql(scripts = "/db/billingPeriod/delete-test-data-truncate-billing-period.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
        void billingPeriodUpdateExample() throws Exception {
                // Given
                Long id = 1L;
                String newPeriodName = "Test name";
                LocalDate newStartDate = LocalDate.of(2024, 1, 1);
                LocalDate newEndDate = LocalDate.of(2024, 12, 31);
                BillingPeriodDTO dto = new BillingPeriodDTO();
                dto.setName(newPeriodName);
                dto.setStartDate(newStartDate);
                dto.setEndDate(newEndDate);

                // When
                ResultActions result = this.mockMvc.perform(put("/billingPeriod/{id}", id).content(asJsonString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));

                // Then
                result.andExpect(status().isOk());
                result.andDo(document("billing-period-update-example", pathParameters(
                                parameterWithName("id").description("The id of the billing period to update")),
                                requestFields(billingPeriodFieldDescriptors(true)),
                                responseFields(billingPeriodFieldDescriptors(false))));
        }

        private FieldDescriptor[] billingPeriodFieldDescriptors(boolean creation) {
                return new FieldDescriptor[] { fieldDescriptorForId(creation), fieldDescriptorForName(),
                                fieldDescriptorForStartingDate(), fieldDescriptorForEndingDate() };
        }

        private FieldDescriptor fieldDescriptorForId(boolean creation) {
                return fieldWithPath("id").description(
                                "Id of the billing period." + (creation
                                                ? "This field should not be passed neither for creation nor for update"
                                                : ""));
        }

        private FieldDescriptor fieldDescriptorForName() {
                return fieldWithPath("name").description(
                                "Name of the billing period, usually a year but could be anything textually identifying the period covered by the billing period");
        }

        private FieldDescriptor fieldDescriptorForStartingDate() {
                return fieldWithPath("startDate").description(
                                "Starting date of the period covered by the billing period");
        }

        private FieldDescriptor fieldDescriptorForEndingDate() {
                return fieldWithPath("endDate").description(
                                "Ending date of the period covered by the billing period");
        }

}
